'''
Python routines used to test claims in the papers
*** SDN = Sparse Dowker Nerves ***
*** SFN = Sparse Filtered Nerves ***
Copyright: Applied Topology Group at University of Bergen.
No commercial use of the software is permitted without proper license.
'''
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import gudhi
from itertools import cycle
# import matplotlib as mpl
import numbers
import warnings


def SimplexTreeFromNerve(
        nerve,
        coeff_field=11,
        min_persistence=0):

    nerve.get_filtered_nerve()
    geometric_dimension = max(
        (len(x) for x in nerve.maximal_faces)) - 1
    persistence_dim_max = geometric_dimension <= nerve.dimension

    simplex_tree = SimplexTree(
        interleaving_line=nerve.interleaving_line,
        coeff_field=coeff_field,
        persistence_dim_max=persistence_dim_max,
        min_persistence=min_persistence)

    for index, simplex in enumerate(nerve.nerve):
        simplex_tree.insert(simplex=list(simplex),
                            filtration=nerve.nerve_values[index])

    return simplex_tree


class SimplexTree(gudhi.SimplexTree):

    def __init__(self,
                 interleaving_line=None,
                 coeff_field=11,
                 persistence_dim_max=False,
                 truncation_value=np.inf,
                 min_persistence=0):
        super().__init__()
        if interleaving_line is None:
            interleaving_line = empty_interleaving_line
        self.interleaving_line = interleaving_line
        self.coeff_field = coeff_field
        self.persistence_dim_max = persistence_dim_max
        self.min_persistence = min_persistence
        self.truncation_value = truncation_value

    def sub_simplex_tree(self, vertex_set):
        st = SimplexTree()
        for simplex, value in self.get_filtration():
            if vertex_set.issuperset(simplex):
                st.insert(simplex, value)
        return st

    def persistent_homology(
            self,
            interleaving_line=None,
            coeff_field=None,
            persistence_dim_max=None,
            min_persistence=None):
        """
        Computing persistent homology using gudhi.

        Parameters
        ----------
        nerve : object of Nerve type

        Returns
        -------
        dgms : list of ndarrays
            One array for each dimension containing birth- and
            death values.
        """
        if interleaving_line is None:
            interleaving_line = self.interleaving_line
        elif not interleaving_line:
            interleaving_line = empty_interleaving_line
        if coeff_field is None:
            coeff_field = self.coeff_field
        if persistence_dim_max is None:
            persistence_dim_max = self.persistence_dim_max
        if min_persistence is None:
            min_persistence = self.min_persistence
        persistence = self.persistence(
            homology_coeff_field=coeff_field,
            min_persistence=min_persistence,
            persistence_dim_max=persistence_dim_max)
        max_homology_dimension = max(
            1, self.dimension() - 1 + persistence_dim_max)
        self.dgms = [[] for dim in range(max_homology_dimension + 1)]
        for hclass in persistence:
            birth = hclass[1][0]
            if birth >= self.truncation_value:
                continue
            death = min(self.truncation_value, hclass[1][1])
            if birth + np.finfo(np.float32).eps < death:
                self.dgms[hclass[0]].append((birth, death))
        self.dgms = [np.array(classes) for classes in self.dgms]
        for index, dgm in enumerate(self.dgms):
            if len(dgm) > 0:
                self.dgms[index] = dgm[np.argsort(dgm[:, 0] - dgm[:, 1])]
        self.dgms[0][0, 1] = np.inf
        return self.dgms

    def plot_persistence(
            self,
            plot_only=None,
            title=None,
            xy_range=None,
            labels=None,
            colormap="default",
            size=10,
            alpha=0.5,
            add_multiplicity=False,
            ax_color=np.array([0.0, 0.0, 0.0]),
            colors=None,
            diagonal=True,
            lifetime=False,
            legend=True,
            show=False,
            plot_infinity=True,
    ):
        """
        Show or save persistence diagram

        Parameters
        ----------
        plot_only: list of numeric
            If specified, an array of only the diagrams that should be plotted.
        title: string, default is None
            If title is defined, add it as title of the plot.
        xy_range: list of numeric [xmin, xmax, ymin, ymax]
            User provided range of axes. This is useful for comparing \
            multiple persistence diagrams.
        labels: string or list of strings
            Legend labels for each diagram. \
            If none are specified, we use H_0, H_1, H_2,... by default.
        colormap: str (default : 'default')
            Any of matplotlib color palettes. \
            Some options are 'default', 'seaborn', 'sequential'.
        size: numeric (default : 10)
            Pixel size of each point plotted.
        alpha: numeric (default : 0.5)
            Transparency of each point plotted.
        add_multiplicity: boolean (default : False)
            Show multiplicity of points plotted.
        ax_color: any valid matplotlib color type.
            See https://matplotlib.org/api/colors_api.html for complete API.
        diagonal: bool (default : True)
            Plot the diagonal x=y line.
        lifetime: bool (default : False). If True, diagonal is turned to False.
            Plot life time of each point instead of birth and death.  \
            Essentially, visualize (x, y-x).
        legend: bool (default : True)
            If true, show the legend.
        show: bool (default : False)
            Call plt.show() after plotting. If you are using self.plot() as \
            part of a subplot, set show=False and call plt.show() only once \
            at the end.
        """
        if not hasattr(self, 'dgms'):
            self.persistent_homology()
        if hasattr(self, 'interleaving_line'):
            interleaving_line = self.interleaving_line
        else:
            interleaving_line = empty_interleaving_line
        ax = plot_dgms(
            diagrams=self.dgms,
            interleaving_line=interleaving_line,
            plot_only=plot_only,
            title=title,
            xy_range=xy_range,
            labels=labels,
            colormap=colormap,
            size=size,
            ax_color=ax_color,
            colors=colors,
            diagonal=diagonal,
            lifetime=lifetime,
            legend=legend,
            show=show,
            alpha=alpha,
            add_multiplicity=add_multiplicity,
            plot_infinity=plot_infinity
        )
        return ax

    def components(self,
                   n_components=2,
                   persistence_function=None,
                   persistence_order='additive',
                   max_birth_value=np.inf):
        # if persistence_function in ['additive', 'multiplicative']:
        #     if n_components is None:
        #         persistence_function = 0
        #     if n_components is not None:
        #         zero_persistence = self.persistent_homology()[0]
        #         zero_persistence = zero_persistence[
        #             zero_persistence[:, 0] <= max_birth_value]
        #         if persistence_function == 'additive':
        #             n_components = min(n_components, len(zero_persistence) - 1)
        #             n_th_persistence_pair = zero_persistence[n_components - 1]
        #             persistence_function = n_th_persistence_pair[1] - \
        #                 n_th_persistence_pair[0]
        #         elif persistence_function == 'multiplicative':
        #             zero_persistence = zero_persistence[
        #                 zero_persistence[:, 0] > 0]
        #             n_components = min(n_components, len(zero_persistence) - 1)
        #             multiplicative_persistence = (
        #                 zero_persistence[:, 1] / zero_persistence[:, 0])
        #             zero_persistence = zero_persistence[
        #                 np.argsort(-multiplicative_persistence)]
        #             n_th_persistence_pair = zero_persistence[n_components - 1]
        #             persistence_function = multiplicative_persistence_function(
        #                 n_th_persistence_pair)
        persistence_function = set_persistence_function(
            simplex_tree=self,
            n_components=n_components,
            persistence_function=persistence_function,
            persistence_order=persistence_order,
            max_birth_value=max_birth_value)
        clusters = persistence_clusters(
            persistence_function=persistence_function,
            max_birth_value=max_birth_value,
            truncation_value=self.truncation_value,
            simplex_tree=self)
        if hasattr(self, 'nearest_point_list'):
            return [np.flatnonzero(
                np.isin(self.nearest_point_list,
                        cluster)) for cluster in clusters]
        else:
            return [np.array(cluster) for cluster in clusters]

    def graph(self):
        G = nx.Graph()
        for face, val in self.get_skeleton(1):
            if len(face) == 2:
                G.add_edge(*face, weight=val)
        return G

    def minimum_spanning_tree(self):
        return nx.minimum_spanning_tree(self.graph())

    def descend_clusters(
            self,
            n_components=2,
            persistence_function=None,
            persistence_order='additive',
            max_birth_value=np.inf):
        comps = self.components(
            n_components=n_components,
            persistence_function=persistence_function,
            persistence_order=persistence_order,
            max_birth_value=max_birth_value)

        G = self.minimum_spanning_tree()

        clusters = np.full(self.num_vertices(), -1)
        for index, comp in enumerate(comps[:-1]):
            clusters[list(comp)] = index

        for index in range(self.num_vertices()):
            if clusters[index] != -1:
                continue
            else:
                L = {index}
                while(1):
                    neighbors = list(G.neighbors(index))
                    parent = neighbors[
                        np.argmin([self.filtration([v])
                                   for v in neighbors])]
                    if clusters[parent] != -1:
                        clusters[list(L)] = clusters[parent]
                        break
                    else:
                        if parent not in L:
                            L.add(parent)
                            index = parent
                        else:
                            break
        return clusters

    def clusters(self,
                 n_components=2,
                 persistence_function=None,
                 persistence_order='additive',
                 max_birth_value=np.inf):
        components = self.descend_clusters(
            n_components=n_components,
            persistence_function=persistence_function,
            persistence_order=persistence_order,
            max_birth_value=max_birth_value)
        components = [set(np.flatnonzero(components == k)) for
                      k in range(n_components)]
        frontiers = components
        component_union = set.union(*frontiers)
        graph = self.graph()
        while any(frontiers):
            new_frontiers = []
            for index, frontier in enumerate(frontiers):
                new_frontier = grow_clusters(graph, frontier, component_union)
                new_frontiers.append(new_frontier)
                components[index] = components[index].union(new_frontier)
                component_union = component_union.union(new_frontier)
            frontiers = new_frontiers
        return [np.array(list(clust)) for clust in components]

    def persistence_points(self, persistence_function=0, depth=None):
        persistence_function = set_persistence_function(
            simplex_tree=self,
            persistence_function=persistence_function)

        clusters = depth_clusters(
            persistence_function=persistence_function,
            depth=depth,
            simplex_tree=self)
        if hasattr(self, 'nearest_point_list'):
            return [np.flatnonzero(
                np.isin(self.nearest_point_list,
                        cluster)) for cluster in clusters]
        else:
            return [np.array(cluster) for cluster in clusters]

    def cycle_components(self,
                         persistence_function=0,
                         persistence_order='additive',
                         max_birth_value=np.inf,
                         max_death_value=np.inf):
        persistence_function = set_persistence_function(
            simplex_tree=self,
            persistence_function=persistence_function)
        clusters = cycle_clusters(
            persistence_function=persistence_function,
            persistence_order=persistence_order,
            max_birth_value=max_birth_value,
            max_death_value=max_death_value,
            simplex_tree=self)
        if hasattr(self, 'nearest_point_list'):
            return [np.flatnonzero(
                np.isin(self.nearest_point_list,
                        cluster)) for cluster in clusters]
        else:
            return [np.array(cluster) for cluster in clusters]

    def cycle_representatives(
            self,
            persistence_function=0,
            max_birth_value=np.inf,
            max_death_value=np.inf,
            persistence_order='multiplicative',
            num_cycles=None):

        persistence_function = set_persistence_function(
            simplex_tree=self,
            persistence_function=persistence_function,
            persistence_order=persistence_order,
            max_birth_value=max_birth_value)
        cycles = representing_cycles(
            simplex_tree=self,
            persistence_function=persistence_function,
            max_death_value=max_death_value,
            max_birth_value=max_birth_value,
            persistence_order=persistence_order,
            num_cycles=num_cycles)
        return cycles
        # if not hasattr(self.nerve.DD, 'nearest_point_list'):
        #         self.nerve.DD.get_neares_point_list()
        # return [[self.nerve.truncation.point_sample[list(edge)] for
        #          edge in cycle] for cycle in cycles]


def plot_dgms(
        diagrams,
        interleaving_line=None,
        plot_only=None,
        title=None,
        xy_range=None,
        labels=None,
        colormap="default",
        size=10,
        ax_color=np.array([0.0, 0.0, 0.0]),
        colors=None,
        diagonal=True,
        lifetime=False,
        legend=True,
        show=False,
        alpha=0.5,
        ax=None,
        add_multiplicity=False,
        rips_dimension=None,
        plot_infinity=True
):
    # Originally from https://github.com/scikit-tda/ripser.py/blob/master/\
    # ripser/ripser.py
    xlabel, ylabel = "Birth", "Death"

    if labels is None:
        # Provide default labels for diagrams if using self.dgm_
        labels = [
            "$H_0$",
            "$H_1$",
            "$H_2$",
            "$H_3$",
            "$H_4$",
            "$H_5$",
            "$H_6$",
            "$H_7$",
            "$H_8$",
        ]

    if not isinstance(diagrams, list):
        # Must have diagrams as a list for processing downstream
        diagrams = [diagrams]
    if sum((len(dgm) for dgm in diagrams)) <= 1:
        print('Persistence diagram is empty!\n Nothing to plot.')
        return

    if plot_only is None:
        plot_only = [i for i, dgm in enumerate(diagrams) if len(dgm) > 0]
    if not isinstance(plot_only, list):
        plot_only = [plot_only]

    if not isinstance(labels, list):
        labels = [labels] * len(diagrams)

    if colors is None:
        # mpl.style.use(colormap)
        colors = cycle(["C0", "C1", "C2", "C3", "C4",
                        "C5", "C6", "C7", "C8", "C9"])
        colors = [next(colors) for i in range(len(diagrams))]
        colors = [colors[i] for i in plot_only]

    diagrams = [diagrams[i] for i in plot_only]
    labels = [labels[i] for i in plot_only]
    # Construct copy with proper type of each diagram
    # so we can freely edit them.
    diagrams = [dgm.astype(np.float32, copy=True) for dgm in diagrams]

    # find min and max of all visible diagrams
    concat_dgms = np.concatenate(diagrams).flatten()
    has_inf = np.any(np.isinf(concat_dgms))
    finite_dgms = concat_dgms[np.isfinite(concat_dgms)]

    if not plot_infinity:
        concat_dgms = finite_dgms
        has_inf = False

    if not xy_range:
        # define bounds of diagram
        ax_min, ax_max = np.min(finite_dgms), np.max(finite_dgms)
        x_r = ax_max - ax_min

        # Give plot a nice buffer on all sides.
        # ax_range=0 when only one point,
        buffer = 1 if xy_range == 0 else x_r / 5

        axx = ax_min - buffer / 2
        bx = ax_max + buffer

        ay, by = axx, bx
    else:
        axx, bx, ay, by = xy_range
    if max(axx - bx, ay - by) >= 0:
        warnings.warn("Warning: degenerate xy_range. Nothing to plot")
        return
    yr = by - ay

    if rips_dimension is not None:
        interleaving_line = rips_interleaving_line(rips_dimension, axx, bx)
    if interleaving_line() is not None:
        interleaving_line_lists = list(interleaving_line())
    else:
        interleaving_line_lists = None

    if ax is None:
        ax = plt.gca()

    if lifetime:

        # Don't plot landscape and diagonal at the same time.
        diagonal = False

        # reset y axis so it doesn't go much below zero
        ay = -yr * 0.05
        by = ay + yr

        # set custom ylabel
        ylabel = "Lifetime"

        # set diagrams to be (x, y-x)
        for dgm in diagrams:
            dgm[:, 1] -= dgm[:, 0]
        if interleaving_line_lists is not None:
            interleaving_line_lists[1] -= interleaving_line_lists[0]

        # plot horizon line
        ax.plot([axx, bx], [0, 0], c=ax_color)

    # Plot diagonal
    if diagonal:
        ax.plot([axx, bx], [axx, bx], "-", c=ax_color)

    # Plot interleaving line
    if interleaving_line_lists is not None:
        x_points, y_points = interleaving_line_lists
        # color="black", linewidth=1.0)
        ax.plot(x_points, y_points, "--", c=ax_color)

    # Plot inf line
    b_inf = ay + yr * 0.95
    if has_inf:
        # put inf line slightly below top

        ax.plot([axx, bx], [b_inf, b_inf], "--", c="k", label=r"$\infty$")

        # convert each inf in each diagram with b_inf
        for dgm in diagrams:
            dgm[np.isinf(dgm)] = b_inf

    plot_h0 = True
    if plot_h0:
        first_dgm = 0
    else:
        first_dgm = 1
    # Plot each diagram
    for dgm, color, label in list(zip(diagrams, colors, labels))[first_dgm:]:

        # plot persistence pairs
        ax.plot(dgm[:, 0], dgm[:, 1], ms=size, color=color,
                label=label, linestyle='',  # edgecolor="none",
                alpha=alpha, marker='o')

        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)

        if add_multiplicity:
            xy_arr, s_arr = np.unique(dgm, return_counts=True, axis=0)
            for xy, s in zip(xy_arr, s_arr):
                ax.text(xy[0], xy[1], s)

    ax.set_xlim([axx, bx])
    ax.set_ylim([ay, by])

    if title is not None:
        ax.set_title(title)

    if legend is True:
        ax.legend(loc="lower right")

    return ax


def rips_interleaving_line(n, ax, bx):
    def interleaving_line():
        dim_const = np.sqrt(2 * n / (n + 1))
        return np.array([ax, bx / dim_const]), np.array([ax, bx])
    return interleaving_line


def representing_cycles(simplex_tree,
                        persistence_function=0,
                        max_death_value=np.inf,
                        max_birth_value=np.inf,
                        persistence_order='additive',
                        num_cycles=None):

    if not hasattr(simplex_tree, 'dgms'):
        simplex_tree.persistent_homology()
    filtration_values = dict(
        (frozenset(face), value)
        for (face, value) in simplex_tree.get_skeleton(2))
    filtration_values[frozenset()] = np.inf
    birth_edges = []
    persistence_values = {}
    for pair in simplex_tree.persistence_pairs():
        if len(pair[0]) == 2:
            birth = frozenset(pair[0])
            death = frozenset(pair[1])
            persistence = filtration_values[death] - filtration_values[birth]
            if all(((filtration_values[death] >=
                     persistence_function(filtration_values[birth])),
                    filtration_values[birth] <= max_birth_value,
                    filtration_values[death] <= max_death_value)):
                birth_edges.append(list(birth))
                if persistence_order == 'multiplicative':
                    persistence_values[frozenset(list(birth))] = (
                        filtration_values[death] / filtration_values[birth])
                elif persistence_order == 'additive':
                    persistence_values[frozenset(list(birth))] = persistence
    subgraphs = []
    birth_edges.sort(key=lambda x: -persistence_values[frozenset(x)])
    edges = [list(face) + [value] for face, value in
             simplex_tree.get_filtration() if
             all((len(face) == 2, value <= max_death_value))]
    filtration_indices = dict(
        (frozenset(face[:2]), index) for (index, face) in
        enumerate(edges))
    for birth_edge in birth_edges[:num_cycles]:
        graph = nx.Graph()
        selected_edges = edges[
            :filtration_indices[frozenset(birth_edge)]]
        # selected_edges = [
        #     edge for edge in edges
        #     if filtration_values[frozenset(edge)] <
        #     filtration_values[frozenset(birth_edge)]]
        graph.add_weighted_edges_from(selected_edges)
        subgraphs.append(list(graph.subgraph(
            nx.shortest_path(
                graph, birth_edge[0], birth_edge[1])).edges()) +
            [birth_edge])
    return subgraphs


def cycle_clusters(persistence_function,
                   persistence_order,
                   max_birth_value,
                   max_death_value,
                   simplex_tree):
    if not hasattr(simplex_tree, 'dgms'):
        simplex_tree.persistent_homology()
    filtration_values = dict(
        (frozenset(face), value)
        for (face, value) in simplex_tree.get_skeleton(2))
    filtration_values[frozenset()] = np.inf
    birth_edges = []
    persistence_values = {}
    for pair in simplex_tree.persistence_pairs():
        if len(pair[0]) == 2:
            birth = frozenset(pair[0])
            death = frozenset(pair[1])
            persistence = filtration_values[death] - filtration_values[birth]
            if all((filtration_values[death] >=
                    persistence_function(filtration_values[birth]),
                    filtration_values[birth] <= max_birth_value)):
                birth_edges.append(list(birth))
                if persistence_order == 'multiplicative':
                    persistence_values[frozenset(list(birth))] = (
                        filtration_values[death] / filtration_values[birth])
                elif persistence_order == 'additive':
                    persistence_values[frozenset(list(birth))] = persistence
    birth_edges.sort(key=lambda x: -persistence_values[frozenset(x)])
    clusters = []
    edges = [list(face) for face, value in filtration_values.items() if
             all((len(face) == 2, value <= max_death_value))]
    for birth_edge in birth_edges:
        graph = nx.Graph()
        selected_edges = [
            edge for edge in edges
            if filtration_values[frozenset(edge)] <=
            filtration_values[frozenset(birth_edge)]]
        graph.add_edges_from(selected_edges)
        graph.add_nodes_from(birth_edge)
        clusters.append(
            list(nx.node_connected_component(graph, birth_edge[0])))
    return [cluster for cluster in clusters if len(cluster) > 1]


def depth_clusters(persistence_function,
                   depth,
                   simplex_tree):
    if depth is None:
        depth = persistence_function(0)
    if not hasattr(simplex_tree, 'dgms'):
        simplex_tree.persistent_homology()
    filtration_values = dict(
        (frozenset(face), value)
        for (face, value) in simplex_tree.get_skeleton(2))
    filtration_values[frozenset()] = np.inf
    birth_points = []
    persistence_values = {}
    for pair in simplex_tree.persistence_pairs():
        if len(pair[0]) == 1:
            birth = frozenset(pair[0])
            death = frozenset(pair[1])
            persistence = filtration_values[death] - filtration_values[birth]
            if (filtration_values[death] >=
                    persistence_function(filtration_values[birth])):
                birth_points.append(list(birth))
                persistence_values[frozenset(list(birth))] = persistence
    birth_points.sort(key=lambda x: -persistence_values[frozenset(x)])
    clusters = []
    edges = [list(face) for face in filtration_values.keys() if len(face) == 2]
    for birth_point in birth_points:
        graph = nx.Graph()
        selected_edges = [
            edge for edge in edges
            if filtration_values[frozenset(edge)] <=
            filtration_values[frozenset(birth_point)] + depth]
        graph.add_edges_from(selected_edges)
        graph.add_nodes_from(birth_point)
        clusters.append(
            list(nx.node_connected_component(graph, birth_point[0])))
    return clusters


def empty_interleaving_line(n_points=100):
    return None


def persistence_clusters(persistence_function,
                         max_birth_value,
                         truncation_value,
                         simplex_tree):
    if not hasattr(simplex_tree, 'dgms'):
        simplex_tree.persistent_homology()
    filtration_values = dict(
        (frozenset(face), value)
        for (face, value) in simplex_tree.get_skeleton(1) if
        value < truncation_value)
    filtration_values[frozenset()] = np.inf
    birth_points = []
    death_values = []
    birth_values = []
    for pair in simplex_tree.persistence_pairs():
        if len(pair[0]) == 1:
            birth = frozenset(pair[0])
            if birth not in filtration_values.keys():
                continue
            birth_value = filtration_values[birth]
            death = frozenset(pair[1])
            if death not in filtration_values.keys():
                death_value = truncation_value
            else:
                death_value = filtration_values[death]
            if (death_value >=
                persistence_function(birth_value) - np.finfo(np.float32).eps
                and
                    birth_value <= max_birth_value):
                birth_points.append(pair[0][0])
                birth_values.append(birth_value)
                death_values.append(death_value)
    cluster_dta = dict()
    bd_clusters = {birth_point: dict() for birth_point in birth_points}
    edges = [list(face) for face in filtration_values.keys() if len(face) == 2]
    # edges = [edge for edge in edges if
    #          max([filtration_values[
    #              frozenset({vertex})] for vertex in edge]) <=
    #          max_birth_value]
    for death_value in death_values:
        graph = nx.Graph()
        selected_edges = [
            edge for edge in edges
            if filtration_values[frozenset(edge)] < death_value]
        graph.add_edges_from(selected_edges)
        graph.add_nodes_from(birth_points)
        born_points = [birth_point for birth_point, birth_value in
                       zip(birth_points, birth_values) if
                       birth_value <= death_value]
        for birth_point in born_points:
            selected_cluster = frozenset(
                nx.node_connected_component(graph, birth_point))
            if selected_cluster in cluster_dta.keys():
                cluster_dta[selected_cluster].append(
                    (birth_point, death_value))
            else:
                cluster_dta[selected_cluster] = [
                    (birth_point, death_value)]
            bd_clusters[birth_point][death_value] = selected_cluster
    core_points = dict()
    for cluster, dta in cluster_dta.items():
        core_points[cluster] = frozenset(
            (birth_point for birth_point, death_value in dta))
    clusters = [bd_clusters[birth_point][max(
        (death_value for death_value, cluster in
         bd_clusters[birth_point].items() if
         len(core_points[cluster]) <= 1), default=np.inf)] for
        birth_point in birth_points]
    nodes = set([list(face)[0] for face in
                 filtration_values.keys() if len(face) == 1])
    outliers = nodes.difference(set.union(*[set(clust) for clust in clusters]))
    return [list(clust) for clust in clusters[::-1]] + [list(outliers)]


def multiplicative_persistence_function(persistence_pair, intercept):
    def persistence_function(t):
        return (t - intercept) * persistence_pair[1] / (
            persistence_pair[0] - intercept)
    return persistence_function


def set_persistence_function(
        simplex_tree,
        n_components=None,
        persistence_function=None,
        persistence_order='additive',
        max_birth_value=np.inf):

    if persistence_order not in ['additive', 'multiplicative']:
        raise ValueError(
            'persistence_order must be either `additive` or `multiplicative`')
    if n_components is not None:
        zero_persistence = simplex_tree.persistent_homology()[0]
        zero_persistence = zero_persistence[
            zero_persistence[:, 0] <= max_birth_value]
        n_components = min(n_components, len(zero_persistence))
        if persistence_order == 'additive':
            n_th_persistence_pair = zero_persistence[n_components - 1]
            persistence_function = n_th_persistence_pair[1] - \
                n_th_persistence_pair[0]
        elif persistence_order == 'multiplicative':
            multiplicative_persistence = (
                (zero_persistence[:, 0] - zero_persistence[0, 0]) /
                (zero_persistence[:, 1]))
            zero_persistence = zero_persistence[
                np.argsort(multiplicative_persistence)]
            n_th_persistence_pair = zero_persistence[n_components - 1]
            persistence_function = multiplicative_persistence_function(
                n_th_persistence_pair, zero_persistence[0, 0])
    elif persistence_function is None:
        persistence_function = 0

    if isinstance(persistence_function, numbers.Number):
        persistence_threshold = persistence_function

        def persistence_function(t):
            return t + persistence_threshold

    return persistence_function


def homotopy_symmetric_difference(X, Y):
    '''
    Mapping cone of union of X and Y by intersection
    of X and Y

    Parameters
    ----------
    X : SimplexTree
    Y : SimplexTree

    Returns
    -------

    Z : SimplexTree
        The mapping cone of union of X and Y by intersection
        of X and Y
    '''
    last_vertex = max(X.num_vertices(), Y.num_vertices())
    Z = SimplexTree()
    for simplex, value in X.get_filtration():
        if Y.find(simplex):
            Z.insert(simplex + [last_vertex],
                     max(value, Y.filtration(simplex)))
            Z.insert(simplex,
                     min(value, Y.filtration(simplex)))
        else:
            Z.insert(simplex, value)
    for simplex, value in Y.get_filtration():
        if X.find(simplex):
            continue
        else:
            Z.insert(simplex, value)
    return Z


def grow_clusters(graph, frontier, component_union):
    new_frontier = set()
    for node in frontier:
        for neigh in graph.neighbors(node):
            if neigh not in component_union:
                new_frontier.add(neigh)
    return new_frontier
