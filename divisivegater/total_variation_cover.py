"""Principal Direction Voronoi Cover

Input: A point cloud X in euclidean space

Method: The set X is divided iteratively.

Division:
    The principal direction of X is found
    and X is split so that the sums of variations
    along this direction is smallest possible.
"""

# Authors: Morten Brun <morten.brun@uib.no>
# License:
import itertools
import collections
import operator
import functools
import math
import numpy as np
from scipy.spatial.distance import cdist, squareform
from scipy.spatial import Delaunay
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import MiniBatchKMeans
from miniball import Miniball
from sklearn.decomposition import PCA
import networkx as nx
# from scipy.spatial import Delaunay
from . simplex_tree import SimplexTree
from . basic_cover import BasicCover
from KDEpy import FFTKDE
###############################################################################
# Patch class


def dtm_patch_distance(
        percentile=None,
        upper_percentile=1.,
        lower_percentile=0.,
        k=100,
        p=2):
    '''
    Sum of squared distances to barycenter.
    '''
    if percentile is not None:
        lower_percentile = 0
        upper_percentile = percentile

    def fct(X, face, patch_list):
        if len(face) == 1:
            return np.inf
        patches = [X[patch_list[patch_index].point_indices]
                   for patch_index in face]
        return patch_dtm(
            patches, lower_percentile, upper_percentile, k, p)
    return fct


def patch_dtm(patches, lower_percentile, upper_percentile, k, p):

    barycenters = [np.mean(patch, axis=0)[np.newaxis, :] for patch in patches]
    mb = Miniball(np.vstack(barycenters))
    # barycenter = np.mean(np.vstack(barycenters), axis=0)[np.newaxis, :]
    return max(dtm(np.array([mb.center()]), patch,
                   lower_percentile, upper_percentile, k, p)
               for patch in patches)


def dtm(center, patch, lower_percentile, upper_percentile, k, p):
    k = max(k, int(lower_percentile * len(patch)))
    k = min(k, math.ceil(upper_percentile * len(patch)))
    dists = cdist(center, patch).flatten()
    dists = np.partition(dists, k-1)[:k]
    return np.mean(dists ** p) ** (1/p)


def distance_to_measure_patch_distance(
        max_num_nearby_points,
        min_num_nearby_points,
        percentile=0.2,
        p=2):
    '''
    Sum of squared distances to barycenter.
    '''
    def fct(X, face, patch_list):
        if len(face) == 1:
            return np.inf
        patches = [X[patch_list[patch_index].point_indices]
                   for patch_index in face]
        numbers_of_points = [math.ceil(percentile * len(patch))
                             for patch in patches]
        numbers_of_points = [min(num_pts, max_num_nearby_points) for num_pts
                             in numbers_of_points]
        numbers_of_points = [max(num_pts, min_num_nearby_points) for num_pts
                             in numbers_of_points]
        numbers_of_points = [min(num_pts, len(patch)) for
                             num_pts, patch in zip(numbers_of_points, patches)]
        # number_of_points = min(numbers_of_points)
        return patch_distance_to_measure(
            list(zip(patches, numbers_of_points)), p)
    return fct


def patch_distance_to_measure(patch_data, p):

    patch_data = nearest_points(patch_data)
    barycenter = np.mean(
        np.vstack([patch for patch, weight in patch_data]),
        axis=0)[np.newaxis, :]
    # barycenter = np.sum(
    #     np.vstack([patch / len(patch) for patch, weight in patch_data]),
    #     axis=0)[np.newaxis, :] / len(patch_data)
    patches = list(zip(*patch_data))[0]
    return max(distance_to_measure(barycenter, patch, p) for patch in patches)


def distance_to_measure(barycenter, patch, p):
    return (np.sum(cdist(barycenter, patch) ** p) / len(patch)) ** (1 / p)


def nearest_points(patch_data):
    while any(len(patch) > number_of_points
              for patch, number_of_points in patch_data):
        patch_data = bisect_nearer(patch_data)
    return patch_data
    # return list(A) + list(B)


def bisect_nearer(patch_data):
    barycenter = np.mean(
        np.vstack(
            [patch for patch, weight in patch_data]), axis=0)[np.newaxis, :]

    for index, (patch, number_of_points) in enumerate(patch_data):
        if len(patch) > number_of_points:
            patch_data[index] = (patch[np.argpartition(cdist(
                patch, barycenter).flatten(), number_of_points)[
                    :max(len(patch) // 2, number_of_points)]],
                number_of_points)
    return patch_data


def square_distance_to_measure(point, X, k):
    k = min(k, len(X))
    dists = cdist([point], X,
                  metric='sqeuclidean').flatten()
    return np.sum(np.partition(dists, k)[:k]) / k


def second_moment_sum(cover, point_indices):
    '''
    This cost is (weighted) total variation
    '''
    # p = 20
    # k = 5
    # k = min(k, len(point_indices) - 1)
    # barycenter = np.mean(cover.X[point_indices], axis=0)[np.newaxis, :]
    # dists = cdist(barycenter, cover.X[point_indices])
    # dists = np.partition(dists, k)[:k]
    # return np.mean(dists**p)**(1/p)
    # radius = np.max(dists)
    # return (radius**2) * np.log(len(point_indices))
    n_components = 1  # min(3, len(point_indices))
    pca = PCA(n_components=n_components)
    pca.fit(cover.X[point_indices])
    return np.prod(
        pca.explained_variance_)
    # return len(point_indices) * np.prod(
    #     pca.explained_variance_ratio_ * pca.explained_variance_)


class KMeansKDEPatch(object):
    """Patch object
    """

    def __init__(
            self,
            cover,
            point_indices,
    ):
        self.point_indices = point_indices
        self.cover = cover
        if len(point_indices) == 0:
            return
        elif len(point_indices) <= self.cover.min_patch_size:
            self.cost_value = -1
        else:
            self.cost_value = second_moment_sum(
                self.cover, point_indices)
        self.barycenter = self.set_center()
        # if self.cost_value < cover.min_cost_value:
        #     self.cost_value = -1
        # if len(point_indices) < cover.min_patch_size:
        #     self.cost_value = -1

    def set_center(self):
        return np.mean(self.cover.X[self.point_indices],
                       axis=0)

    def center_split(self, centers):
        direction = centers[1] - centers[0]
        X = self.cover.X
        point_indices = self.point_indices
        x = np.dot(X[point_indices], direction)
        # sort by principal direction
        # order = np.argsort(x)
        # inverse_order = np.arange(len(order))[np.argsort(order)]
        # x = x[order]

        scaler = StandardScaler()
        z = scaler.fit_transform(x[:, np.newaxis]).flatten()
        transformed_centers = np.sort(scaler.transform(
            np.dot(centers, direction)[:, np.newaxis]).flatten())
        xx, yy = FFTKDE(
            kernel='gaussian', bw=self.cover.bandwidth).fit(z).evaluate()

        left_max = np.argmin(xx < transformed_centers[0])
        right_max = np.argmin(xx < transformed_centers[1])
        split_index = left_max + np.argmin(yy[left_max:right_max + 1])
        left_patch_indices = np.flatnonzero(z < xx[split_index])
        right_patch_indices = np.flatnonzero(z >= xx[split_index])
        children = (KMeansKDEPatch(cover=self.cover,
                                   point_indices=point_indices[
                                       left_patch_indices]),
                    KMeansKDEPatch(cover=self.cover,
                                   point_indices=point_indices[
                                       right_patch_indices],))
        # children = [child for child in children if
        #             len(child.point_indices) >= self.cover.min_patch_size]
        # if len(children) == 1:
        #     children[0].cost_value = -1
        return children

    def split(self):
        '''
        Split data by hyperplane orthogonal to principal
        direction so that each part contains the same number of
        points.
        '''
        kmeans = MiniBatchKMeans(
            n_clusters=2, random_state=self.cover.minibatch_random_state)
        X = self.cover.X
        point_indices = self.point_indices
        kmeans.fit(X[point_indices])
        centers = kmeans.cluster_centers_
        return self.center_split(centers=centers)


class StandardDeviationCover(BasicCover):
    """Standard Deviation Cover

    """

    def __init__(self,
                 n_patches=10,
                 percentile=0.1,
                 min_patch_size=1,
                 coeff_field=11,
                 min_persistence=0,
                 dimension=None,
                 simplex_batch_size=10000,
                 truncation_value=np.inf,
                 bandwidth=0.6,
                 distance_fct=None,
                 max_num_nearby_points=1000,
                 min_num_nearby_points=1,
                 p=2,
                 minibatch_random_state=None,
                 patch_class=KMeansKDEPatch,
                 svd_solver='auto'):
        self.n_patches = n_patches
        self.percentile = percentile
        self.min_patch_size = min_patch_size
        self.coeff_field = coeff_field
        self.bandwidth = bandwidth
        self.max_num_nearby_points = max_num_nearby_points
        self.min_persistence = min_persistence
        self.dimension = dimension
        self.minibatch_random_state = minibatch_random_state
        # self.min_cost_value_factor = min_cost_value_factor
        if distance_fct is None:
            distance_fct = distance_to_measure_patch_distance(
                max_num_nearby_points=max_num_nearby_points,
                min_num_nearby_points=min_num_nearby_points,
                percentile=percentile,
                p=p)
        self.distance_function = distance_fct
        self.filtration_value_fct = self.distance_function
        self.svd_solver = svd_solver
        self.simplex_batch_size = simplex_batch_size
        self.patch = patch_class
        self.truncation_value = truncation_value

    def compute_cover(self,
                      X=None,
                      # min_cost_value_factor=None,
                      n_patches=None):
        if all((not hasattr(self, 'X'),
                X is None)):
            raise ValueError('X has to be specified')
        if all((hasattr(self, 'cover'),
                X is None,
                n_patches is None)):
            return
        self.__dict__.pop('nerve', None)
        self.__dict__.pop('simplex_tree', None)
        self.__dict__.pop('patch_graph', None)
        if n_patches is not None:
            self.n_patches = n_patches
        if X is None:
            X = self.X
        self.X = X
        # self.X = X[:, np.min(X, axis=0) < np.max(X, axis=0)]
        self.minX = np.min(self.X, axis=0)
        self.maxX = np.max(self.X, axis=0)
        if self.X.shape[1] == 0:
            raise ValueError('X has to be non-degenerate')
        # if min_cost_value_factor is None:
        #     min_cost_value_factor = self.min_cost_value_factor
        # self.min_cost_value = (
        #     self.min_cost_value_factor * second_moment_sum(self.X))
        root_patch = self.patch(
            cover=self,
            point_indices=np.arange(len(self.X)))
        self.patch_list = [root_patch]

        while len(self.patch_list) < self.n_patches:
            cost_values = np.array(
                [patch.cost_value for patch in self.patch_list])
            next_patch = np.argmax(cost_values)
            if cost_values[next_patch] <= 0:
                break
            patch = self.patch_list.pop(next_patch)
            self.patch_list.extend(patch.split())
            # [patch for patch in patch.split() if
            #  len(patch.point_indices) >= self.min_patch_size])

        self.cover = self.patch_list
        # self.cover = [patch for patch in self.patch_list if
        #               len(patch.point_indices) >= self.min_patch_size]

    def set_distance_matrix(
            self,
            X=None,
            truncation_value=None,
            n_patches=None,
            dimension=None):

        if X is not None:
            self.X = X
        if not hasattr(self, 'dimension'):
            self.dimension = self.X.shape[1] - 1
        if truncation_value is not None:
            self.trunction_value = truncation_value

        if dimension is not None:
            self.dimension = dimension
        if not hasattr(self, 'cover') or not all(
                (X is None, n_patches is None)):
            self.compute_cover(
                X=X, n_patches=n_patches)

        self.distmat = patch_distance_matrix(
            self.X, self.cover, self.distance_function)

    def set_simplex_tree(
            self,
            X=None,
            truncation_value=None,
            n_patches=None,
            dimension=None):

        if hasattr(self, 'simplex_tree'):
            if all((X is None,
                    n_patches is None,
                    dimension is None,
                    truncation_value is None)):
                return
        if not all((
                hasattr(self, 'distmat'),
                X is None,
                n_patches is None,
                dimension is None,
                truncation_value is None)):
            self.set_distance_matrix(
                X=X,
                truncation_value=truncation_value,
                n_patches=n_patches,
                dimension=dimension)

        # filtmat = np.zeros_like(self.distmat)
        # cardmat = np.empty_like(self.distmat, dtype=int)
        # for i, p in enumerate(self.cover):
        #     for j, q in enumerate(self.cover):
        #         cardmat[i, j] = min(len(p.point_indices), len(q.point_indices))
        #         cardmat[j, i] = cardmat[i, j]
        # filtmat[self.distmat < self.truncation_value] = self.distmat[
        #     self.distmat < self.truncation_value]
        # filtmat = filtmat / cardmat
        #     filt_trunc_value = np.log(self.truncation_value)
        # else:
        #     filt_trunc_value = self.truncation_value
        # filt_trunc_value = np.max(filtmat)

        self.simplex_tree = simplex_tree_from_patch_list(
            patch_list=self.cover,
            X=self.X,
            filtration_function=self.filtration_value_fct,
            filtmat=self.distmat,
            truncation_value=self.truncation_value,
            coeff_field=self.coeff_field,
            min_persistence=self.min_persistence,
            simplex_batch_size=self.simplex_batch_size,
            dimension=self.dimension)


def patch_distance_matrix(X,
                          cover,
                          distance_function):
    distances = []
    for index1 in range(len(cover)):
        for index2 in range(index1 + 1, len(cover)):
            face = [index1, index2]
            distances.append(distance_function(
                X,
                face=face,
                patch_list=cover))

    distances = squareform(distances)
    np.fill_diagonal(distances, np.inf)

    return distances


def truncation_distance_matrix(X, cover):
    distmat = directed_truncation_distance_matrix(X, cover)
    return np.maximum(distmat, distmat.transpose())


def directed_truncation_distance_matrix(X, cover):
    distmat = np.empty((len(cover), len(cover)))

    for index1, patch1 in enumerate(cover):
        for index2, patch2 in enumerate(cover):
            if index1 == index2:
                distmat[index1, index2] = np.inf
            else:
                patch2 = cover[index2]
                distmat[index1, index2] = (
                    np.min(cdist([patch1.barycenter],
                                 X[patch2.point_indices])) -
                    np.max(cdist([patch1.barycenter],
                                 X[patch1.point_indices])))
    return distmat


def enumerate_cliques(G, max_cardinality=None):
    """Returns cliques of cardinality up to max_cardinality
    in an undirected graph.

    This method returns cliques of size (cardinality)
    k = 1, 2, 3, ..., max_cardinality.

    For now I require the nodes of G to be consequetive integers.

    Parameters
    ----------
    G: undirected graph
    max_cardinality: int

    Returns
    -------
    generator of lists: generator of list for each clique.

    Notes
    -----
    To obtain a list of all cliques, use
    :samp:`list(enumerate_all_cliques(G))`.

    Based on the algorithm published by Zhang et al. (2005) [1]_
    and adapted to output all cliques discovered.

    This algorithm is not applicable on directed graphs.

    This algorithm ignores self-loops and parallel edges as
    clique is not conventionally defined with such edges.

    There are often many cliques in graphs.
    This algorithm however, hopefully, does not run out of memory
    since it only keeps candidate sublists in memory and
    continuously removes exhausted sublists.

    References
    ----------
    .. [1] Yun Zhang, Abu-Khzam, F.N., Baldwin, N.E., Chesler, E.J.,
           Langston, M.A., Samatova, N.F.,
           Genome-Scale Computational Approaches to Memory-Intensive
           Applications in Systems Biology.
           Supercomputing, 2005. Proceedings of the ACM/IEEE SC 2005
           Conference, pp. 12, 12-18 Nov. 2005.
           doi: 10.1109/SC.2005.29.
           http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=1559964&isnumber=33129
    """
    if max_cardinality is None:
        max_cardinality = G.number_of_nodes()
    index = {}
    nbrs = {}
    for u in G:
        index[u] = len(index)
        # Neighbors of u that appear after u in the iteration order of G.
        nbrs[u] = {v for v in G[u] if v not in index}

    queue = collections.deque(
        ([u], sorted(nbrs[u], key=index.__getitem__)) for u in G)
    # Loop invariants:
    # 1. len(base) is nondecreasing.
    # 2. (base + cnbrs) is sorted with respect to the iteration order of G.
    # 3. cnbrs is a set of common neighbors of nodes in base.
    while queue:
        base, cnbrs = map(list, queue.popleft())
        yield base
        if len(base) < max_cardinality:
            for i, u in enumerate(cnbrs):
                # Use generators to reduce memory consumption.
                queue.append((itertools.chain(base, [u]),
                              filter(nbrs[u].__contains__,
                                     itertools.islice(cnbrs, i + 1, None))))


def approximate_maximal_faces(X, patch_list, simplex_batch_size=10000):
    '''(Approximate) Delaunay maximal faces of barycenters
       More precisely strong Witness complex as in Carlsson
       Topology and Data.
    '''
    if X.shape[1] < 8:
        max_faces = delaunay_maximal_faces(X=X, patch_list=patch_list)
    barycenters = np.vstack([
        np.mean(X[patch.point_indices], axis=0) for patch in patch_list])
    max_faces = set()
    for index, x in enumerate(X):
        if index % simplex_batch_size:
            max_faces = set(eliminate_subsets(max_faces))
        dists = cdist([x], barycenters).flatten()
        face = frozenset(
            np.flatnonzero(dists <= np.min(dists)))
        if len(face) > X.shape[1] + 1:
            face = frozenset(
                np.argpartition(dists, X.shape[1])[:X.shape[1]])
        # elif len(face) <= X.shape[1]:

        max_faces.add(face)
    return eliminate_subsets(max_faces)


def delaunay_maximal_faces(X, patch_list):
    '''Delaunay maximal faces of barycenters
    '''
    barycenters = np.vstack([
        np.mean(X[patch.point_indices], axis=0) for patch in patch_list])
    tri = Delaunay(barycenters)
    return set(frozenset(face) for face in tri.simplices)


def get_nerve_from_maximal_faces(maximal_faces,
                                 dimension):
    '''Returns nerve given a list of maximal faces
    Parameters
    -----------
    maximal_face :
    dimension : int
        Maximal homology dimension
    Returns
    -----------
    nerve : ndarray of lists of integers
        List of all simplices up to desired dimension
    '''
    # This needs to be optimized.
    # First we find the maximal faces of the dimension + 1 skeleton.
    # Then we fill in the rest of the faces.
    faces = ((frozenset(face)
              for face in
              powerset(maximal_face,
                       max_card=dimension + 2,
                       min_card=min(len(maximal_face),
                                    dimension + 2)))
             for maximal_face in maximal_faces)
    faces = frozenset(
        itertools.chain.from_iterable(faces))
    faces = ((frozenset(face)
              for face in
              powerset(maximal_face,
                       max_card=dimension + 2))
             for maximal_face in faces)
    return np.array(list(frozenset(
        itertools.chain.from_iterable(faces))))


def powerset(some_set, max_card, min_card=1):
    return itertools.chain(*[
        list(itertools.combinations(some_set, dim + 1))
        for dim in range(min_card - 1, max_card)])


def simplex_tree_from_patch_list(
        patch_list,
        X,
        filtration_function,
        filtmat,
        truncation_value,
        coeff_field=11,
        min_persistence=0,
        simplex_batch_size=10000,
        dimension=None):

    if dimension is None:
        dimension = X.shape[1] - 1

    geometric_dimension = X.shape[1]
    persistence_dim_max = geometric_dimension <= dimension
    if persistence_dim_max:
        print('strange thing')
    simplex_tree = SimplexTree(
        coeff_field=coeff_field,
        persistence_dim_max=persistence_dim_max,
        truncation_value=truncation_value,
        min_persistence=min_persistence)
    if dimension == 0:
        nerve = minimum_spanning_tree(filtmat)
    else:
        nerve = get_nerve_from_maximal_faces(
            delaunay_maximal_faces(X, patch_list), dimension=dimension)
    for face in nerve:
        if len(face) == 1:
            simplex_tree.insert(
                simplex=list(face), filtration=truncation_value)
        else:
            weight = filtration_function(
                X,
                face,
                patch_list)
            if weight <= truncation_value:
                simplex_tree.insert(
                    simplex=list(face),
                    filtration=weight)
    return simplex_tree


def minimum_spanning_tree(filtmat):
    filtmat = filtmat.copy()
    np.fill_diagonal(filtmat, 0)
    G = nx.from_numpy_array(filtmat)
    T = nx.minimum_spanning_tree(G)
    return [(node, ) for node in T.nodes] + [edge for edge in T.edges]


def is_power_of_two(n):
    """Returns True iff n is a power of two.  Assumes n > 0."""
    return (n & (n - 1)) == 0


def eliminate_subsets(sequence_of_sets):
    """I did not write this. Far too clever for me.
    Return a list of the elements of `sequence_of_sets`, removing all
    elements that are subsets of other elements.  Assumes that each
    element is a set or frozenset."""
    # The code below does not handle the case of a sequence containing
    # only the empty set, so let's just handle all easy cases now.
    sequence_of_sets = list(frozenset(sequence_of_sets))
    if len(sequence_of_sets) <= 1:
        return list(sequence_of_sets)
    # We need an indexable sequence so that we can use a bitmap to
    # represent each set.
    if not isinstance(sequence_of_sets, collections.Sequence):
        sequence_of_sets = list(sequence_of_sets)
    # For each element, construct the list of all sets containing that
    # element.
    sets_containing_element = {}
    for i, s in enumerate(sequence_of_sets):
        for element in s:
            try:
                sets_containing_element[element] |= 1 << i
            except KeyError:
                sets_containing_element[element] = 1 << i
    # For each set, if the intersection of all of the lists in which it is
    # contained has length != 1, this set can be eliminated.
    out = [s for s in sequence_of_sets
           if s and is_power_of_two(functools.reduce(
               operator.and_, (sets_containing_element[x] for x in s)))]
    return out
