# name = 'divisivegater'
from . import simplex_tree
from . import cover
from . import spectralcover
from . import gater
from . import gater_std
# from . import umapgater
from . import voronoi_cover
from . import kmeans_cover
from . import basic_cover
#from . import standard_deviation_cover
from . import total_variation_cover
from . import weighted_total_variation_cover
from . import patch_persistence
from . import utils
from . import gabriel_persistence
