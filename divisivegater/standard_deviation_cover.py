"""Principal Direction Voronoi Cover

Input: A point cloud X in euclidean space

Method: The set X is divided iteratively.

Division:
    The principal direction of X is found
    and X is split so that the sums of variations
    along this direction is smallest possible.
"""

# Authors: Morten Brun <morten.brun@uib.no>
# License:
import numpy as np
from scipy.spatial.distance import cdist, squareform
import itertools
from miniball import Miniball
# from scipy.spatial import Delaunay
from . simplex_tree import SimplexTree
from KDEpy import FFTKDE
from sklearn.preprocessing import StandardScaler
from . basic_cover import get_principal_directions, BasicCover
import collections
import operator
import functools
import math
from scipy.spatial import Delaunay
from sklearn.cluster import MiniBatchKMeans
import networkx as nx
###############################################################################
# Patch class


def percentile_patch_distance(isolated_points,
                              percentile=0.8):
    def fct(X, face, patch_list):
        if len(face) == 1:
            if isolated_points:
                patch = patch_list[list(face)[0]].point_indices
                barycenter = np.mean(X[patch], axis=0)
                dists = cdist([barycenter], X[patch]).flatten()
                number_of_points = math.ceil(percentile * len(patch))
                number_of_points = max(1, number_of_points)
                mb = Miniball(X[patch[
                    np.argpartition(dists, number_of_points-1)[
                        :number_of_points]]])
                return np.sqrt(mb.squared_radius())
            else:
                return np.inf
        patches = [X[patch_list[patch_index].point_indices]
                   for patch_index in face]
        numbers_of_points = [
            math.ceil(percentile * len(patch)) for
            patch in patches]
        return patch_distance(patches,
                              numbers_of_points)
    return fct


class KMeansPatch(object):
    """Patch object
    """

    def __init__(
            self,
            cover,
            point_indices
    ):
        self.point_indices = point_indices
        self.cover = cover
        self.cost_value = np.linalg.norm(
            np.std(self.cover.X[point_indices], axis=0))
        self.barycenter = self.set_center()
        if self.cost_value < cover.min_cost_value:
            self.cost_value = -1
        # if len(point_indices) < cover.min_patch_size:
        #     self.cost_value = -1

    def set_center(self):
        return np.mean(self.cover.X[self.point_indices], axis=0)

    def split(self):
        '''
        Split data by hyperplane orthogonal to principal
        direction so that each part contains the same number of
        points.
        '''
        kmeans = MiniBatchKMeans(n_clusters=2)
        X = self.cover.X
        point_indices = self.point_indices
        classes = kmeans.fit_predict(X[point_indices])
        return [KMeansPatch(cover=self.cover,
                            point_indices=point_indices[
                                np.flatnonzero(classes == 0)]),
                KMeansPatch(cover=self.cover,
                            point_indices=point_indices[
                                np.flatnonzero(classes == 1)])]


class KDEPatch(KMeansPatch):
    """Patch object
    """

    def split(self):
        '''
        Split data by hyperplane orthogonal to principal
        direction so that each part contains the same number of
        points.
        '''
        point_indices = self.point_indices
        # find coordinates in principal direction
        principal_direction = get_principal_directions(
            self.cover.X[point_indices],
            svd_solver=self.cover.svd_solver).flatten()
        x = np.dot(self.cover.X[point_indices], principal_direction)
        # sort by principal direction
        order = np.argsort(x)
        # inverse_order = np.arange(len(order))[np.argsort(order)]
        x = x[order]

        scaler = StandardScaler()
        z = scaler.fit_transform(x[:, np.newaxis]).flatten()
        xx, yy = FFTKDE(kernel='gaussian', bw='silverman').fit(z).evaluate()

        split_index = len(xx) // 2
        left_max = np.argmax(yy[:split_index])
        right_max = split_index + np.argmax(yy[split_index:])
        split_index = left_max + np.argmin(yy[left_max:right_max + 1])

        # diff = np.diff(yy)

        # split_indices = np.array(
        #     [split_index - np.argmax(diff[:split_index][::-1] < 0),
        #      split_index + np.argmax(diff[split_index:] >= 0)], dtype=int)
        # split_index = split_indices[
        #     np.argmin(yy[split_indices])]

        split = np.argmin(z < xx[split_index])
        if split in [0, len(z)]:
            split_index = np.argmax(yy)
            split = np.argmin(z < xx[split_index])
            print('unbreakable', len(z))
        return [KDEPatch(cover=self.cover,
                         point_indices=point_indices[order[:split]]),
                KDEPatch(cover=self.cover,
                         point_indices=point_indices[order[split:]])]


class KMeansKDEPatch(KMeansPatch):
    """Patch object
    """

    def split(self):
        '''
        Split data by hyperplane orthogonal to principal
        direction so that each part contains the same number of
        points.
        '''
        kmeans = MiniBatchKMeans(n_clusters=2)
        X = self.cover.X
        point_indices = self.point_indices
        kmeans.fit(X[point_indices])
        centers = kmeans.cluster_centers_
        principal_direction = centers[1] - centers[0]
        x = np.dot(X[point_indices], principal_direction)
        # sort by principal direction
        order = np.argsort(x)
        # inverse_order = np.arange(len(order))[np.argsort(order)]
        x = x[order]

        scaler = StandardScaler()
        z = scaler.fit_transform(x[:, np.newaxis]).flatten()
        xx, yy = FFTKDE(kernel='gaussian', bw='silverman').fit(z).evaluate()

        split_index = len(xx) // 2
        left_max = np.argmax(yy[:split_index])
        right_max = split_index + np.argmax(yy[split_index:])
        split_index = left_max + np.argmin(yy[left_max:right_max + 1])

        split = np.argmin(z < xx[split_index])
        if split in [0, len(z)]:
            split_index = np.argmax(yy)
            split = np.argmin(z < xx[split_index])
            print('unbreakable', len(z))
        return [KMeansKDEPatch(cover=self.cover,
                               point_indices=point_indices[order[:split]]),
                KMeansKDEPatch(cover=self.cover,
                               point_indices=point_indices[order[split:]])]


class StandardDeviationCover(BasicCover):
    """Standard Deviation Cover

    """

    def __init__(self,
                 n_patches=10,
                 percentile=0.1,
                 min_patch_size=10,
                 min_cost_value_factor=0.1,
                 coeff_field=11,
                 min_persistence=0,
                 dimension=None,
                 simplex_batch_size=10000,
                 isolated_points=False,
                 n_neighbors=5.1,
                 truncation_value=None,
                 log_filtration=True,
                 distance_fct=None,
                 patch_class=KDEPatch,
                 svd_solver='auto'):
        self.isolated_points = isolated_points
        self.n_patches = n_patches
        self.percentile = percentile
        self.min_patch_size = min_patch_size
        self.coeff_field = coeff_field
        self.min_persistence = min_persistence
        self.dimension = dimension
        self.min_cost_value_factor = min_cost_value_factor
        if distance_fct is None:
            distance_fct = percentile_patch_distance(
                isolated_points=isolated_points,
                percentile=percentile)
        self.distance_function = distance_fct
        if log_filtration:
            self.filtration_value_fct = log_filtration_value_function(
                self.distance_function)
        else:
            self.filtration_value_fct = self.distance_function
        self.svd_solver = svd_solver
        self.simplex_batch_size = simplex_batch_size
        self.Patch = patch_class
        self.n_neighbors = n_neighbors
        self.truncation_value = truncation_value
        self.log_filtration = log_filtration

    def compute_cover(self,
                      X=None,
                      min_cost_value_factor=None,
                      n_patches=None):
        if all((not hasattr(self, 'X'),
                X is None)):
            raise ValueError('X has to be specified')
        if all((hasattr(self, 'cover'),
                X is None,
                n_patches is None)):
            return
        self.__dict__.pop('nerve', None)
        self.__dict__.pop('simplex_tree', None)
        self.__dict__.pop('patch_graph', None)
        if n_patches is not None:
            self.n_patches = n_patches
        if X is None:
            X = self.X

        self.X = X[:, np.min(X, axis=0) < np.max(X, axis=0)]
        self.minX = np.min(self.X, axis=0)
        self.maxX = np.max(self.X, axis=0)
        if self.X.shape[1] == 0:
            raise ValueError('X has to be non-degenerate')
        if min_cost_value_factor is None:
            min_cost_value_factor = self.min_cost_value_factor
        self.min_cost_value = self.min_cost_value_factor * np.linalg.norm(
            np.std(self.X, axis=0))
        root_patch = self.Patch(
            cover=self,
            point_indices=np.arange(len(self.X)))
        self.patch_list = [root_patch]

        while len(self.patch_list) < self.n_patches:
            cost_values = np.array(
                [patch.cost_value for patch in self.patch_list])
            next_patch = np.argmax(cost_values)
            if cost_values[next_patch] <= 0:
                break
            patch = self.patch_list.pop(next_patch)
            self.patch_list.extend(patch.split())
            # [patch for patch in patch.split() if
            #  len(patch.point_indices) >= self.min_patch_size])

        self.cover = self.patch_list

    def set_simplex_tree(
            self,
            X=None,
            truncation_value=None,
            n_patches=None,
            dimension=None):
        if X is not None:
            self.X = X
        if not hasattr(self, 'dimension'):
            self.dimension = self.X.shape[1] - 1
        if truncation_value is not None:
            self.trunction_value = truncation_value
        if hasattr(self, 'simplex_tree'):
            if all((X is None,
                    n_patches is None,
                    dimension is None,
                    truncation_value is None)):
                return
        if dimension is not None:
            self.dimension = dimension
        if not hasattr(self, 'patch_list') or not all(
                (X is None, n_patches is None)):
            self.compute_cover(
                X=X, n_patches=n_patches)

        self.distmat = patch_distance_matrix(
            self.X, self.cover, self.distance_function, self.isolated_points)

        if self.truncation_value is None:
            num_edges = min(
                math.ceil(self.n_neighbors * len(self.cover)),
                len(self.cover)**2 - 1)
            self.truncation_value = np.sort(
                self.distmat.flatten())[num_edges]

        filtmat = np.zeros_like(self.distmat)
        # cardmat = np.empty_like(self.distmat, dtype=int)
        # for i, p in enumerate(self.cover):
        #     for j, q in enumerate(self.cover):
        #         cardmat[i, j] = min(len(p.point_indices), len(q.point_indices))
        #         cardmat[j, i] = cardmat[i, j]
        filtmat[self.distmat < self.truncation_value] = self.distmat[
            self.distmat < self.truncation_value]
        # filtmat = filtmat / cardmat
        if self.log_filtration:
            filtmat[filtmat > 0] = np.log(filtmat[filtmat > 0])
        #     filt_trunc_value = np.log(self.truncation_value)
        # else:
        #     filt_trunc_value = self.truncation_value
        filt_trunc_value = np.max(filtmat)

        self.simplex_tree = simplex_tree_from_patch_list(
            patch_list=self.cover,
            X=self.X,
            filtration_function=self.filtration_value_fct,
            filtmat=filtmat,
            truncation_value=filt_trunc_value,
            coeff_field=self.coeff_field,
            min_persistence=self.min_persistence,
            simplex_batch_size=self.simplex_batch_size,
            isolated_points=self.isolated_points,
            dimension=self.dimension)


def patch_distance_matrix(X,
                          cover,
                          distance_function,
                          isolated_points):
    distances = []
    for index1 in range(len(cover)):
        for index2 in range(index1 + 1, len(cover)):
            face = [index1, index2]
            distances.append(distance_function(
                X,
                face=face,
                patch_list=cover))

    distances = squareform(distances)
    if isolated_points:
        np.fill_diagonal(distances, np.inf)
    else:
        for index in range(len(cover)):
            face = [index]
            distances[index, index] = (distance_function(
                X,
                face=face,
                patch_list=cover))

    return distances


def truncation_distance_matrix(X, cover):
    distmat = directed_truncation_distance_matrix(X, cover)
    return np.maximum(distmat, distmat.transpose())


def directed_truncation_distance_matrix(X, cover):
    distmat = np.empty((len(cover), len(cover)))

    for index1, patch1 in enumerate(cover):
        for index2, patch2 in enumerate(cover):
            if index1 == index2:
                distmat[index1, index2] = np.inf
            else:
                patch2 = cover[index2]
                distmat[index1, index2] = (
                    np.min(cdist([patch1.barycenter],
                                 X[patch2.point_indices])) -
                    np.max(cdist([patch1.barycenter],
                                 X[patch1.point_indices])))
    return distmat


def enumerate_cliques(G, max_cardinality=None):
    """Returns cliques of cardinality up to max_cardinality
    in an undirected graph.

    This method returns cliques of size (cardinality)
    k = 1, 2, 3, ..., max_cardinality.

    For now I require the nodes of G to be consequetive integers.

    Parameters
    ----------
    G: undirected graph
    max_cardinality: int

    Returns
    -------
    generator of lists: generator of list for each clique.

    Notes
    -----
    To obtain a list of all cliques, use
    :samp:`list(enumerate_all_cliques(G))`.

    Based on the algorithm published by Zhang et al. (2005) [1]_
    and adapted to output all cliques discovered.

    This algorithm is not applicable on directed graphs.

    This algorithm ignores self-loops and parallel edges as
    clique is not conventionally defined with such edges.

    There are often many cliques in graphs.
    This algorithm however, hopefully, does not run out of memory
    since it only keeps candidate sublists in memory and
    continuously removes exhausted sublists.

    References
    ----------
    .. [1] Yun Zhang, Abu-Khzam, F.N., Baldwin, N.E., Chesler, E.J.,
           Langston, M.A., Samatova, N.F.,
           Genome-Scale Computational Approaches to Memory-Intensive
           Applications in Systems Biology.
           Supercomputing, 2005. Proceedings of the ACM/IEEE SC 2005
           Conference, pp. 12, 12-18 Nov. 2005.
           doi: 10.1109/SC.2005.29.
           http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=1559964&isnumber=33129
    """
    if max_cardinality is None:
        max_cardinality = G.number_of_nodes()
    index = {}
    nbrs = {}
    for u in G:
        index[u] = len(index)
        # Neighbors of u that appear after u in the iteration order of G.
        nbrs[u] = {v for v in G[u] if v not in index}

    queue = collections.deque(
        ([u], sorted(nbrs[u], key=index.__getitem__)) for u in G)
    # Loop invariants:
    # 1. len(base) is nondecreasing.
    # 2. (base + cnbrs) is sorted with respect to the iteration order of G.
    # 3. cnbrs is a set of common neighbors of nodes in base.
    while queue:
        base, cnbrs = map(list, queue.popleft())
        yield base
        if len(base) < max_cardinality:
            for i, u in enumerate(cnbrs):
                # Use generators to reduce memory consumption.
                queue.append((itertools.chain(base, [u]),
                              filter(nbrs[u].__contains__,
                                     itertools.islice(cnbrs, i + 1, None))))


def maximal_faces(X, patch_list, distmat, simplex_batch_size=10000):
    '''(Approximate) Delaunay maximal faces of barycenters
       More precisely strong Witness complex as in Carlsson
       Topology and Data.
    '''
    if X.shape[1] < 8:
        max_faces = delaunay_maximal_faces(X=X, patch_list=patch_list)
    barycenters = np.vstack([
        np.mean(X[patch.point_indices], axis=0) for patch in patch_list])
    max_faces = set()
    for index, x in enumerate(X):
        if index % simplex_batch_size:
            max_faces = set(eliminate_subsets(max_faces))
        dists = cdist([x], barycenters).flatten()
        face = frozenset(
            np.flatnonzero(dists <= np.min(dists)))
        if len(face) > X.shape[1] + 1:
            face = frozenset(
                np.argpartition(dists, X.shape[1])[:X.shape[1] + 1])
        # elif len(face) <= X.shape[1]:

        max_faces.add(face)
    return eliminate_subsets(max_faces)


def delaunay_maximal_faces(X, patch_list):
    '''Delaunay maximal faces of barycenters
    '''
    barycenters = np.vstack([
        np.mean(X[patch.point_indices], axis=0) for patch in patch_list])
    tri = Delaunay(barycenters)
    return set(frozenset(face) for face in tri.simplices)


def get_nerve_from_maximal_faces(maximal_faces,
                                 dimension):
    '''Returns nerve given a list of maximal faces
    Parameters
    -----------
    maximal_face :
    dimension : int
        Maximal homology dimension
    Returns
    -----------
    nerve : ndarray of lists of integers
        List of all simplices up to desired dimension
    '''
    # This needs to be optimized.
    # First we find the maximal faces of the dimension + 1 skeleton.
    # Then we fill in the rest of the faces.
    faces = ((frozenset(face)
              for face in
              powerset(maximal_face,
                       max_card=dimension + 2,
                       min_card=min(len(maximal_face),
                                    dimension + 2)))
             for maximal_face in maximal_faces)
    faces = frozenset(
        itertools.chain.from_iterable(faces))
    faces = ((frozenset(face)
              for face in
              powerset(maximal_face,
                       max_card=dimension + 2))
             for maximal_face in faces)
    return np.array(list(frozenset(
        itertools.chain.from_iterable(faces))))


def powerset(some_set, max_card, min_card=1):
    return itertools.chain(*[
        list(itertools.combinations(some_set, dim + 1))
        for dim in range(min_card - 1, max_card)])


def log_filtration_value_function(filtration_value_fct):
    def fct(X,
            face,
            patch_list):
        return np.log(
            filtration_value_fct(
                X,
                face,
                patch_list))
    return fct


def patch_distance(patches,
                   numbers_of_points):
    patch_data = zip(patches, numbers_of_points)
    minimizing_points = []
    for (A, nA), (B, nB) in itertools.combinations(patch_data, r=2):
        minimizing_points.extend(nearest_points(
            A, B, max(nA, 1), max(nB, 1)))
        # minimizing_points.extend(brute_nearest_points(A, B))
    mb = Miniball(np.vstack(minimizing_points))
    return np.sqrt(mb.squared_radius())


def nearest_points(A, B, nA, nB):
    while (len(A) > nA or len(B) > nB):
        A, B = bisect_nearer(A, B, nA, nB)
    return np.vstack((A, B))
    # return list(A) + list(B)


def bisect_nearer(A, B, nA, nB):
    barycenter = np.mean(np.vstack((A, B)), axis=0)[np.newaxis, :]
    if len(A) > nA:
        # improve with argpartition
        A = A[np.argsort(cdist(A, barycenter).flatten())[
            :max(len(A) // 2, nA)]]
    if len(B) > nB:
        B = B[np.argsort(cdist(B, barycenter).flatten())[
            :max(len(B) // 2, nB)]]
    return A, B


def simplex_tree_from_patch_list(
        patch_list,
        X,
        filtration_function,
        filtmat,
        truncation_value,
        coeff_field=11,
        min_persistence=0,
        simplex_batch_size=10000,
        isolated_points=False,
        dimension=None):

    if dimension is None:
        dimension = X.shape[1] - 1

    geometric_dimension = X.shape[1]
    persistence_dim_max = geometric_dimension <= dimension
    if persistence_dim_max:
        print('strange thing')
    simplex_tree = SimplexTree(
        coeff_field=coeff_field,
        persistence_dim_max=persistence_dim_max,
        truncation_value=truncation_value,
        min_persistence=min_persistence)

    G = nx.from_numpy_array(filtmat)
    for face in enumerate_cliques(G, dimension + 2):
        if len(face) == 1:
            if isolated_points:
                node = list(face)[0]
                weight = G.edges[node, node]['weight']
                simplex_tree.insert(simplex=list(face), filtration=weight)
            else:
                simplex_tree.insert(
                    simplex=list(face), filtration=truncation_value)
        elif len(face) == 2:
            weight = G.edges[face]['weight']
            simplex_tree.insert(simplex=face, filtration=weight)
        else:
            weight = filtration_function(
                X,
                face,
                patch_list)
            if weight <= truncation_value:
                simplex_tree.insert(
                    simplex=list(face),
                    filtration=weight)
    return simplex_tree


def is_power_of_two(n):
    """Returns True iff n is a power of two.  Assumes n > 0."""
    return (n & (n - 1)) == 0


def eliminate_subsets(sequence_of_sets):
    """I did not write this. Far too clever for me.
    Return a list of the elements of `sequence_of_sets`, removing all
    elements that are subsets of other elements.  Assumes that each
    element is a set or frozenset."""
    # The code below does not handle the case of a sequence containing
    # only the empty set, so let's just handle all easy cases now.
    sequence_of_sets = list(frozenset(sequence_of_sets))
    if len(sequence_of_sets) <= 1:
        return list(sequence_of_sets)
    # We need an indexable sequence so that we can use a bitmap to
    # represent each set.
    if not isinstance(sequence_of_sets, collections.Sequence):
        sequence_of_sets = list(sequence_of_sets)
    # For each element, construct the list of all sets containing that
    # element.
    sets_containing_element = {}
    for i, s in enumerate(sequence_of_sets):
        for element in s:
            try:
                sets_containing_element[element] |= 1 << i
            except KeyError:
                sets_containing_element[element] = 1 << i
    # For each set, if the intersection of all of the lists in which it is
    # contained has length != 1, this set can be eliminated.
    out = [s for s in sequence_of_sets
           if s and is_power_of_two(functools.reduce(
               operator.and_, (sets_containing_element[x] for x in s)))]
    return out
