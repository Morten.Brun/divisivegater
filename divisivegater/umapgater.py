'''
Copyright: Applied Topology Group at University of Bergen.
No commercial use of the software is permitted without proper license.
'''
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import re
from sklearn.cluster import KMeans
# import matplotlib.colors as mcolors
import fcsparser
from umap import UMAP
from sklearn.manifold import TSNE
from KDEpy import FFTKDE
from itertools import cycle, islice


class Gater(object):

    def read_fcs_file(
            self,
            filepaths,
            asinh_factor=5,
            verbose=False):
        '''
        Read in fcs file and store it as data attribute
        Default parser is fcsparser. If the fcs file is
        corrupted it may fail, and the FlowCore parser may
        succeed.
        '''
        if type(filepaths) == str:
            filepaths = [filepaths]
        read_fcs = read_fcs_file
        datas = [read_fcs(
            filepath=filepath,
            asinh_factor=asinh_factor,
            verbose=verbose) for filepath
            in filepaths]
        self.data = pd.concat(datas, ignore_index=True, sort=False)

    def set_markers(
            self,
            markers=None,
            point_indices=None,
            scale=True,
            n_components=None):
        if markers is None:
            markers = self.data.columns
        scaler = StandardScaler()
        if point_indices is None:
            self.point_indices = np.arange(len(self.data))
        elif point_indices.dtype == 'int64':
            self.point_indices = point_indices
        else:
            self.point_indices = np.hstack(
                (point_indices[component] for component in point_indices))
        self.values = self.data[markers].iloc[self.point_indices].values
        if scale:
            self.values = scaler.fit_transform(self.values)

    def umap_transform_data(self, reducer='umap', n_components=None):
        if not hasattr(self, 'values'):
            self.set_markers()
        if reducer == 'umap':
            reducer = UMAP(random_state=42)
        if n_components is None:
            reducer.n_components = self.values.shape[1]
        self.X = reducer.fit_transform(self.values)

    def get_all_markers(
            self):
        return self.data.columns

    def set_n_clusters(self, n_clusters):
        if not hasattr(self, 'n_clusters'):
            self.n_clusters = n_clusters
            return True
        if all((isinstance(n_clusters, int),
                n_clusters != self.n_clusters)):
            self.n_clusters = n_clusters
            return True
        else:
            return False

    def compute_clusters(self, n_clusters=None, random_state=42):
        if any((self.set_n_clusters(n_clusters),
                not hasattr(self, 'kmeans'))):
            if not hasattr(self, 'X'):
                self.umap_transform_data()
            self.kmeans = KMeans(n_clusters=self.n_clusters,
                                 random_state=random_state).fit(self.X)

    def get_color_list(self, n_clusters):
        colors = np.vstack((
            plt.cm.tab10(np.arange(10)),
            plt.cm.Set3(np.arange(12))))
        colors = np.array(list(islice(cycle(colors), n_clusters)))
        colors = np.vstack((colors, [0, 0, 0, 1]))
        return colors

    def plot_cluster_size_histogram(
            self, n_clusters=None, random_state=42):
        self.compute_clusters(
            n_clusters=n_clusters, random_state=random_state)
        colors = self.get_color_list(self.n_clusters)
        labels = np.unique(self.kmeans.labels_)
        for label in labels:
            height = np.zeros(len(labels))
            comp = self.kmeans.labels_ == label
            height[label] = np.sum(comp)
            plt.bar(np.arange(len(labels)),
                    height=height,
                    color=colors[label])
        plt.show()

    def plot_clusters_umap(
            self,
            n_clusters=None,
            random_state=42,
            colors=None,
            s=2,
            ax=None,
            legend=True,
            sample_size=np.inf,
            ticks=False,
            alpha=1,
            scatterpoints=1,
            fontsize=10,
            markerscale=5,
            reducer=None):
        self.compute_clusters(
            n_clusters=n_clusters, random_state=random_state)
        if colors is None:
            colors = self.get_color_list(self.n_clusters)
        if ax is None:
            ax = plt.gca()
        if not hasattr(self, 'umap_embedding'):
            if reducer is None:
                reducer = UMAP(random_state=42)
            self.umap_embedding = reducer.fit_transform(self.values)
        for label in np.unique(self.kmeans.labels_):
            comp = np.flatnonzero(self.kmeans.labels_ == label)
            if sample_size < len(comp):
                sample = np.random.choice(len(comp),
                                          sample_size,
                                          replace=False)
                comp = comp[sample]
            ax.scatter(self.umap_embedding[comp, 0],
                       self.umap_embedding[comp, 1],
                       c=colors[self.kmeans.labels_[comp]],
                       s=s, label=label)
        if legend:
            ax.legend(scatterpoints=scatterpoints,
                      fontsize=fontsize,
                      markerscale=markerscale,
                      )
        return ax

    def plot_clusters_tsne(
            self,
            n_clusters=None,
            random_state=42,
            colors=None,
            s=2,
            ax=None,
            sample_size=10000,
            legend=True,
            scatterpoints=1,
            fontsize=10,
            markerscale=5,
            reducer=None):
        self.compute_clusters(
            n_clusters=n_clusters, random_state=random_state)
        if colors is None:
            colors = self.get_color_list(self.n_clusters)
        if ax is None:
            ax = plt.gca()
        if not hasattr(self, 'tsne_embedding'):
            if reducer is None:
                reducer = TSNE(n_components=2, perplexity=30)
            if sample_size < len(self.values):
                self.tsne_sample = np.random.choice(
                    len(self.values),
                    size=sample_size,
                    replace=False)
            else:
                self.tsne_sample = np.arange(len(self.values))
            self.tsne_embedding = reducer.fit_transform(
                self.values[self.tsne_sample])
        for label in np.unique(self.kmeans.labels_):
            comp = np.flatnonzero(self.kmeans.labels_ == label)
            comp = np.isin(self.tsne_sample, comp)
            ax.scatter(self.tsne_embedding[comp, 0],
                       self.tsne_embedding[comp, 1],
                       c=colors[self.kmeans.labels_[
                           self.tsne_sample[comp]]],
                       s=s,
                       label=label)
        if legend:
            ax.legend(scatterpoints=scatterpoints,
                      fontsize=fontsize,
                      markerscale=markerscale,
                      )
            ax.legend()
        return ax

    def plot_intensities(
            self,
            n_clusters=None,
            random_state=42,
            markers=None,
            figsize=None,
            plot_columns=None,
            components=None,
            comps=None,
            ax=None,
            sample_size=10000,
            ticks=False,
            colors=None,
            cmap='jet',
            alpha=1,
            labels=True,
            bandwidth='silverman',
            legends=False,
            KDE=False,
            s=10):
        self.compute_clusters(
            n_clusters=n_clusters, random_state=random_state)
        if comps is None:
            comps = [np.flatnonzero(label == self.kmeans.labels_) for label in
                     np.unique(self.kmeans.labels_)]
        if markers is None:
            markers = list(self.data.columns[[0, 1]])
        if plot_columns is None:
            plot_columns = len(markers)
        if components is None:
            components = list(range(len(comps)))
        if colors is None:
            colors = len(components) * [None]
        if figsize is None:
            figsize = (
                15, (15 / plot_columns) *
                np.ceil((1 + len(markers) +
                         np.ceil(len(markers) / plot_columns) *
                         plot_columns * len(components)) / plot_columns))
        plt.figure(figsize=figsize)
        for component_idx, component in enumerate(components):
            for marker_idx, marker in enumerate(markers):
                ax = plt.subplot(
                    np.ceil((1 + len(markers) +
                             np.ceil(len(markers) / plot_columns) *
                             plot_columns * len(components)) / plot_columns),
                    plot_columns,
                    marker_idx + 1 +
                    np.ceil(len(markers) / plot_columns) *
                    plot_columns * component_idx)
                scaler = StandardScaler()
                Z = self.data[[marker]].values
                Z = scaler.fit_transform(Z)
                estimator = FFTKDE(kernel='gaussian', bw=bandwidth)
                # x = np.linspace(np.min(Y), 1.1 * np.max(Y), 128)
                # - 0.05 * np.max(Y)
                x, y = estimator.fit(Z).evaluate(512)
                estimator = FFTKDE(kernel='gaussian', bw=bandwidth)
                Y = self.data[[marker]].iloc[
                    self.point_indices[comps[component]]].values
                Y = scaler.transform(Y)
                y = estimator.fit(Y).evaluate(x)
                x = scaler.inverse_transform(x)
                ax.plot(x, y)
                if labels and component_idx == 0:
                    ax.set_xlabel(marker)
                    ax.xaxis.set_label_position('top')
                if marker_idx == 0:
                    ax.set_title(component,
                                 x=-0.6, y=0.5)
                if legends:
                    ax.legend([component])
                ax.set_yticks(())
                ax.set_yticklabels(())
                if any((not ticks,
                        component_idx != len(components) - 1)):
                    ax.set_xticklabels(())

        if ticks:
            plt.subplots_adjust(
                top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.65,
                wspace=0.45)
        else:
            plt.subplots_adjust(
                top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.25,
                wspace=0.35)
        return ax


def read_fcs_file(filepath,
                  asinh_factor=5,
                  verbose=False):
    if verbose:
        print(filepath)
    meta, data = fcsparser.parse(filepath, reformat_meta=True)
    # pick out markers that have been used and rename columns
    Pchannels = [str for str in meta.keys() if
                 (str.endswith('S') and str.startswith('$P'))]
    Pchannel_numbers = [
        [int(s) for s in re.findall(r'\d+', channel)][0]
        for channel in Pchannels]
    names = [name for name in meta['_channel_names_']]
    for k, n in enumerate(Pchannel_numbers):
        names[n-1] = meta[Pchannels[k]]
    data.columns = names
    asinh_markers = list(meta[name] for name in Pchannels)
    data[asinh_markers] = np.arcsinh(data[asinh_markers] / 5)
    data = data[np.sort([x for x in data.columns])]
    return data
