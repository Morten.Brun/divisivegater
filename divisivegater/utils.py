from sklearn.cluster import KMeans
import numpy as np
import math
from scipy.spatial.distance import cdist


def iterated_k_means_centers(
        X,
        n_clusters=20,
        n_iter=10,
        max_iter=1,
        sample_size=None):

    if sample_size is None:
        sample_size = len(X)
    else:
        sample_size = min(sample_size, len(X))
    kmeans = KMeans(n_clusters=n_clusters,
                    init='k-means++',
                    n_init=1,
                    max_iter=max_iter)
    Y = []
    for index in range(n_iter):
        sample = np.random.choice(
            len(X),
            size=sample_size,
            replace=False)
        kmeans.fit(X[sample])
        Y.append(kmeans.cluster_centers_)
    return np.vstack(Y)


def voronoi_cluster_labels(X, centers, batch_size=None):

    labels = np.empty(len(X))
    if batch_size is None:
        batch_size = len(X)
    num_batches = math.ceil(len(X) / batch_size)
    for i in range(num_batches):
        labels[i * batch_size: (i + 1) * batch_size] = np.argmin(
            cdist(X[i * batch_size: (i + 1) * batch_size],
                  centers), axis=1)
    return labels


def voronoi_clusters(X, centers, batch_size=1000):
    labels = voronoi_cluster_labels(
        X=X, centers=centers, batch_size=batch_size)
    return [set(np.flatnonzero(labels == label)) for
            label in range(len(centers))]


def node_index_sets(model, clusters):
    # create the set of samples under each node
    node_idx_sets = [cluster for cluster in clusters]
    for left, right in model.children_:
        node_idx_sets.append(
            set.union(
                node_idx_sets[left],
                node_idx_sets[right]))
    return np.array(node_idx_sets)


def merged_clusters(model,
                    clusters,
                    min_leaf_size=None,
                    distance_threshold=0):
    n_samples = 1 + max(max(cluster) for cluster in clusters if cluster)
    node_idx_sets = node_index_sets(model=model, clusters=clusters)
    clusters = node_idx_sets[cardinality_pruned_leaves(
        model=model,
        node_idx_sets=node_idx_sets,
        min_leaf_size=min_leaf_size,
        distance_threshold=distance_threshold)]
    labels = np.empty(n_samples, dtype=int)
    for idx, clus in enumerate(clusters):
        labels[list(clus)] = idx
    return labels


def cardinality_pruned_leaves(
        model,
        node_idx_sets,
        min_leaf_size=None,
        distance_threshold=0):
    node_idx_counts = np.array([len(c) for c in node_idx_sets])
    if min_leaf_size is not None:
        decision_list = np.min(
            (node_idx_counts >= min_leaf_size)[model.children_],
            axis=1)
    else:
        decision_list = model.distances_ >= distance_threshold

    return np.array(
        leaf_indices(model=model,
                     has_children=decision_list,
                     idx=model.n_leaves_ + model.children_.shape[0] - 1))


def leaf_indices(model, has_children, idx):
    if idx < model.n_leaves_:
        return [idx]
    elif has_children[idx - model.n_leaves_]:
        left_child, right_child = model.children_[idx - model.n_leaves_]
        return (leaf_indices(model=model,
                             has_children=has_children,
                             idx=left_child)
                +
                leaf_indices(model=model,
                             has_children=has_children,
                             idx=right_child)
                )
    else:
        return [idx]


def is_wellcentered(pts, tol=1e-8):
    """Determines whether a set of points defines a well-centered simplex.
    """
    barycentric_coordinates = circumcenter_barycentric(pts)
    return np.min(barycentric_coordinates) > tol


def circumcenter_barycentric(pts):
    """Barycentric coordinates of the circumcenter of a set of points.

    Parameters
    ----------
    pts : array-like
        An N-by-K array of points which define an (N-1)-simplex
        in K dimensional space.
        N and K must satisfy 1 <= N <= K + 1 and K >= 1.
    Returns
    -------
    coords : ndarray
        Barycentric coordinates of the circumcenter of
        the simplex defined by pts.
        Stored in an array with shape (K,)

    Examples
    --------
    >>> from pydec.math.circumcenter import *
    >>> circumcenter_barycentric([[0],[4]])           # edge in 1D
    array([ 0.5,  0.5])
    >>> circumcenter_barycentric([[0,0],[4,0]])       # edge in 2D
    array([ 0.5,  0.5])
    >>> circumcenter_barycentric([[0,0],[4,0],[0,4]]) # triangle in 2D
    array([ 0. ,  0.5,  0.5])
    See Also
    --------
    circumcenter_barycentric

    References
    ----------
    Uses an extension of the method described here:
    http://www.ics.uci.edu/~eppstein/junkyard/circumcenter.html
    https://github.com/hirani/pydec/blob/master/pydec/math/circumcenter.py
    """

    pts = np.asarray(pts)

    rows, cols = pts.shape

    assert(rows <= cols + 1)

    A = np.bmat([[2*np.dot(pts, pts.T), np.ones((rows, 1))],
                 [np.ones((1, rows)),  np.zeros((1, 1))]])

    b = np.hstack((np.sum(pts * pts, axis=1), np.ones((1))))
    x = np.linalg.solve(A, b)
    bary_coords = x[:-1]

    return bary_coords


def circumcenter(pts):
    """Circumcenter and circumradius of a set of points.

    Parameters
    ----------
    pts : array-like
        An N-by-K array of points which define an (N-1)-simplex
        in K dimensional space.
        N and K must satisfy 1 <= N <= K + 1 and K >= 1.
    Returns
    -------
    center : ndarray
        Circumcenter of the simplex defined by pts.
        Stored in an array with shape (K,)
    radius : float
        Circumradius of the circumsphere that
        circumscribes the points defined by pts.

    Examples
    --------
    >>> circumcenter([[0],[1]])             # edge in 1D
    (array([ 0.5]), 0.5)
    >>> circumcenter([[0,0],[1,0]])         # edge in 2D
    (array([ 0.5,  0. ]), 0.5)
    >>> circumcenter([[0,0],[1,0],[0,1]])   # triangle in 2D
    (array([ 0.5,  0.5]), 0.70710678118654757)
    See Also
    --------
    circumcenter_barycentric

    References
    ----------
    Uses an extension of the method described here:
    http://www.ics.uci.edu/~eppstein/junkyard/circumcenter.html
    https://github.com/hirani/pydec/blob/master/pydec/math/circumcenter.py
    """
    pts = np.asarray(pts)
    bary_coords = circumcenter_barycentric(pts)
    center = np.dot(bary_coords, pts)
    radius = np.linalg.norm(pts[0, :] - center)
    return (center, radius)
