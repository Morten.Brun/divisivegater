"""Principal Direction Voronoi Cover

Input: A point cloud X in euclidean space

Method: The set X is divided iteratively.

Division:
    The principal direction of X is found
    and X is split so that the sums of variations
    along this direction is smallest possible.
"""

# Authors: Morten Brun <morten.brun@uib.no>
# License:
import itertools
import collections
import operator
import functools
import math
import numpy as np
from scipy.spatial.distance import cdist, squareform
from scipy.spatial import Delaunay
# from sklearn.cluster import MiniBatchKMeans
from miniball import Miniball
import networkx as nx
# from scipy.spatial import Delaunay
from . simplex_tree import SimplexTree
from . basic_cover import BasicCover
###############################################################################
# Patch class


def dtm_patch_distance(
        percentile=None,
        upper_percentile=1.,
        lower_percentile=0.,
        k=100,
        p=2):
    '''
    Sum of squared distances to barycenter.
    '''
    if percentile is not None:
        lower_percentile = 0
        upper_percentile = percentile

    def fct(X, face, patch_list):
        if len(face) == 1:
            return np.inf
        patches = [X[patch_list[patch_index].point_indices]
                   for patch_index in face]
        return patch_dtm(
            patches, lower_percentile, upper_percentile, k, p)
    return fct


def patch_dtm(patches, lower_percentile, upper_percentile, k, p):

    barycenters = [np.mean(patch, axis=0)[np.newaxis, :] for patch in patches]
    mb = Miniball(np.vstack(barycenters))
    # barycenter = np.mean(np.vstack(barycenters), axis=0)[np.newaxis, :]
    return max(dtm(np.array([mb.center()]), patch,
                   lower_percentile, upper_percentile, k, p)
               for patch in patches)


def dtm(center, patch, lower_percentile, upper_percentile, k, p):
    k = max(k, int(lower_percentile * len(patch)))
    k = min(k, math.ceil(upper_percentile * len(patch)))
    dists = cdist(center, patch).flatten()
    dists = np.partition(dists, k-1)[:k]
    return np.mean(dists ** p) ** (1/p)


class Patch(object):
    """Patch object
    """

    def __init__(
            self,
            point_indices,
            X
    ):
        self.point_indices = point_indices
        self.barycenter = np.mean(X[point_indices], axis=0)


class StandardDeviationCover(BasicCover):
    """Standard Deviation Cover

    """

    def __init__(self,
                 n_patches=10,
                 percentile=0.1,
                 min_patch_size=1,
                 coeff_field=11,
                 min_persistence=0,
                 dimension=None,
                 simplex_batch_size=10000,
                 truncation_value=np.inf,
                 bandwidth=0.6,
                 distance_fct=None,
                 max_num_nearby_points=1000,
                 min_num_nearby_points=1,
                 p=2,
                 minibatch_random_state=None,
                 labels=None,
                 batch_size=100,
                 init_size=None,
                 svd_solver='auto'):
        self.n_patches = n_patches
        self.percentile = percentile
        self.min_patch_size = min_patch_size
        self.coeff_field = coeff_field
        self.bandwidth = bandwidth
        self.max_num_nearby_points = max_num_nearby_points
        self.min_persistence = min_persistence
        self.dimension = dimension
        self.minibatch_random_state = minibatch_random_state
        # self.min_cost_value_factor = min_cost_value_factor
        if distance_fct is None:
            distance_fct = dtm_patch_distance(
                percentile=None,
                upper_percentile=0.2,
                lower_percentile=0.,
                k=10,
                p=p)
        self.distance_function = distance_fct
        self.filtration_value_fct = self.distance_function
        self.svd_solver = svd_solver
        self.simplex_batch_size = simplex_batch_size
        self.batch_size = batch_size
        self.init_size = init_size
        self.truncation_value = truncation_value
        self.clusters = [
            np.flatnonzero(labels == label) for
            label in np.unique(labels)]

    def compute_cover(self,
                      X=None,
                      labels=None,
                      # min_cost_value_factor=None,
                      n_patches=None):
        '''
        Merging of clusters is not good.
        Need some kind of
        persistence to merge.
        '''
        if all((not hasattr(self, 'X'),
                X is None)):
            raise ValueError('X has to be specified')
        if all((hasattr(self, 'cover'),
                X is None,
                labels is None,
                n_patches is None)):
            return
        self.__dict__.pop('nerve', None)
        self.__dict__.pop('simplex_tree', None)
        self.__dict__.pop('patch_graph', None)
        if n_patches is not None:
            self.n_patches = n_patches
        if X is None:
            X = self.X
        self.X = X
        if labels is not None:
            self.clusters = [
                np.flatnonzero(labels == label) for
                label in np.unique(labels)]
        # self.X = X[:, np.min(X, axis=0) < np.max(X, axis=0)]
        self.minX = np.min(self.X, axis=0)
        self.maxX = np.max(self.X, axis=0)
        if self.X.shape[1] == 0:
            raise ValueError('X has to be non-degenerate')

        self.cover = [Patch(
            point_indices=point_indices,
            X=self.X) for
            point_indices in self.clusters]
        # cover_sets = [set(patch.point_indices) for patch in cover]
        # min_patch_size = min(len(patch.point_indices) for patch in
        #                      cover)
        # while min_patch_size < self.min_patch_size:
        #     patch_index = np.argmin(
        #         [len(patch.point_indices) for patch in
        #          cover])
        #     outlier_indices = np.array(list(
        #         cover_sets[patch_index]))
        #     cover.pop(patch_index)
        #     cover_sets.pop(patch_index)
        #     barycenters = np.vstack(
        #         [patch.barycenter for patch in cover])
        #     dists = cdist(barycenters, self.X[outlier_indices])
        #     outlier_patch_indices = np.argmin(dists, axis=0)
        #     for outlier_index, patch_index in enumerate(
        #             outlier_patch_indices):
        #         cover_sets[patch_index].add(outlier_indices[outlier_index])
        #     min_patch_size = min(
        #         len(patch.point_indices) for patch in cover)

        # self.cover = [Patch(
        #     point_indices=np.array(list(point_indices)),
        #     X=self.X) for
        #     point_indices in cover_sets]

    def set_distance_matrix(
            self,
            X=None,
            truncation_value=None,
            n_patches=None,
            dimension=None):

        if X is not None:
            self.X = X
        if not hasattr(self, 'dimension'):
            self.dimension = self.X.shape[1] - 1
        if truncation_value is not None:
            self.trunction_value = truncation_value

        if dimension is not None:
            self.dimension = dimension
        if not hasattr(self, 'cover') or not all(
                (X is None, n_patches is None)):
            self.compute_cover(
                X=X, n_patches=n_patches)

        self.distmat = patch_distance_matrix(
            self.X, self.cover, self.distance_function)

    def set_simplex_tree(
            self,
            X=None,
            truncation_value=None,
            n_patches=None,
            dimension=None):

        if hasattr(self, 'simplex_tree'):
            if all((X is None,
                    n_patches is None,
                    dimension is None,
                    truncation_value is None)):
                return
        if not all((
                hasattr(self, 'distmat'),
                X is None,
                n_patches is None,
                dimension is None,
                truncation_value is None)):
            self.set_distance_matrix(
                X=X,
                truncation_value=truncation_value,
                n_patches=n_patches,
                dimension=dimension)

        self.simplex_tree = simplex_tree_from_patch_list(
            patch_list=self.cover,
            X=self.X,
            filtration_function=self.filtration_value_fct,
            filtmat=self.distmat,
            truncation_value=self.truncation_value,
            coeff_field=self.coeff_field,
            min_persistence=self.min_persistence,
            simplex_batch_size=self.simplex_batch_size,
            dimension=self.dimension)


def patch_distance_matrix(X,
                          cover,
                          distance_function):
    distances = []
    for index1 in range(len(cover)):
        for index2 in range(index1 + 1, len(cover)):
            face = [index1, index2]
            distances.append(distance_function(
                X,
                face=face,
                patch_list=cover))

    distances = squareform(distances)
    np.fill_diagonal(distances, np.inf)

    return distances


def truncation_distance_matrix(X, cover):
    distmat = directed_truncation_distance_matrix(X, cover)
    return np.maximum(distmat, distmat.transpose())


def directed_truncation_distance_matrix(X, cover):
    distmat = np.empty((len(cover), len(cover)))

    for index1, patch1 in enumerate(cover):
        for index2, patch2 in enumerate(cover):
            if index1 == index2:
                distmat[index1, index2] = np.inf
            else:
                patch2 = cover[index2]
                distmat[index1, index2] = (
                    np.min(cdist([patch1.barycenter],
                                 X[patch2.point_indices])) -
                    np.max(cdist([patch1.barycenter],
                                 X[patch1.point_indices])))
    return distmat


def approximate_maximal_faces(X, patch_list, simplex_batch_size=10000):
    '''(Approximate) Delaunay maximal faces of barycenters
       More precisely strong Witness complex as in Carlsson
       Topology and Data.
    '''
    if X.shape[1] < 8:
        max_faces = delaunay_maximal_faces(X=X, patch_list=patch_list)
    barycenters = np.vstack([
        np.mean(X[patch.point_indices], axis=0) for patch in patch_list])
    max_faces = set()
    for index, x in enumerate(X):
        if index % simplex_batch_size:
            max_faces = set(eliminate_subsets(max_faces))
        dists = cdist([x], barycenters).flatten()
        face = frozenset(
            np.flatnonzero(dists <= np.min(dists)))
        if len(face) > X.shape[1] + 1:
            face = frozenset(
                np.argpartition(dists, X.shape[1])[:X.shape[1]])
        # elif len(face) <= X.shape[1]:

        max_faces.add(face)
    return eliminate_subsets(max_faces)


def delaunay_maximal_faces(X, patch_list):
    '''Delaunay maximal faces of barycenters
    '''
    barycenters = np.vstack([
        np.mean(X[patch.point_indices], axis=0) for patch in patch_list])
    tri = Delaunay(barycenters)
    return set(frozenset(face) for face in tri.simplices)


def get_nerve_from_maximal_faces(maximal_faces,
                                 dimension):
    '''Returns nerve given a list of maximal faces
    Parameters
    -----------
    maximal_face :
    dimension : int
        Maximal homology dimension
    Returns
    -----------
    nerve : ndarray of lists of integers
        List of all simplices up to desired dimension
    '''
    # This needs to be optimized.
    # First we find the maximal faces of the dimension + 1 skeleton.
    # Then we fill in the rest of the faces.
    faces = ((frozenset(face)
              for face in
              powerset(maximal_face,
                       max_card=dimension + 2,
                       min_card=min(len(maximal_face),
                                    dimension + 2)))
             for maximal_face in maximal_faces)
    faces = frozenset(
        itertools.chain.from_iterable(faces))
    faces = ((frozenset(face)
              for face in
              powerset(maximal_face,
                       max_card=dimension + 2))
             for maximal_face in faces)
    return np.array(list(frozenset(
        itertools.chain.from_iterable(faces))))


def powerset(some_set, max_card, min_card=1):
    return itertools.chain(*[
        list(itertools.combinations(some_set, dim + 1))
        for dim in range(min_card - 1, max_card)])


def simplex_tree_from_patch_list(
        patch_list,
        X,
        filtration_function,
        filtmat,
        truncation_value,
        coeff_field=11,
        min_persistence=0,
        simplex_batch_size=10000,
        dimension=None):

    if dimension is None:
        dimension = X.shape[1] - 1

    geometric_dimension = X.shape[1]
    persistence_dim_max = geometric_dimension <= dimension
    if persistence_dim_max:
        print('strange thing')
    simplex_tree = SimplexTree(
        coeff_field=coeff_field,
        persistence_dim_max=persistence_dim_max,
        truncation_value=truncation_value,
        min_persistence=min_persistence)
    if dimension == 0:
        nerve = minimum_spanning_tree(filtmat)
    else:
        nerve = get_nerve_from_maximal_faces(
            delaunay_maximal_faces(X, patch_list), dimension=dimension)
    for face in nerve:
        if len(face) == 1:
            simplex_tree.insert(
                simplex=list(face), filtration=truncation_value)
        else:
            weight = filtration_function(
                X,
                face,
                patch_list)
            if weight <= truncation_value:
                simplex_tree.insert(
                    simplex=list(face),
                    filtration=weight)
    return simplex_tree


def minimum_spanning_tree(filtmat):
    filtmat = filtmat.copy()
    np.fill_diagonal(filtmat, 0)
    G = nx.from_numpy_array(filtmat)
    T = nx.minimum_spanning_tree(G)
    return [(node, ) for node in T.nodes] + [edge for edge in T.edges]


def is_power_of_two(n):
    """Returns True iff n is a power of two.  Assumes n > 0."""
    return (n & (n - 1)) == 0


def eliminate_subsets(sequence_of_sets):
    """I did not write this. Far too clever for me.
    Return a list of the elements of `sequence_of_sets`, removing all
    elements that are subsets of other elements.  Assumes that each
    element is a set or frozenset."""
    # The code below does not handle the case of a sequence containing
    # only the empty set, so let's just handle all easy cases now.
    sequence_of_sets = list(frozenset(sequence_of_sets))
    if len(sequence_of_sets) <= 1:
        return list(sequence_of_sets)
    # We need an indexable sequence so that we can use a bitmap to
    # represent each set.
    if not isinstance(sequence_of_sets, collections.Sequence):
        sequence_of_sets = list(sequence_of_sets)
    # For each element, construct the list of all sets containing that
    # element.
    sets_containing_element = {}
    for i, s in enumerate(sequence_of_sets):
        for element in s:
            try:
                sets_containing_element[element] |= 1 << i
            except KeyError:
                sets_containing_element[element] = 1 << i
    # For each set, if the intersection of all of the lists in which it is
    # contained has length != 1, this set can be eliminated.
    out = [s for s in sequence_of_sets
           if s and is_power_of_two(functools.reduce(
               operator.and_, (sets_containing_element[x] for x in s)))]
    return out
