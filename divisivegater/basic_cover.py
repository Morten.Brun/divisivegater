"""Density Divisive Cover"""

# Authors: Morten Brun <morten.brun@uib.no>
# License:

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import matplotlib as mpl
from itertools import islice, cycle
from KDEpy import FFTKDE
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from matplotlib.collections import LineCollection
# import networkx as nx
###############################################################################


class BasicCover(object):
    """Basic Cover
    """

    def plot_persistence(self,
                         X=None,
                         dimension=None,
                         n_patches=None,
                         labels=None,
                         plot_labels=None,
                         plot_only=None,
                         title=None,
                         xy_range=None,
                         colormap="default",
                         size=10,
                         alpha=0.5,
                         add_multiplicity=False,
                         ax_color=np.array([0.0, 0.0, 0.0]),
                         colors=None,
                         diagonal=True,
                         lifetime=True,
                         legend=True,
                         show=False,
                         ticks=True,
                         ):
        """
        Show or save persistence diagram

        Parameters
        ----------
        plot_only: list of numeric
            If specified, an array of only the diagrams that should be plotted.
        title: string, default is None
            If title is defined, add it as title of the plot.
        xy_range: list of numeric [xmin, xmax, ymin, ymax]
            User provided range of axes. This is useful for comparing \
            multiple persistence diagrams.
        labels: string or list of strings
            Legend labels for each diagram. \
            If none are specified, we use H_0, H_1, H_2,... by default.
        colormap: str (default : 'default')
            Any of matplotlib color palettes. \
            Some options are 'default', 'seaborn', 'sequential'.
        size: numeric (default : 10)
            Pixel size of each point plotted.
        alpha: numeric (default : 0.5)
            Transparency of each point plotted.
        add_multiplicity: boolean (default : False)
            Show multiplicity of points plotted.
        ax_color: any valid matplotlib color type.
            See https://matplotlib.org/api/colors_api.html for complete API.
        diagonal: bool (default : True)
            Plot the diagonal x=y line.
        lifetime: bool (default : False). If True, diagonal is turned to False.
            Plot life time of each point instead of birth and death.  \
            Essentially, visualize (x, y-x).
        legend: bool (default : True)
            If true, show the legend.
        show: bool (default : False)
            Call plt.show() after plotting. If you are using self.plot() as \
            part of a subplot, set show=False and call plt.show() only once \
            at the end.
        """
        self.set_simplex_tree(
            X=X, n_patches=n_patches,  labels=labels,
            dimension=dimension)
        ax = self.simplex_tree.plot_persistence(
            plot_only=plot_only,
            title=title,
            xy_range=xy_range,
            labels=plot_labels,
            colormap=colormap,
            size=size,
            alpha=alpha,
            add_multiplicity=add_multiplicity,
            ax_color=ax_color,
            colors=colors,
            diagonal=diagonal,
            lifetime=lifetime,
            legend=legend,
            show=show,
        )
        if not ticks:
            plt.xticks(())
            plt.yticks(())
        return ax

    def plot_cover(self,
                   X=None,
                   Y=None,
                   n_patches=None,
                   components=None,
                   ax=None,
                   s=1,
                   center_size=10,
                   alpha=1,
                   colors=plt.cm.tab20b(range(20)),
                   sample_size=10000,
                   cluster_list=None,
                   ticks=False,
                   reducer=None,
                   legend=False,
                   fontsize=10,
                   markerscale=2,
                   plot_centers=False):
        if (X is not None or
            n_patches is not None or
                not hasattr(self, 'cover')):
            self.compute_cover(X, n_patches=n_patches)
        # plt.figure()
        if Y is None:
            X = self.X
        else:
            X = Y
        if len(self.X) != len(X):
            raise ValueError('X and Y must be of the same length')
        sample_size = min((sample_size, len(self.X)))
        if ax is None:
            ax = plt.gca()

        if cluster_list is None:
            cluster_list = np.arange(len(self.cover))
        if components is None:
            components = np.unique(cluster_list)
        clusters = [(value, np.flatnonzero(cluster_list == value)) for
                    value in components]
        # colors = np.random.permutation(colors)
        for idx, cluster in clusters:
            point_indices = np.hstack([
                self.cover[index].point_indices for index in cluster])
            if len(point_indices) > sample_size:
                sample = np.random.choice(
                    len(point_indices),
                    size=sample_size,
                    replace=False)
                point_indices = point_indices[sample]
            if idx >= 0:
                color = colors[idx % len(colors)]
            else:
                color = 'black'
            if (Y is None and reducer is not None):
                Z = reducer.transform(X[point_indices])
            else:
                Z = X[point_indices]

            ax.scatter(Z[:, 0],
                       Z[:, 1],
                       color=color,
                       label=idx,
                       s=s, alpha=alpha)
        if plot_centers:
            for patch in self.cover:
                if len(patch.point_indices) == 0:
                    continue
                barycenters = np.mean(self.X[patch.point_indices], axis=0)[
                    np.newaxis, :]
                if reducer is not None:
                    barycenters = reducer.transform(barycenters)
                ax.scatter(*barycenters.transpose(),
                           c='black',
                           s=center_size)
        if reducer is None:
            ax.scatter([np.min(X[:, 0]), np.max(X[:, 0])],
                       [np.min(X[:, 1]), np.max(X[:, 1])],
                       alpha=0)
        if not ticks:
            plt.xticks(())
            plt.yticks(())
        if legend:
            ax.legend(scatterpoints=1,
                      fontsize=fontsize,
                      markerscale=markerscale)

        return ax

    def plot_minimum_spanning_tree(
            self,
            X=None,
            n_patches=None,
            ax=None,
            s=5,
            alpha=1,
            cm=mpl.colors.LinearSegmentedColormap.from_list(
                'mycolors', ['blue', 'red']),
            ticks=False,
            plot_centers=True,
            linewidths=2,
            barycenters=None,
            reducer=None):
        if (X is not None or
            n_patches is not None or
                not hasattr(self, 'cover')):
            self.compute_cover(X, n_patches=n_patches)
        if ax is None:
            ax = plt.gca()
        if barycenters is None:
            barycenters = np.vstack([patch.barycenter
                                     for patch in self.cover])
        if reducer is not None:
            barycenters = reducer.transform(barycenters)
        mst = self.minimum_spanning_tree()
        weighted_edges = [
            list(simp) for simp in mst.edges()]
        # if edge[1] >= 0.55] )
        colors = np.array([mst.edges[edge]['weight']
                           for edge in weighted_edges])
        if np.max(colors) > np.min(colors):
            colors = (colors - np.min(colors)) / \
                (np.max(colors) - np.min(colors))
        else:
            colors = np.zeros_like(colors)
        edges = [barycenters[simp] for simp in weighted_edges]
        col = [cm(int(256*color)) for color in colors]
        lc = LineCollection(edges, colors=col, linewidths=linewidths)
        ax.add_collection(lc)
        if plot_centers:
            ax.scatter(barycenters[:, 0],
                       barycenters[:, 1],
                       s=s,
                       alpha=alpha,
                       c='black')

        if not ticks:
            plt.xticks(())
            plt.yticks(())
        return ax

    def cycle_patches(self,
                      X=None,
                      n_patches=None,
                      cycle_num=None,
                      max_birth_value=np.inf,
                      max_death_value=np.inf,
                      persistence_function=None,
                      persistence_order='additive'):
        if (X is not None or
            n_patches is not None or
                not hasattr(self, 'simplex_tree')):
            self.set_simplex_tree(X, n_patches=n_patches, dimension=1)
        if any((not hasattr(self, 'cycle_clust'),
                cycle_num is not None,
                persistence_function is not None)):
            if persistence_function is None:
                if cycle_num is None:
                    cycle_num = 0
                one_pers = np.sort(
                    [pers[1][1] - pers[1][0] for pers in
                     self.simplex_tree.persistence() if
                     (pers[0] == 1 and pers[1][0] <= max_birth_value)])
                num_cycles = min(cycle_num + 1, len(one_pers))
                if num_cycles == 0:
                    return
                persistence_function = one_pers[-num_cycles]
            self.cycle_clust = self.simplex_tree.cycle_components(
                persistence_function=persistence_function,
                persistence_order=persistence_order,
                max_birth_value=max_birth_value,
                max_death_value=max_death_value)
        return [self.cover[idx] for idx in self.cycle_clust[-1]]

    def plot_cycle_components(self,
                              X=None,
                              Y=None,
                              n_patches=None,
                              cycles=[0],
                              x_axis=0,
                              y_axis=1,
                              alpha=0.1,
                              s=1,
                              c='black',
                              center_size=10,
                              sample_size=np.inf,
                              max_birth_value=np.inf,
                              max_death_value=np.inf,
                              ax=None,
                              ticks=False,
                              persistence_function=None,
                              persistence_order='additive',
                              reducer=None,
                              plot_centers=False):
        if ax is None:
            ax = plt.gca()
        for cycle_num in cycles:
            self.plot_cycle_component(
                X=X,
                Y=Y,
                n_patches=n_patches,
                cycle_num=cycle_num,
                x_axis=x_axis,
                y_axis=y_axis,
                alpha=alpha,
                s=s,
                c=c,
                center_size=center_size,
                sample_size=sample_size,
                max_birth_value=max_birth_value,
                max_death_value=max_death_value,
                ax=ax,
                ticks=ticks,
                persistence_order=persistence_order,
                persistence_function=persistence_function,
                reducer=reducer,
                plot_centers=plot_centers)
        return ax

    def plot_cycle_component(self,
                             X=None,
                             Y=None,
                             n_patches=None,
                             cycle_num=None,
                             x_axis=0,
                             y_axis=1,
                             alpha=0.1,
                             s=1,
                             c='black',
                             center_size=10,
                             sample_size=np.inf,
                             max_birth_value=np.inf,
                             max_death_value=np.inf,
                             ax=None,
                             ticks=False,
                             persistence_order='additive',
                             persistence_function=None,
                             reducer=None,
                             plot_centers=False):
        cycle_patches = self.cycle_patches(
            X=X,
            n_patches=n_patches,
            cycle_num=cycle_num,
            max_birth_value=max_birth_value,
            max_death_value=max_death_value,
            persistence_order=persistence_order,
            persistence_function=persistence_function)

        if Y is None:
            Y = self.X[:, [0, 1]]
        if reducer is not None:
            Y = reducer.transform(Y)
        if len(self.X) != len(Y):
            raise ValueError('X and Y must be of the same length')
        if ax is None:
            ax = plt.gca()
        if cycle_patches is None:
            return ax
        for patch in cycle_patches:
            if len(patch.point_indices) > sample_size:
                sample = np.random.choice(
                    len(patch.point_indices),
                    size=sample_size,
                    replace=False)
                point_indices = patch.point_indices[sample]
            else:
                point_indices = patch.point_indices
            ax.scatter(Y[point_indices, 0],
                       Y[point_indices, 1], s=s, alpha=alpha)
        if plot_centers:
            for patch in cycle_patches:
                point_indices = patch.point_indices
                ax.scatter(np.mean(Y[point_indices, 0]),
                           np.mean(Y[point_indices, 1]),
                           c=c,
                           s=center_size)
        ax.scatter([np.min(Y[:, 0]), np.max(Y[:, 0])],
                   [np.min(Y[:, 1]), np.max(Y[:, 1])],
                   alpha=0)
        if not ticks:
            plt.xticks(())
            plt.yticks(())

        return ax

    def plot_cycle_representatives(self,
                                   X=None,
                                   Y=None,
                                   n_patches=None,
                                   num_cycles=1,
                                   cycles=None,
                                   max_birth_value=np.inf,
                                   max_death_value=np.inf,
                                   x_axis=0,
                                   y_axis=1,
                                   s=1,
                                   alpha=0.1,
                                   ax=None,
                                   KDE=False,
                                   grid_points=1024,
                                   sample_size=1000,
                                   bandwidth=None,
                                   legend=True,
                                   plot_data=False,
                                   ticks=False,
                                   linewidth=1,
                                   size=1,
                                   alpha_data=0.5,
                                   colors=None,
                                   reducer=None,
                                   persistence_function=None,
                                   persistence_order='additive'):
        if ax is None:
            ax = plt.gca()
        if cycles is not None:
            num_cycles = max(cycles) + 1
        elif num_cycles is not None:
            cycles = np.arange(num_cycles)
        if (X is not None or
            n_patches is not None or
                not hasattr(self, 'simplex_tree')):
            self.set_simplex_tree(X, n_patches=n_patches, dimension=1)
        if Y is None:
            Y = self.X[:, [0, 1]]
        if reducer is not None:
            Y = reducer.transform(Y)
        if len(self.X) != len(Y):
            raise ValueError('X and Y must be of the same length')
        if plot_data:
            ax = plotX(X=Y,
                       bandwidth=bandwidth,
                       grid_points=grid_points,
                       sample_size=sample_size,
                       ticks=ticks,
                       size=size,
                       KDE=KDE,
                       ax=ax,
                       alpha=alpha_data)
        if any((not hasattr(self, 'rep_cycles'),
                num_cycles is not None,
                persistence_function is not None)):
            if persistence_function is None:
                if num_cycles is None:
                    num_cycles = 2
            # one_pers = np.sort(
            #     [pers[1][1] - pers[1][0] for pers in
            #      self.simplex_tree.persistence() if (
            #          pers[0] == 1 and
            #          pers[1][1] <= max_death_value and
            #          pers[1][0] <= max_birth_value)])
            # num_cycles = min(num_cycles, len(one_pers) - 1)
            # if num_cycles == 0:
            #     return ax
            # if persistence_function == 'additive':
            #     persistence_function = one_pers[-num_cycles - 1]
            # elif persistence_function == 'multiplicative':

        self.rep_cycles = self.cycle_representatives(
            persistence_function=persistence_function,
            max_birth_value=max_birth_value,
            max_death_value=max_death_value,
            persistence_order=persistence_order,
            num_cycles=num_cycles)
        if len(self.rep_cycles) == 0:
            print('No cycles selected to plot.')
            return ax
        self.rep_cycles = [self.rep_cycles[cycle] for cycle in cycles if
                           cycle < len(self.rep_cycles)]
        if colors is None:
            colors = [  # '#377eb8',
                '#ff7f00', '#4daf4a',
                '#f781bf', '#a65628', '#984ea3',
                '#999999', '#e41a1c', '#dede00']
        colors = np.array(list(islice(cycle(colors),
                                      len(self.rep_cycles))))
        center_points = np.array([np.mean(Y[patch.point_indices], axis=0)
                                  for patch in self.cover])
        for idx, cyc in enumerate(self.rep_cycles):
            ax.plot([], color=colors[idx], label=idx)
            for edge in cyc:
                ax.plot([center_points[edge[0], 0],
                         center_points[edge[1], 0]],
                        [center_points[edge[0], 1],
                         center_points[edge[1], 1]],
                        linewidth=linewidth,
                        color=colors[idx])
        ax.scatter([np.min(Y[:, 0]), np.max(Y[:, 0])],
                   [np.min(Y[:, 1]), np.max(Y[:, 1])],
                   alpha=0)
        if legend:
            ax.legend()
        return ax

    def components(self,
                   X=None,
                   n_patches=None,
                   n_components=2,
                   persistence_function=None,
                   persistence_order='additive',
                   max_birth_value=np.inf):
        comps = self.patch_components(
            X=X,
            n_patches=n_patches,
            n_components=n_components,
            persistence_function=persistence_function,
            persistence_order=persistence_order,
            max_birth_value=max_birth_value)
        return np.array([np.concatenate(
            [self.cover[index].point_indices for
             index in comp]) if len(comp) > 0 else np.array([])
            for comp in comps], dtype=object)

    def patch_components(self,
                         X=None,
                         n_patches=None,
                         n_components=2,
                         persistence_function=None,
                         persistence_order='additive',
                         max_birth_value=np.inf):
        if (X is not None or
            n_patches is not None or
                not hasattr(self, 'simplex_tree')):
            self.set_simplex_tree(X, n_patches=n_patches, dimension=0)
        return self.simplex_tree.components(
            n_components=n_components,
            persistence_function=persistence_function,
            persistence_order=persistence_order,
            max_birth_value=max_birth_value)

    def plot_outlier_histogram(self,
                               X=None,
                               max_num_clusters=28,
                               persistence_function=None,
                               persistence_order='additive',
                               max_birth_value=np.inf,
                               figsize=(15, 8)):

        outlier_lengths = {}
        for n_components in range(2, max_num_clusters + 1):
            outlier_lengths[n_components] = len(
                self.components(n_components=n_components,
                                persistence_function=persistence_function,
                                persistence_order=persistence_order,
                                max_birth_value=max_birth_value)[-1])
        plt.figure(figsize=figsize)
        ax = plt.gca()
        ax.bar(np.arange(len(outlier_lengths)),
               outlier_lengths.values())
        ax.set_xticks(np.arange(
            0, len(outlier_lengths)))
        ax.set_xticklabels(
            np.arange(1, len(outlier_lengths) + 1))
        return ax

    def plot_component_size_histogram(
            self,
            persistence_function=None,
            persistence_order='additive',
            max_birth_value=np.inf,
            X=None,
            n_components=11):
        if X is not None:
            self.X = X
        components = self.components(
            n_components=n_components,
            persistence_function=persistence_function,
            persistence_order=persistence_order,
            max_birth_value=max_birth_value)
        component_list = np.empty(len(self.X), dtype=int)
        for idx, component in enumerate(components):
            if len(component) > 0:
                component_list[component] = idx
        colors = np.vstack((
            plt.cm.tab10(np.arange(10)),
            plt.cm.Set3(np.arange(12))))
        colors = np.array(list(islice(cycle(colors), n_components)))
        colors = np.vstack((colors, [0, 0, 0, 1]))
        ax = plt.gca()
        for idx, comp in enumerate(components):
            height = np.zeros(len(components))
            height[idx] = len(comp)
            ax.bar(np.arange(len(components)),
                   height=height,
                   color=colors[idx])
        return ax

    def plot_components(self,
                        X=None,
                        Y=None,
                        n_patches=None,
                        n_components=2,
                        components=None,
                        comps=None,
                        colors=list(mcolors.TABLEAU_COLORS.values()),
                        cmap='Blues',
                        persistence_function=None,
                        persistence_order='additive',
                        max_birth_value=np.inf,
                        ax=None,
                        sample_size=None,
                        ticks=False,
                        alpha=1,
                        bandwidth=None,
                        legend=True,
                        fontsize=10,
                        markerscale=2,
                        reducer=None,
                        KDE=False,
                        s=1):
        if Y is None:
            Y = self.X
        if len(self.X) != len(Y):
            raise ValueError('X and Y must be of the same length')
        if ax is None:
            ax = plt.gca()
        if comps is None:
            comps = self.components(
                X=X,
                n_patches=n_patches,
                n_components=n_components,
                persistence_function=persistence_function,
                persistence_order=persistence_order,
                max_birth_value=max_birth_value)
        if components is None:
            components = list(range(len(comps)))
        n_components = len(comps) - 1
        # if colors is None:
        #     colors = np.vstack((
        #         plt.cm.tab10(np.arange(10)),
        #         plt.cm.Set3(np.arange(12))))
        #     colors = np.array(list(islice(cycle(colors), n_components)))
        #     colors = np.vstack((colors, [0, 0, 0, 1]))
        for idx in components:
            if idx >= len(comps):
                continue
            component = comps[idx]
            if len(component) == 0:
                continue
            if idx < n_components:
                color = colors[idx % len(colors)],
            else:
                color = 'black'
            plotX(Y[component],
                  reducer=reducer,
                  sample_size=sample_size,
                  size=s,
                  ax=ax,
                  color=color,
                  cmap=cmap,
                  alpha=alpha,
                  ticks=ticks,
                  bandwidth=bandwidth,
                  label=idx,
                  KDE=KDE)
        if KDE is False:
            if reducer is None:
                ax.scatter([np.min(Y[:, 0]), np.max(Y[:, 0])],
                           [np.min(Y[:, 1]), np.max(Y[:, 1])],
                           alpha=0)
            if legend:
                ax.legend(scatterpoints=1,
                          fontsize=fontsize,
                          markerscale=markerscale)
        return ax

    def cycle_component(self,
                        X=None,
                        n_patches=None,
                        cycle_num=None,
                        max_birth_value=np.inf,
                        max_death_value=np.inf,
                        persistence_function=None,
                        persistence_order='additive'):

        cycle_patches = self.cycle_patches(
            X=X,
            n_patches=n_patches,
            cycle_num=cycle_num,
            max_birth_value=max_birth_value,
            max_death_value=max_death_value,
            persistence_function=persistence_function,
            persistence_order=persistence_order)

        return np.hstack([patch.point_indices for patch in cycle_patches])

    def cycle_representatives(
            self,
            X=None,
            n_patches=None,
            max_birth_value=np.inf,
            max_death_value=np.inf,
            persistence_function=None,
            persistence_order='additive',
            num_cycles=None):
        if any((X is not None,
                n_patches is not None,
                not hasattr(self, 'simplex_tree'))):
            self.set_simplex_tree(X=X, n_patches=n_patches, dimension=1)
        return self.simplex_tree.cycle_representatives(
            persistence_function=persistence_function,
            max_birth_value=max_birth_value,
            max_death_value=max_death_value,
            persistence_order=persistence_order,
            num_cycles=num_cycles)

    # def parent_patch_index(self, index, G):
    #     neighbors = np.array(G[index])
    #     return neighbors[np.argmin([self.simplex_tree.filtration([v])
    #                                 for v in neighbors])]

    # def assign_point_to_cluster(
    #         self,
    #         cluster_list,
    #         index,
    #         assigned_points,
    #         G):
    #     path_cluster = set([index])
    #     while 1:
    #         if index in assigned_points:
    #             path_index = cluster_list[index]
    #             break
    #         index = self.parent_patch_index(index, G)
    #         if index in path_cluster:
    #             path_index = -1
    #             break
    #         path_cluster.add(index)
    #     assigned_points = assigned_points.union(path_cluster)
    #     cluster_list[list(path_cluster)] = path_index
    #     #     else:
    #     #         self.assign_point_to_cluster(
    #     #             cluster_list, parent, assigned_points, G)
    #     #         cluster_list[index] = cluster_list[parent]
    #     #     assigned_points.add(index)

    #     # if index not in assigned_points:
    #     #     parent = self.parent_patch_index(index, G)
    #     #     if parent == index:
    #     #         cluster_list[index] = 0
    #     #     else:
    #     #         self.assign_point_to_cluster(
    #     #             cluster_list, parent, assigned_points, G)
    #     #         cluster_list[index] = cluster_list[parent]
    #     #     assigned_points.add(index)

    def graph(self):
        if not hasattr(self, 'simplex_tree'):
            self.set_simplex_tree()
        return self.simplex_tree.graph()

    def minimum_spanning_tree(self):
        if not hasattr(self, 'simplex_tree'):
            self.set_simplex_tree()
        return self.simplex_tree.minimum_spanning_tree()

    def patch_cluster_list(
            self,
            X=None,
            comps=None,
            n_clusters=2,
            persistence_function=None,
            persistence_order='additive',
            max_birth_value=np.inf):
        if comps is None:
            comps = self.patch_components(
                X=X,
                n_patches=None,
                n_components=n_clusters,
                persistence_function=persistence_function,
                persistence_order=persistence_order,
                max_birth_value=max_birth_value)

        G = self.minimum_spanning_tree()

        clusters = np.full(len(self.cover), -1)
        for index, comp in enumerate(comps[:-1]):
            clusters[list(comp)] = index

        for index in G.nodes():
            if clusters[index] != -1:
                continue
            else:
                L = {index}
                while(1):
                    neighbors = list(G.neighbors(index))
                    parent = neighbors[
                        np.argmin([self.simplex_tree.filtration([v])
                                   for v in neighbors])]
                    if clusters[parent] != -1:
                        clusters[list(L)] = clusters[parent]
                        break
                    else:
                        if parent not in L:
                            L.add(parent)
                            index = parent
                        else:
                            break
        return clusters

        # assigned_points=set.union(*(set(comp) for comp in comps[: -1]))
        # cluster_list=np.empty(len(self.cover), dtype = int)
        # for comp_index, comp in enumerate(comps[: -1]):
        #     for index in comp:
        #         cluster_list[index]=comp_index
        # for index, y in enumerate(self.cover):
        #     self.assign_point_to_cluster(
        #         cluster_list, index, assigned_points, G)
        # assigned_points=set(np.flatnonzero(~(cluster_list == -1)))
        # while len(assigned_points) < len(self.cover):
        #     for index in np.flatnonzero(cluster_list == -1):
        #         assigned_neighbors=np.array(
        #             list(assigned_points.intersection(G[index])))
        #         if len(assigned_neighbors) == 0:
        #             continue
        #         cluster_list[index]=cluster_list[assigned_neighbors[
        #             np.argmin(np.array([
        #                 self.simplex_tree.filtration([v]) for
        #                 v in assigned_neighbors]))]]
        #         assigned_points.add(index)
        # return cluster_list

    def cluster_list(
            self,
            X=None,
            comps=None,
            n_clusters=2,
            persistence_function=None,
            persistence_order='additive',
            max_birth_value=np.inf):
        patch_clusters = self.patch_cluster_list(
            X=X,
            comps=comps,
            n_clusters=n_clusters,
            persistence_function=persistence_function,
            persistence_order=persistence_order,
            max_birth_value=max_birth_value)
        clusters = np.full(len(self.X), -1)
        for index, patch_cluster in enumerate(patch_clusters):
            clusters[self.cover[index].point_indices] = patch_cluster
        return clusters

    def plot_patch_clusters(
            self,
            X=None,
            Y=None,
            comps=None,
            n_clusters=2,
            persistence_function=None,
            persistence_order='additive',
            reducer=None,
            s=5,
            alpha=0.6,
            max_birth_value=np.inf):
        comps = self.patch_components(
            n_components=n_clusters,
            persistence_function=persistence_function,
            persistence_order=persistence_order,
            max_birth_value=max_birth_value)
        cluster_list = self.patch_cluster_list(
            X=None,
            comps=comps,
            n_clusters=n_clusters,
            persistence_function=persistence_function,
            persistence_order=persistence_order,
            max_birth_value=max_birth_value)
        if Y is None:
            Y = np.array([patch.barycenter for patch in self.cover])
        if reducer is not None:
            Y = reducer.transform(Y)
        colors = np.array(list(islice(cycle(['#377eb8',
                                             '#ff7f00', '#4daf4a',
                                             '#f781bf', '#a65628', '#984ea3',
                                             '#999999', '#e41a1c', '#dede00']),
                                      len(comps))))
        plt.scatter(Y[:, 0],
                    Y[:, 1],
                    color=colors[cluster_list],
                    alpha=alpha,
                    s=s
                    )
        # plt.scatter(self.X[:, 0], self.X[:, 1], s=0.01, color='grey')

    def plot_clusters(
            self,
            X=None,
            Y=None,
            comps=None,
            components=None,
            n_components=2,
            ax=None,
            persistence_function=None,
            persistence_order='additive',
            reducer=None,
            s=1,
            alpha=1,
            max_birth_value=np.inf,
            colors=plt.cm.tab20b(range(20)),
            center_size=5,
            sample_size=10000,
            ticks=False,
            plot_centers=False,
            fontsize=10,
            markerscale=2,
            legend=True):
        cluster_list = self.patch_cluster_list(
            X=None,
            comps=comps,
            n_clusters=n_components,
            persistence_function=persistence_function,
            persistence_order=persistence_order,
            max_birth_value=max_birth_value)

        return self.plot_cover(
            X=X,
            Y=Y,
            n_patches=None,
            components=components,
            ax=ax,
            s=s,
            center_size=center_size,
            alpha=alpha,
            colors=colors,
            cluster_list=cluster_list,
            sample_size=sample_size,
            ticks=ticks,
            reducer=reducer,
            plot_centers=plot_centers,
            fontsize=fontsize,
            markerscale=markerscale,
            legend=legend)

    def plotX(self,
              x_axis=0,
              y_axis=1,
              bandwidth=None,
              grid_points=1024,
              sample_size=1000,
              ticks=False,
              size=1,
              color=None,
              cmap='Blues',
              KDE=False,
              ax=None,
              label=None,
              alpha=1):
        KDEplotX(X=self.X,
                 x_axis=x_axis,
                 y_axis=y_axis,
                 bandwidth=bandwidth,
                 grid_points=grid_points,
                 ticks=ticks,
                 ax=ax,
                 cmap=cmap,
                 label=label,
                 alpha=alpha)


def plotX(X,
          weights=None,
          x_axis=0,
          y_axis=1,
          reducer=None,
          bandwidth=None,
          grid_points=1024,
          sample_size=1000,
          ticks=False,
          size=1,
          color=None,
          c=None,
          cmap='Blues',
          KDE=False,
          ax=None,
          label=None,
          alpha=1):
    if ax is None:
        ax = plt.gca()
    if KDE:
        return KDEplotX(
            X=X,
            weights=weights,
            reducer=reducer,
            x_axis=x_axis,
            y_axis=y_axis,
            bandwidth=bandwidth,
            grid_points=grid_points,
            ticks=ticks,
            cmap=cmap,
            ax=ax,
            label=label,
            alpha=alpha)
    else:
        return plotX_sample(
            X=X,
            reducer=reducer,
            x_axis=x_axis,
            y_axis=y_axis,
            sample_size=sample_size,
            size=size,
            ticks=ticks,
            ax=ax,
            label=label,
            color=color,
            c=c,
            alpha=alpha)


def plotX_sample(X,
                 x_axis=0,
                 y_axis=1,
                 reducer=None,
                 sample_size=None,
                 size=1,
                 ticks=False,
                 color=None,
                 c=None,
                 label=None,
                 ax=None,
                 alpha=1):
    if ax is None:
        ax = plt.gca()
    if reducer is None:
        ax.scatter([np.min(X[:, x_axis]), np.max(X[:, x_axis])],
                   [np.min(X[:, y_axis]), np.max(X[:, y_axis])],
                   alpha=0, color='black')
    if sample_size is None:
        sample_size = len(X)
    if len(X) > sample_size:
        sample = np.random.choice(
            len(X),
            size=sample_size,
            replace=False)
        X = X[sample]
    if reducer is not None:
        X = reducer.transform(X)
    ax.scatter(X[:, x_axis],
               X[:, y_axis],
               s=size,
               alpha=alpha,
               color=color,
               c=c,
               label=label)
    if not ticks:
        plt.xticks(())
        plt.yticks(())
    return ax


def KDEplotX(X,
             weights=None,
             x_axis=0,
             y_axis=1,
             reducer=None,
             bandwidth=None,
             grid_points=1024,
             ticks=False,
             ax=None,
             cmap='Blues',
             label=None,
             alpha=1):
    if ax is None:
        ax = plt.gca()
    if reducer is not None:
        X = reducer.transform(X)
    coords = X[:, [x_axis, y_axis]]
    scaler = StandardScaler()
    coords = scaler.fit_transform(coords)
    n, d = coords.shape
    if bandwidth is None:
        bandwidth = n**(-1./(d+4))
    # kde_fft = FFTKDE(kernel='tri', bw=bandwidth).fit(coords)
    kde_fft = FFTKDE(kernel='gaussian', bw=bandwidth).fit(
        coords, weights=weights)
    grid, y = kde_fft.evaluate(grid_points=1024)
    grid = scaler.inverse_transform(grid)
    # Plot density heat map
    xmin, xmax = np.min(grid[:, 0]), np.max(grid[:, 0])
    ymin, ymax = np.min(grid[:, 1]), np.max(grid[:, 1])

    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    ax.imshow(y.reshape((
        int(np.sqrt(len(y))), int(np.sqrt(len(y))))).transpose(),
        cmap=cmap,
        origin='lower',
        extent=[xmin, xmax, ymin, ymax],
        alpha=alpha)
    if not ticks:
        plt.xticks(())
        plt.yticks(())
    ax.set_aspect((xmax - xmin) / (ymax - ymin))
    return ax


def get_principal_directions(X, num_directions=1, svd_solver='auto'):
    pca = PCA(n_components=num_directions, svd_solver=svd_solver,
              whiten=False).fit(X)
    return pca.components_
