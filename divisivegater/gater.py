'''
Copyright: Applied Topology Group at University of Bergen.
No commercial use of the software is permitted without proper license.
'''
import pandas as pd
import numpy as np
from . cover import Cover
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import re
import fcsparser
# from umap import UMAP
from KDEpy import FFTKDE


class Gater(Cover):

    def __init__(self,
                 n_patches=155,
                 center_method='median',
                 filtration_value_fct='distance',
                 distance_function='deviance_distance',
                 svd_solver='auto',
                 cone_solver='glpk'
                 ):
        super().__init__(n_patches=n_patches,
                         center_method=center_method,
                         filtration_value_fct=filtration_value_fct,
                         distance_function=distance_function,
                         svd_solver=svd_solver,
                         cone_solver=cone_solver)

    def subgater(self, components):
        cover_indices = np.hstack(
            *(component for component in components))
        subgtr = self.subcover(cover_indices)
        subgtr.data = self.data
        return subgtr

    def read_fcs_file(
            self,
            filepaths,
            asinh_factor=5,
            parser='fcsparser',
            verbose=False):
        '''
        Read in fcs file and store it as data attribute
        Default parser is fcsparser. If the fcs file is
        corrupted it may fail, and the FlowCore parser may
        succeed.
        '''
        if type(filepaths) == str:
            filepaths = [filepaths]
        if parser == 'FlowCore':
            read_fcs = read_fcs_file_flowCore
        else:
            read_fcs = read_fcs_file
        datas = [read_fcs(
            filepath=filepath,
            asinh_factor=asinh_factor,
            verbose=verbose) for filepath
            in filepaths]
        self.data = pd.concat(datas, ignore_index=True, sort=False)

    def set_markers(
            self,
            markers=None,
            point_indices=None,
            scale=True,
            reducer=None,
            n_components=None):
        self.__dict__.pop('nerve', None)
        self.__dict__.pop('simplex_tree', None)
        self.__dict__.pop('patch_graph', None)
        self.__dict__.pop('cover', None)
        self.reducer = reducer
        if markers is None:
            markers = self.data.columns
        scaler = StandardScaler()
        if point_indices is None:
            self.point_indices = np.arange(len(self.data))
        elif point_indices.dtype == 'int64':
            self.point_indices = point_indices
        else:
            self.point_indices = np.hstack(
                (point_indices[component] for component in point_indices))
        self.X = self.data[markers].iloc[self.point_indices].values
        if scale:
            self.X = scaler.fit_transform(self.X)
        if reducer:
            if n_components is None:
                n_components = self.X.shape[1]
#             if reducer == 'umap':
#                 reducer = UMAP(random_state=42, n_components=n_components)
            self.X = reducer.fit_transform(self.X)

    def get_all_markers(
            self):
        return self.data.columns

    def plot_components(
            self,
            Y=None,
            markers=None,
            n_patches=None,
            n_components=2,
            components=None,
            comps=None,
            colors=None,
            cmap=None,
            persistence_function=None,
            max_birth_value=np.inf,
            ax=None,
            sample_size=10000,
            ticks=False,
            alpha=1,
            labels=True,
            bandwidth=1,
            KDE=False,
            s=1):
        if markers is None:
            markers = list(self.data.columns[[0, 1]])
        if Y is None:
            Y = self.data[markers].iloc[self.point_indices].values

        ax = super().plot_components(
            Y=Y,
            n_patches=n_patches,
            n_components=n_components,
            components=components,
            comps=comps,
            colors=colors,
            cmap=cmap,
            persistence_function=persistence_function,
            max_birth_value=max_birth_value,
            ax=ax,
            sample_size=sample_size,
            ticks=ticks,
            alpha=alpha,
            bandwidth=bandwidth,
            KDE=KDE,
            s=s)
        if labels:
            ax.set_xlabel(markers[0])
            ax.xaxis.set_label_position('top')
            ax.set_ylabel(markers[1])
            ax.yaxis.set_label_position('right')
        return ax

    def plot_biaxial(
            self,
            markers=None,
            figsize=None,
            plot_columns=None,
            n_patches=None,
            n_components=2,
            components=None,
            comps=None,
            persistence_function=None,
            max_birth_value=np.inf,
            ax=None,
            sample_size=10000,
            ticks=False,
            colors=None,
            cmap='jet',
            alpha=1,
            labels=True,
            bandwidth=None,
            legends=False,
            KDE=False,
            s=10):
        if comps is None:
            comps = self.components(
                n_patches=n_patches,
                n_components=n_components,
                persistence_function=persistence_function,
                max_birth_value=max_birth_value)
        if markers is None:
            markers = [list(self.data.columns[[0, 1]])]
        if plot_columns is None:
            plot_columns = len(markers)
        if components is None:
            components = list(range(len(comps)))
        if colors is None:
            colors = len(components) * [None]
        if figsize is None:
            figsize = (
                15, (15 / plot_columns) *
                np.ceil((1 + len(markers) +
                         np.ceil(len(markers) / plot_columns) *
                         plot_columns * len(components)) / plot_columns))
        plt.figure(figsize=figsize)
        for component_idx, component in enumerate(components):
            for marker_idx, marker_pair in enumerate(markers):
                plt.subplot(
                    np.ceil((1 + len(markers) +
                             np.ceil(len(markers) / plot_columns) *
                             plot_columns * len(components)) / plot_columns),
                    plot_columns,
                    marker_idx + 1 +
                    np.ceil(len(markers) / plot_columns) *
                    plot_columns * component_idx)
                ax = None
                ax = self.plot_components(
                    markers=marker_pair,
                    n_patches=n_patches,
                    colors=colors,
                    cmap=cmap,
                    n_components=n_components,
                    components=[component],
                    comps=comps,
                    persistence_function=persistence_function,
                    max_birth_value=max_birth_value,
                    ax=ax,
                    sample_size=sample_size,
                    ticks=ticks,
                    alpha=alpha,
                    labels=labels,
                    bandwidth=bandwidth,
                    KDE=KDE,
                    s=s)
                if marker_idx == 0:
                    ax.set_title(component,
                                 x=-0.6, y=0.5)
                if legends:
                    ax.legend([component])
        if ticks:
            plt.subplots_adjust(
                top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.65,
                wspace=0.45)
        else:
            plt.subplots_adjust(
                top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.25,
                wspace=0.35)
        return ax

    def plot_intensities(
            self,
            markers=None,
            figsize=None,
            plot_columns=None,
            n_patches=None,
            n_components=2,
            components=None,
            comps=None,
            persistence_function=None,
            max_birth_value=np.inf,
            ax=None,
            sample_size=10000,
            ticks=False,
            colors=None,
            cmap='jet',
            alpha=1,
            labels=True,
            bandwidth='silverman',
            legends=False,
            KDE=False,
            s=10):
        if comps is None:
            comps = self.components(
                n_patches=n_patches,
                n_components=n_components,
                persistence_function=persistence_function,
                max_birth_value=max_birth_value)
        if markers is None:
            markers = list(self.data.columns[[0, 1]])
        if plot_columns is None:
            plot_columns = len(markers)
        if components is None:
            components = list(range(len(comps)))
        if colors is None:
            colors = len(components) * [None]
        if figsize is None:
            figsize = (
                15, (15 / plot_columns) *
                np.ceil((1 + len(markers) +
                         np.ceil(len(markers) / plot_columns) *
                         plot_columns * len(components)) / plot_columns))
        plt.figure(figsize=figsize)
        for component_idx, component in enumerate(components):
            for marker_idx, marker in enumerate(markers):
                ax = plt.subplot(
                    np.ceil((1 + len(markers) +
                             np.ceil(len(markers) / plot_columns) *
                             plot_columns * len(components)) / plot_columns),
                    plot_columns,
                    marker_idx + 1 +
                    np.ceil(len(markers) / plot_columns) *
                    plot_columns * component_idx)
                scaler = StandardScaler()
                Z = self.data[[marker]].values
                Z = scaler.fit_transform(Z)
                estimator = FFTKDE(kernel='gaussian', bw=bandwidth)
                # x = np.linspace(np.min(Y), 1.1 * np.max(Y), 128)
                # - 0.05 * np.max(Y)
                x, y = estimator.fit(Z).evaluate(512)
                estimator = FFTKDE(kernel='gaussian', bw=bandwidth)
                Y = self.data[[marker]].iloc[
                    self.point_indices[comps[component]]].values
                Y = scaler.transform(Y)
                y = estimator.fit(Y).evaluate(x)
                x = scaler.inverse_transform(x)
                ax.plot(x, y)
                if labels and component_idx == 0:
                    ax.set_xlabel(marker)
                    ax.xaxis.set_label_position('top')
                if marker_idx == 0:
                    ax.set_title(component,
                                 x=-0.6, y=0.5)
                if legends:
                    ax.legend([component])
                ax.set_yticks(())
                ax.set_yticklabels(())
                if any((not ticks,
                        component_idx != len(components) - 1)):
                    ax.set_xticklabels(())

        if ticks:
            plt.subplots_adjust(
                top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.65,
                wspace=0.45)
        else:
            plt.subplots_adjust(
                top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.25,
                wspace=0.35)
        return ax

    def plot_intensities_overlay(
            self,
            markers=None,
            figsize=None,
            plot_columns=None,
            n_patches=None,
            n_components=2,
            components=None,
            comps=None,
            persistence_function=None,
            max_birth_value=np.inf,
            ax=None,
            sample_size=10000,
            ticks=False,
            colors=None,
            cmap='jet',
            alpha=1,
            labels=True,
            bandwidth=None,
            legends=False,
            KDE=False,
            s=10):
        if comps is None:
            comps = self.components(
                n_patches=n_patches,
                n_components=n_components,
                persistence_function=persistence_function,
                max_birth_value=max_birth_value)
        if markers is None:
            markers = list(self.data.columns[[0, 1]])
        if plot_columns is None:
            plot_columns = len(markers)
        if components is None:
            components = list(range(len(comps)))
        if colors is None:
            colors = len(components) * [None]
        if figsize is None:
            figsize = (
                15, (15 / plot_columns) *
                np.ceil((2 + len(markers) +
                         np.ceil(1 + len(markers) / plot_columns) *
                         plot_columns) / plot_columns))
        plt.figure(figsize=figsize)
        plt.subplot(
            np.ceil((1 + len(markers) +
                     np.ceil(len(markers) / plot_columns) *
                     plot_columns) / plot_columns),
            plot_columns,
            1 +
            np.ceil(len(markers) / plot_columns) *
            plot_columns)
        ax = plt.gca()
        for component_idx, component in enumerate(components):
            ax.plot([0, 1], [0, 1], label=component_idx,
                    color=colors[component_idx])
        ax.legend()
        for marker_idx, marker in enumerate(markers):
            plt.subplot(
                np.ceil((1 + len(markers) +
                         np.ceil(len(markers) / plot_columns) *
                         plot_columns) / plot_columns),
                plot_columns,
                marker_idx + 2 +
                np.ceil(len(markers) / plot_columns) *
                plot_columns)
            ax = plt.gca()
            for component_idx, component in enumerate(components):
                Y = self.data[marker].iloc[
                    self.point_indices[comps[component]]].values
                estimator = FFTKDE(kernel='gaussian', bw='silverman')
                # x = np.linspace(np.min(Y), 1.1 * np.max(Y), 128)
                # - 0.05 * np.max(Y)
                x, y = estimator.fit(Y).evaluate(256)
                ax.plot(x, y, color=colors[component_idx])
                if labels:
                    ax.set_xlabel(marker)
                    ax.xaxis.set_label_position('top')
                if marker_idx == 0:
                    ax.set_title(component,
                                 x=-0.6, y=0.5)
                if legends:
                    ax.legend([component])
                plt.yticks(())
                if not ticks:
                    plt.xticks(())
        if ticks:
            plt.subplots_adjust(
                top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.65,
                wspace=0.45)
        else:
            plt.subplots_adjust(
                top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.25,
                wspace=0.35)
        return ax

    def plot_component_array(
            self,
            markers=None,
            figsize=None,
            n_patches=None,
            n_components=2,
            components=None,
            persistence_function=None,
            max_birth_value=np.inf,
            ax=None,
            sample_size=10000,
            ticks=False,
            alpha=1,
            labels=True,
            legends=True,
            s=1):
        if components is None:
            components = list(range(n_components + 1))
        if figsize is None:
            figsize = (15, 5 * (1 + (len(components)) // 3))
        plt.figure(figsize=figsize)
        for idx, component in enumerate(components):
            plt.subplot(1 + (len(components)) // 3, 3, idx + 1)
            ax = None
            ax = self.plot_components(
                markers=markers,
                n_patches=n_patches,
                n_components=n_components,
                components=[component],
                persistence_function=persistence_function,
                max_birth_value=max_birth_value,
                ax=ax,
                sample_size=sample_size,
                ticks=ticks,
                alpha=alpha,
                labels=labels,
                s=s)
            if legends:
                ax.legend([component])

    def plot_persistence(
            self,
            X=None,
            dimension=0,
            n_patches=None,
            plot_only=None,
            title=None,
            xy_range=None,
            labels=None,
            colors=None,
            size=10,
            alpha=0.5,
            add_multiplicity=False,
            ax_color=np.array([0.0, 0.0, 0.0]),
            diagonal=True,
            lifetime=True,
            legend=True,
            show=False,
            ticks=True,
            return_plot=False
    ):
        ax = super().plot_persistence(
            X=X,
            dimension=dimension,
            n_patches=n_patches,
            plot_only=plot_only,
            title=title,
            xy_range=xy_range,
            labels=labels,
            size=size,
            alpha=alpha,
            add_multiplicity=add_multiplicity,
            ax_color=ax_color,
            colors=colors,
            diagonal=diagonal,
            lifetime=lifetime,
            legend=legend,
            show=show,
            ticks=ticks)
        return ax

    def compute_cover(self,
                      X=None,
                      n_patches=None):
        super().compute_cover(X=X,
                              n_patches=n_patches)


def read_fcs_file(filepath,
                  asinh_factor=5,
                  verbose=False):
    if verbose:
        print(filepath)
    meta, data = fcsparser.parse(filepath, reformat_meta=True)
    # pick out markers that have been used and rename columns
    Pchannels = [str for str in meta.keys() if
                 (str.endswith('S') and str.startswith('$P'))]
    Pchannel_numbers = [
        [int(s) for s in re.findall(r'\d+', channel)][0]
        for channel in Pchannels]
    names = [name for name in meta['_channel_names_']]
    for k, n in enumerate(Pchannel_numbers):
        names[n-1] = meta[Pchannels[k]]
    data.columns = names
    asinh_markers = list(meta[name] for name in Pchannels)
    data[asinh_markers] = np.arcsinh(data[asinh_markers] / 5)
    data = data[np.sort([x for x in data.columns])]
    return data


def read_fcs_file_flowCore(filepath,
                           asinh_factor=5,
                           verbose=False):
        # download and install flowCore and Biobase
    from rpy2.robjects import packages, pandas2ri
    from rpy2.robjects.packages import importr
    if verbose:
        print(filepath)
    if not packages.isinstalled('flowCore'):
        base = importr('base')
        base.source("http://www.bioconductor.org/biocLite.R")
        biocinstaller = importr("BiocInstaller")
        biocinstaller.biocLite("flowCore")
    if not packages.isinstalled('Biobase'):
        base = importr('base')
        base.source("http://www.bioconductor.org/biocLite.R")
        biocinstaller = importr("BiocInstaller")
        biocinstaller.biocLite("Biobase")

    # import flowCore and Biobase
    fc = importr('flowCore')
    bb = importr('Biobase')

    # read data (using one data file for testing)
    # filename = r'../../raw_data/FCS/Gates_PTLG001_1_Unstim.fcs'
    ff = fc.read_FCS(filepath)
    # extract data and metadata
    data = pd.DataFrame(np.array(bb.exprs(ff)))
    meta = pandas2ri.rpy2py_dataframe(bb.pData(fc.parameters(ff)))

    # change column names

    def choose_descriptor(name, desc):
        if desc == 'NA':
            return name
        else:
            return desc

    columns = [choose_descriptor(name, desc)
               for name, desc in zip(meta.name, meta.desc)]
    # select clustering markers
    asinh_markers = [choose_descriptor(name, desc) for name, desc in zip(
        meta.name, meta.desc) if desc != 'NA']
    data.columns = columns
    data = data[np.sort([x for x in data.columns])]
    data[asinh_markers] = np.arcsinh(data[asinh_markers] / 5)
    return data
