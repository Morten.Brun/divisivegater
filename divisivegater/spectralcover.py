"""Density Divisive Cover"""

# Authors: Morten Brun <morten.brun@uib.no>
# License:

import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial.distance import euclidean
from KDEpy import FFTKDE
from sklearn.manifold import spectral_embedding
from sklearn.preprocessing import StandardScaler
from scipy.spatial.distance import directed_hausdorff
# from scipy.stats.stats import _cdf_distance
from scipy.spatial.distance import cdist
from sklearn.neighbors import NearestNeighbors
# from sklearn.metrics import calinski_harabasz_score
###############################################################################
# Patch class


class Patch(object):
    """Patch object

    Parameters
    ----------
    cover : Cover, the density divisive cover the patch belongs to
    parent : Patch, the Patch object this Pathc descends from
    principal_direction : float ndarray with shape (n_dims,),
        normal direction of the defining halfspace
    split_value : float, position of defining halfspace
    side : int, either -1 or 1, to be multiplied on principal_direction
    point_indices : int ndarray of indices of points in the patch

    Attributes
    ----------
    cover : Cover, the density divisive cover the patch belongs to
    parent : Patch, the Patch object this Pathc descends from
    principal_direction : float ndarray, [n_features,],
        normal direction of the defining halfspace
    split_value : float, position of defining halfspace
    side : int, either -1 or 1, to be multiplied on principal_direction
    point_indices : int ndarray of indices of points in the patch
    halfspace : array, [n_halfspaces, n_features + 2]
        halfspaces, each halfspace specified by
        [principal_direction, split_value, side]
    center : array, [n_features,]
        Coordinates of center
    leaf_cost : int, when not split len(self.point_indices),
        if split succeded -1, else 0
    children : list, empty before split
    cluster_centers_ : array, [n_clusters, n_features]
        Coordinates of cluster centers. If the algorithm stops before fully
        converging (see ``tol`` and ``max_iter``), these will not be
        consistent with ``labels_``.
    """

    def __init__(
            self,
            cover,
            parent,
            second_moment,
            point_indices,
    ):
        self.cover = cover
        self.parent = parent
        self.point_indices = point_indices
        self.set_center()
        # self.second_moment = np.std(self.cover.X[self.point_indices])
        self.second_moment = (
            len(self.point_indices) * np.var(
                self.cover.X[self.point_indices]))
        self.children = []

    def set_center(self):
        if len(self.point_indices) == 0:
            self.center = None
        elif self.cover.center_method == 'barycenter':
            self.center = np.mean(self.cover.X[self.point_indices], axis=0)
        elif self.cover.center_method == 'median':
            self.center = np.median(self.cover.X[self.point_indices], axis=0)
        self.center_index = self.point_indices[np.argmin(
            cdist([self.center], self.cover.X[self.point_indices]).flatten())]

    def split(self):
        '''
        Split data by hyperplane giving least total variation
        '''
        point_indices = self.point_indices
        # refuse splitting if cardinality is one
        if len(point_indices) <= 1:
            self.leaf_cost = 0
            self.children = []
            return
        elif len(point_indices) == 2:
            order = np.array([0, 1])
            split = 1
        else:
            # find coordinates in principal direction
            n_neighbors = min(self.cover.n_neighbors,
                              len(self.point_indices))
            neigh = NearestNeighbors(n_neighbors=n_neighbors)
            neigh.fit(self.cover.X[self.point_indices])
            distances = neigh.kneighbors_graph(
                self.cover.X[self.point_indices])
            distances.data = distances.data ** 2
            distances.data *= -self.cover.gamma
            np.exp(distances.data, distances.data)
            distances = 0.5 * (distances + distances.transpose())
            x = spectral_embedding(distances,
                                   n_components=1)
            x = x.flatten()
            # sort by principal direction
            order = np.argsort(x)
            x = x[order]
            # split by second moment
            forward_split_costs = split_costs(x)
            forward_split_costs[[0, -1]] = np.inf
            reverse_split_costs = split_costs(-x[::-1])[::-1]
            split = 1 + np.argmin(forward_split_costs + reverse_split_costs)

        if split == 1:
            self.leaf_cost = 0
            return

        children = [
            Patch(cover=self.cover,
                  parent=self,
                  second_moment=split*forward_split_costs[split],
                  point_indices=point_indices[order[:split]]),
            Patch(cover=self.cover,
                  parent=self,
                  second_moment=(
                      (len(x) - split) * reverse_split_costs[split]),
                  point_indices=point_indices[order[split:]])
        ]
        self.children = children
        # I need to get the leaf cost right.
        # self.leaf_cost = len(self.point_indices)
        # self.leaf_cost = self.second_moment / sum(
        #    (child.second_moment for child in self.children))
        # self.leaf_cost /= self.second_moment
        # y = np.zeros(len(order), dtype=int)
        # y[order[:split]] = 1
        # self.leaf_cost = calinski_harabasz_score(
        #     self.cover.X[self.point_indices],
        #     y)
        # self.leaf_cost = (
        #     len(self.point_indices) *
        #     euclidean(*(child.center for child in children)) / sum(
        #         (child.second_moment for child in self.children)))
        self.leaf_cost = self.second_moment


class Cover(object):
    """Density Divisive Cover

    Parameters
    ----------
    n_patches : int, cardinality of the cover
    center_method : string, either 'median' or 'barycenter'
    filtration_value_fct : function or string, if string either
        'distance', 'product', 'difference' or 'density'
    distance_function : function or string, if string either
        'wall_distance' or 'center_distance' or 'cardinality_distance'

    Attributes
    ----------
    n_patches : int, cardinality of the cover
    center_method : string, either 'median' or 'barycenter'
    filtration_value_fct : function or string, if string either
        'distance', 'product', 'difference' or 'density'
    distance_function : function or string, if string either
        'cardinality_split_distance', 'split_distance',
        'center_distance', 'cardinality_distance',
        'relative_distance'or 'patch_distance'
    cover : list, list of patches in the cover
    patch_list : list, list of all patches used to construct the cover
    X : array [n_samples, n_features]
    minX : array [n_features,], np.min(X, axis=0)
    maxX : array [n_features,], np.max(X, axis=0)
    simplex_tree : gudhi simplex_tree, semplex tree used to compute
        persistent homology and clustering
    patch_fct : function, default max, determines how to make
        the unsymmetric distances between patches symmetric
    filtration_value : function, computes filtration values
    patch_graph : nx.Graph, graph of patches in the cover
    dimension : int, homological dimension

    Examples
    --------
    >>> import numpy as np
    >>> import matplotlib.pyplot as plt
    >>> from density_persistence.density_divisive_cover import Cover
    >>> np.random.seed(45)
    >>> t = np.linspace(0,2*np.pi, 100, endpoint=False)[:, np.newaxis]
    >>> X = np.hstack((np.cos(t), np.sin(t)))
    >>> X = X + 0.05 * np.random.randn(X.shape[0], X.shape[1])
    >>> X = np.vstack((X, 2*np.random.rand(200,2) - 1))
    >>> Cover = Cover(n_patches=10, center_method='median',
    ...           distance_function='cardinality_distance',
    ...           filtration_value_fct='distance')

    >>> Cover.plot_cover(X, s=10, center_size=20);
    >>> Cover.plot_persistence()
    >>> Cover.simplex_tree.persistent_homology()
    [array([[0.04712666,        inf],
            [0.05285762, 0.07295817],
            [0.0525323 , 0.07072795],
            [0.074071  , 0.07529987]]), array([[0.08996254, 0.17943537]])]
    >>> Cover.plot_cycle_representatives(num_cycles=1, max_death_value=np.inf);
    >>> Cover.plot_cover();
    >>> Cover.plot_cycle_components(num_cycles=1)
    """

    def __init__(self,
                 n_patches=10,
                 center_method='median',
                 n_neighbors=5,
                 gamma=None,
                 filtration_value_fct='distance',
                 distance_function='cardinality_split_distance',
                 svd_solver='auto'):
        self.n_patches = n_patches
        self.center_method = center_method
        self.n_neighbors = n_neighbors
        self.gamma = gamma
        # self.set_filtration_value_fct(filtration_value_fct)
        self.filtration_value_fct = filtration_value_fct
        # self.set_distance_function(distance_function)

    def remove_nerve(self):
        self.__dict__.pop('nerve', None)
        self.__dict__.pop('simplex_tree', None)

    def compute_cover(self,
                      X=None,
                      n_patches=None):
        if all((not hasattr(self, 'X'),
                X is None)):
            raise ValueError('X has to be specified')
        if all((hasattr(self, 'cover'),
                X is None,
                n_patches is None)):
            return
        self.__dict__.pop('nerve', None)
        self.__dict__.pop('simplex_tree', None)
        self.__dict__.pop('patch_graph', None)
        if n_patches is not None:
            self.n_patches = n_patches
        if X is None:
            X = self.X
        elif self.gamma is None:
            self.gamma = 1. / X.shape[1]

        self.X = X[:, np.min(X, axis=0) < np.max(X, axis=0)]
        self.minX = np.min(self.X, axis=0)
        self.maxX = np.max(self.X, axis=0)
        if self.X.shape[1] == 0:
            raise ValueError('X has to be non-degenerate')
        root_patch = Patch(
            cover=self,
            parent=None,
            second_moment=0,
            point_indices=np.arange(len(self.X)))
        self.patch_list = [root_patch]
        root_patch.split()
        next_patch = 0

        while 1 + len(self.patch_list) < 2 * self.n_patches:
            self.patch_list[next_patch].leaf_cost = -1
            new_patches = self.patch_list[next_patch].children
            for patch in new_patches:
                patch.split()

            self.patch_list.extend(new_patches)
            cost_values = np.array(
                [patch.leaf_cost for patch in self.patch_list])
            next_patch = np.argmax(cost_values)
            if cost_values[next_patch] == 0:
                break
        self.cover = [
            patch for patch in self.patch_list if patch.leaf_cost > -1]
        for idx, patch in enumerate(self.cover):
            patch.index = idx

    def plot_cover(self,
                   X=None,
                   Y=None,
                   n_patches=None,
                   ax=None,
                   s=1,
                   center_size=10,
                   alpha=1,
                   cm=plt.cm.tab10,
                   sample_size=10000,
                   num_colors=10,
                   ticks=False,
                   plot_centers=False):
        if (X is not None or
            n_patches is not None or
                not hasattr(self, 'cover')):
            self.compute_cover(X, n_patches=n_patches)
        # plt.figure()
        if Y is None:
            Y = self.X[:, [0, 1]]
        if len(self.X) != len(Y):
            raise ValueError('X and Y must be of the same length')
        sample_size = min((sample_size, len(self.X)))
        if ax is None:
            ax = plt.gca()
        colors = cm(range(num_colors))

        # colors = np.random.permutation(colors)
        for idx, patch in enumerate(self.patch_list):
            if patch.leaf_cost == -1:
                continue
            if len(patch.point_indices) > sample_size:
                sample = np.random.choice(
                    len(patch.point_indices),
                    size=sample_size,
                    replace=False)
                point_indices = patch.point_indices[sample]
            else:
                point_indices = patch.point_indices
            ax.scatter(Y[point_indices, 0],
                       Y[point_indices, 1],
                       color=colors[idx % num_colors], s=s, alpha=alpha)
        if plot_centers:
            for patch in self.cover:
                if len(patch.point_indices) == 0:
                    continue
                ax.scatter(np.mean(Y[patch.point_indices, 0]),
                           np.mean(Y[patch.point_indices, 1]),
                           c='black',
                           s=center_size)
        ax.scatter([np.min(Y[:, 0]), np.max(Y[:, 0])],
                   [np.min(Y[:, 1]), np.max(Y[:, 1])],
                   alpha=0)
        if not ticks:
            plt.xticks(())
            plt.yticks(())
        return ax


def plotX(X,
          x_axis=0,
          y_axis=1,
          bandwidth=None,
          grid_points=1024,
          sample_size=1000,
          ticks=False,
          size=1,
          color=None,
          cmap='Blues',
          KDE=False,
          ax=None,
          label=None,
          alpha=1):
    if ax is None:
        ax = plt.gca()
    if KDE:
        return KDEplotX(
            X=X,
            x_axis=x_axis,
            y_axis=y_axis,
            bandwidth=bandwidth,
            grid_points=grid_points,
            ticks=ticks,
            cmap=cmap,
            ax=ax,
            label=label,
            alpha=alpha)
    else:
        return plotX_sample(
            X=X,
            x_axis=x_axis,
            y_axis=y_axis,
            sample_size=sample_size,
            size=size,
            ticks=ticks,
            ax=ax,
            label=label,
            color=color,
            alpha=alpha)


def plotX_sample(X,
                 x_axis=0,
                 y_axis=1,
                 sample_size=None,
                 size=1,
                 ticks=False,
                 color=None,
                 label=None,
                 ax=None,
                 alpha=1):
    if ax is None:
        ax = plt.gca()
    ax.scatter([np.min(X[:, x_axis]), np.max(X[:, x_axis])],
               [np.min(X[:, y_axis]), np.max(X[:, y_axis])],
               alpha=0, color='black')
    if sample_size is None:
        sample_size = len(X)
    if len(X) > sample_size:
        sample = np.random.choice(
            len(X),
            size=sample_size,
            replace=False)
        X = X[sample]
    ax.scatter(X[:, x_axis],
               X[:, y_axis],
               s=size,
               alpha=alpha,
               color=color,
               label=label)
    if not ticks:
        plt.xticks(())
        plt.yticks(())
    return ax


def KDEplotX(X,
             x_axis=0,
             y_axis=1,
             bandwidth=None,
             grid_points=1024,
             ticks=False,
             ax=None,
             cmap='Blues',
             label=None,
             alpha=1):
    if ax is None:
        ax = plt.gca()
    coords = X[:, [x_axis, y_axis]]
    scaler = StandardScaler()
    coords = scaler.fit_transform(coords)
    n, d = coords.shape
    if bandwidth is None:
        bandwidth = n**(-1./(d+4))
    # kde_fft = FFTKDE(kernel='tri', bw=bandwidth).fit(coords)
    kde_fft = FFTKDE(kernel='gaussian', bw=bandwidth).fit(coords)
    grid, y = kde_fft.evaluate(grid_points=1024)
    grid = scaler.inverse_transform(grid)
    # Plot density heat map
    xmin, xmax = np.min(grid[:, 0]), np.max(grid[:, 0])
    ymin, ymax = np.min(grid[:, 1]), np.max(grid[:, 1])

    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    ax.imshow(y.reshape((
        int(np.sqrt(len(y))), int(np.sqrt(len(y))))).transpose(),
        cmap=cmap,
        origin='lower',
        extent=[xmin, xmax, ymin, ymax],
        alpha=alpha)
    if not ticks:
        plt.xticks(())
        plt.yticks(())
    ax.set_aspect((xmax - xmin) / (ymax - ymin))
    return ax


def division(x, y):
    '''
    Safe division.
    Division by zero yields np.inf
    '''
    if y != 0:
        return x/y
    else:
        return np.inf


def second_moment_sum(X):
    '''
    This cost is total variation times local density
    '''
    return np.sum(np.var(X, axis=0)) * len(X)


def second_moment_max(X):
    '''
    This cost is total variation times local density
    '''
    return np.max(np.var(X, axis=0)) * len(X)


def second_moment_variance(X):
    '''
    This cost is total variation times local density
    '''
    return np.max(np.var(X, axis=0))


def second_moment_variation(X):
    '''
    This cost is total variation times local density
    '''
    return np.sum(np.var(X, axis=0))


def split_costs(x):
    '''
    This is a one-dimensional version of the patch_cost.
    Using two-pass version of Welford's online algorithm
    https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
    '''
    return np.cumsum((
        (np.arange(1, len(x) + 1) * x - np.cumsum(x)) /
        np.arange(1, len(x) + 1))**2)


def get_principal_directions(X, num_directions=1, svd_solver='auto'):
    pca = PCA(n_components=num_directions, svd_solver=svd_solver,
              whiten=False).fit(X)
    return pca.components_


def patch_distance(self, patch, fct=max):
    return fct(
        self.distance_to_point(patch.center),
        patch.distance_to_point(self.center))


def relative_distance(self, patch, fct=max):
    return fct(
        self.distance_to_point(patch.center)**2 /
        patch.center_to_boundary_distance(),
        patch.distance_to_point(self.center)**2 /
        self.center_to_boundary_distance())


def cardinality_distance(self, patch, fct=max):
    return fct(
        self.distance_to_point(patch.center) *
        len(self.point_indices)**(1 / len(self.principal_direction)),
        patch.distance_to_point(self.center) *
        len(patch.point_indices)**(1 / len(self.principal_direction)))


def center_distance(self, patch, fct=max):
    return euclidean(patch.center, self.center)


def supremum_distance(self, patch, fct=max):
    return np.max(np.abs(patch.center - self.center))


def percentile_distance(self, patch, fct=None):
    if fct is None:
        fct = 0.5
    return np.percentile(np.max(np.abs(
        patch.center - self.cover.X[self.point_indices])), q=fct)


def split_distance(self, patch, fct=max):
    principal_directions = np.vstack((
        self.get_unreduced_halfspaces()[:, :-2],
        patch.get_unreduced_halfspaces()[:, :-2]))
    return np.max(np.abs(np.dot(
        principal_directions,
        (self.center - patch.center).transpose())))


def cardinality_split_distance(self, patch, fct=max):
    if self.center is None or patch.center is None:
        return np.inf
    principal_directions = np.vstack((
        self.get_unreduced_halfspaces()[:, :-2],
        patch.get_unreduced_halfspaces()[:, :-2]))
    relative_cardinality = (
        len(self.cover.X) /
        (len(self.point_indices) +
         len(patch.point_indices)))**(1.0 / principal_directions.shape[1])

    return (np.max(np.abs(np.dot(
        principal_directions,
        (self.center - patch.center).transpose()))) *
        relative_cardinality)


# def wasserstein_distance(self, patch, fct=None):
#     if self == patch:
#         return 0.
#     a = self.center - patch.center
#     a = a / np.linalg.norm(a)
#     a = a[:, np.newaxis]
#     u = np.dot(self.cover.X[self.point_indices], a).flatten()
#     v = np.dot(self.cover.X[patch.point_indices], a).flatten()
#     return stats.wasserstein_distance(u, v)


def hausdorff_distance(self, patch, fct=None):
    return max(
        directed_hausdorff(self.cover.X[self.point_indices],
                           patch.cover.X[patch.point_indices])[0],
        directed_hausdorff(patch.cover.X[patch.point_indices],
                           self.cover.X[self.point_indices])[0]
    )


# def energy_distance(self, patch, fct=None):
#     if self == patch:
#         return 0.
#     a = self.center - patch.center
#     a = a / np.linalg.norm(a)
#     a = a[:, np.newaxis]
#     u = np.dot(self.cover.X[self.point_indices], a).flatten()
#     v = np.dot(self.cover.X[patch.point_indices], a).flatten()
#     return stats.energy_distance(u, v)
# 
# def wasserstein_distance(p=1):
#     def fct(self, patch, fct=None):
#         if self == patch:
#             return 0.
#         a = self.center - patch.center
#         a = a / np.linalg.norm(a)
#         a = a[:, np.newaxis]
#         u = np.dot(self.cover.X[self.point_indices], a).flatten()
#         v = np.dot(self.cover.X[patch.point_indices], a).flatten()
#         return _cdf_distance(p, u, v) ** (1/p)
#     return fct
# 

def deviance_distance(self, patch, fct=None):
    point_indices = np.hstack((self.point_indices,
                               patch.point_indices))
    Y = np.unique(self.cover.X[point_indices], axis=0)
    if len(Y) == 1:
        return 0
    pca = PCA(n_components=1, whiten=False).fit(self.cover.X[point_indices])
    return np.sqrt(pca.explained_variance_[0])


def ward_deviance_distance(self, patch, fct=None):
    nself = len(self.point_indices)
    npatch = len(patch.point_indices)
    ward_factor = np.sqrt(1 / (nself + npatch))
    point_indices = np.hstack((self.point_indices,
                               patch.point_indices))
    Y = np.unique(self.cover.X[point_indices], axis=0)
    if len(Y) == 1:
        return 0
    pca = PCA(n_components=1, whiten=False).fit(self.cover.X[point_indices])
    return np.sqrt(pca.explained_variance_[0]) / ward_factor
