"""Density Divisive Cover"""

# Authors: Morten Brun <morten.brun@uib.no>
# License:

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from . simplex_tree import SimplexTree
import networkx as nx
from collections import deque
from itertools import chain, islice, cycle, combinations
from cvxopt import matrix, solvers
from scipy.spatial.distance import euclidean
from KDEpy import FFTKDE
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from scipy.spatial.distance import directed_hausdorff
# from scipy.stats.stats import _cdf_distance
from scipy.spatial.distance import cdist
###############################################################################
# Patch class


class Patch(object):
    """Patch object

    Parameters
    ----------
    cover : Cover, the density divisive cover the patch belongs to
    parent : Patch, the Patch object this Pathc descends from
    principal_direction : float ndarray with shape (n_dims,),
        normal direction of the defining halfspace
    split_value : float, position of defining halfspace
    side : int, either -1 or 1, to be multiplied on principal_direction
    point_indices : int ndarray of indices of points in the patch

    Attributes
    ----------
    cover : Cover, the density divisive cover the patch belongs to
    parent : Patch, the Patch object this Pathc descends from
    principal_direction : float ndarray, [n_features,],
        normal direction of the defining halfspace
    split_value : float, position of defining halfspace
    side : int, either -1 or 1, to be multiplied on principal_direction
    point_indices : int ndarray of indices of points in the patch
    halfspace : array, [n_halfspaces, n_features + 2]
        halfspaces, each halfspace specified by
        [principal_direction, split_value, side]
    center : array, [n_features,]
        Coordinates of center
    leaf_cost : int, when not split len(self.point_indices),
        if split succeded -1, else 0
    children : list, empty before split
    cluster_centers_ : array, [n_clusters, n_features]
        Coordinates of cluster centers. If the algorithm stops before fully
        converging (see ``tol`` and ``max_iter``), these will not be
        consistent with ``labels_``.
    """

    def __init__(
            self,
            cover,
            parent,
            principal_direction,
            split_value,
            side,
            point_indices,
    ):
        self.cover = cover
        self.parent = parent
        self.point_indices = point_indices

        self.principal_direction = principal_direction
        self.split_value = split_value
        self.side = side
        if self.parent is not None:
            self.halfspace = np.append(
                self.principal_direction,
                (self.split_value, self.side))
        else:
            self.halfspace = np.zeros(self.cover.X.shape[1] + 2)
        self.leaf_cost = len(self.point_indices)
        self.set_center()
        self.children = []

    def set_center(self):
        if len(self.point_indices) == 0:
            self.center = None
        elif self.cover.center_method == 'barycenter':
            self.center = np.mean(self.cover.X[self.point_indices], axis=0)
        elif self.cover.center_method == 'median':
            self.center = np.median(self.cover.X[self.point_indices], axis=0)
        self.center_index = self.point_indices[np.argmin(
            cdist([self.center], self.cover.X[self.point_indices]).flatten())]

    def populate(self, point_indices):
        if self.parent is None:
            self.point_indices = point_indices
        else:
            halfspace = self.halfspace[:-1] * self.halfspace[-1]
            self.point_indices = point_indices[np.dot(
                self.cover.X[point_indices],
                halfspace[:-1]) <= halfspace[-1]]
        self.set_center()
        for child in self.children:
            child.populate(self.point_indices)

    def neighbors(self, patch):
        if self.collision(patch):
            if self.leaf_cost > -1:
                return [self]
            else:
                return list(chain(*(child.neighbors(patch) for
                                    child in self.children)))
        else:
            return []

    def center_to_boundary_distance(self):
        if self.center is None:
            return np.inf
        distances = []
        for halfspace in self.get_halfspaces():
            distances.append(
                halfspace[-1] *
                (halfspace[-2] - np.dot(self.center, halfspace[:-2])))
        return min(distances)

    def distance_to_point(self, point):
        halfspaces = self.get_halfspaces()
        P = matrix(np.identity(halfspaces.shape[1] - 2), tc='d')
        q = matrix(-point, tc='d')
        G = matrix(halfspaces[:, [-1]] * halfspaces[:, :-2], tc='d')
        h = matrix(halfspaces[:, -1] * halfspaces[:, -2], tc='d')
        sol = solvers.qp(P, q, G, h, solver=self.cover.cone_solver)
        return np.sqrt(
            2 * (sol['primal objective'] + 0.5 * np.dot(point, point)))

    def collision(self, patch):
        '''
        Report collision with patch
        '''
        halfspaces = np.vstack((
            self.get_halfspaces(),
            patch.get_halfspaces()))
        G = matrix(np.vstack((
            halfspaces[:, [-1]] * halfspaces[:, :-2],
            np.identity(halfspaces.shape[1] - 2),
            -np.identity(halfspaces.shape[1] - 2))),
            tc='d')
        h = matrix(np.vstack((
            halfspaces[:, [-1]] * halfspaces[:, [-2]],
            self.cover.maxX[:, np.newaxis],
            -self.cover.minX[:, np.newaxis])),
            tc='d')
        c = matrix(np.zeros(self.cover.X.shape[1]), tc='d')
        sol = solvers.lp(c, G, h, solver=self.cover.cone_solver)
        if sol['status'] == 'primal infeasible':
            return False
        else:
            return True

    def get_unreduced_halfspaces(self):
        if self.parent is not None:
            h = self.parent.get_unreduced_halfspaces()
            return np.vstack((h, self.halfspace))
        else:
            return self.halfspace

    def get_halfspaces(self):
        if hasattr(self, 'reduced_halfspaces'):
            return self.reduced_halfspaces
        halfspaces = self.get_unreduced_halfspaces()
        G = matrix(np.vstack((
            halfspaces[:, [-1]] * halfspaces[:, :-2],
            np.identity(halfspaces.shape[1] - 2),
            -np.identity(halfspaces.shape[1] - 2))),
            tc='d')
        # print(
        #     (halfspaces[:, [-1]] * halfspaces[:, :-2]).shape,
        #     np.identity(halfspaces.shape[1] - 2).shape)
        h = matrix(np.vstack((
            halfspaces[:, [-1]] * halfspaces[:, [-2]],
            self.cover.maxX[:, np.newaxis],
            -self.cover.minX[:, np.newaxis])),
            tc='d')
        # print(
        #     (halfspaces[:, [-1]] * halfspaces[:, [-2]]).shape,
        #     self.cover.maxX[:, np.newaxis].shape)
        reduced_halfspaces = np.ones(len(halfspaces), dtype=bool)
        for index, hs in enumerate(halfspaces):
            c = matrix(-hs[-1] * hs[:-2], tc='d')
            h[index] += 1
            sol = solvers.lp(c, G, h, solver=self.cover.cone_solver)
            reduced_halfspaces[index] = (
                np.dot(np.array(-sol['x']).flatten(),
                       np.array(c).flatten()) > hs[-1] * hs[-2])
            h[index] = hs[-1] * hs[-2]
        self.reduced_halfspaces = halfspaces[reduced_halfspaces]
        return self.reduced_halfspaces

    def split(self):
        '''
        Split data by hyperplane giving least total variation
        '''
        point_indices = self.point_indices
        # refuse splitting if cardinality is one
        if len(np.unique(self.cover.X[point_indices], axis=0)) <= 1:
            self.leaf_cost = 0
            self.children = []
            return
        # find coordinates in principal direction
        principal_direction = get_principal_directions(
            self.cover.X[point_indices],
            svd_solver=self.cover.svd_solver).flatten()
        x = np.dot(self.cover.X[point_indices], principal_direction)
        # sort by principal direction
        order = np.argsort(x)
        x = x[order]
        # split by second moment
        forward_split_costs = split_costs(x)
        forward_split_costs[[0, -1]] = np.inf
        reverse_split_costs = split_costs(-x[::-1])[::-1]
        split = 1 + np.argmin(forward_split_costs + reverse_split_costs)
        # self.leaf_cost = reverse_split_costs[split] + \
        #     forward_split_costs[split]
        # if not np.isfinite(self.leaf_cost):
        #     self.leaf_cost = np.var(x)
        #     # split = max(0, split - 1)
        #     # self.leaf_cost = reverse_split_costs[split] + \
        #     #    forward_split_costs[split]

        # if split == 1:
        #     self.leaf_cost = 0
        #     return []
        split_value = (x[split - 1] + x[split]) / 2
        sign = np.sign(
            principal_direction[np.flatnonzero(principal_direction)[0]])
        principal_direction *= sign
        split_value *= sign

        children = [
            Patch(cover=self.cover,
                  parent=self,
                  principal_direction=principal_direction,
                  split_value=split_value,
                  side=sign,
                  point_indices=point_indices[order[:split]]),
            Patch(cover=self.cover,
                  parent=self,
                  principal_direction=principal_direction,
                  split_value=split_value,
                  side=-sign,
                  point_indices=point_indices[order[split:]])
        ]
        self.children = children


class Cover(object):
    """Density Divisive Cover

    Parameters
    ----------
    n_patches : int, cardinality of the cover
    center_method : string, either 'median' or 'barycenter'
    filtration_value_fct : function or string, if string either
        'distance', 'product', 'difference' or 'density'
    distance_function : function or string, if string either
        'cardinality_split_distance', 'split_distance',
        'center_distance', 'supremum_distance',
        'cardinality_distance', 'wasserstein_distance',
        'energy_distance', 'hausdorff_distance',
        'deviance_distance'

    Attributes
    ----------
    n_patches : int, cardinality of the cover
    center_method : string, either 'median' or 'barycenter'
    filtration_value_fct : function
    distance_function : function
    cover : list, list of patches in the cover
    patch_list : list, list of all patches used to construct the cover
    X : array [n_samples, n_features]
    minX : array [n_features,], np.min(X, axis=0)
    maxX : array [n_features,], np.max(X, axis=0)
    simplex_tree : gudhi simplex_tree, semplex tree used to compute
        persistent homology and clustering
    patch_fct : function, default max, determines how to make
        the unsymmetric distances between patches symmetric
    filtration_value : function, computes filtration values
    patch_graph : nx.Graph, graph of patches in the cover
    dimension : int, homological dimension

    Examples
    --------
    >>> import numpy as np
    >>> import matplotlib.pyplot as plt
    >>> from density_persistence.density_divisive_cover import Cover
    >>> np.random.seed(45)
    >>> t = np.linspace(0,2*np.pi, 100, endpoint=False)[:, np.newaxis]
    >>> X = np.hstack((np.cos(t), np.sin(t)))
    >>> X = X + 0.05 * np.random.randn(X.shape[0], X.shape[1])
    >>> X = np.vstack((X, 2*np.random.rand(200,2) - 1))
    >>> Cover = Cover(n_patches=10, center_method='median',
    ...           distance_function='cardinality_distance',
    ...           filtration_value_fct='distance')

    >>> Cover.plot_cover(X, s=10, center_size=20);
    >>> Cover.plot_persistence()
    >>> Cover.simplex_tree.persistent_homology()
    [array([[0.04712666,        inf],
            [0.05285762, 0.07295817],
            [0.0525323 , 0.07072795],
            [0.074071  , 0.07529987]]), array([[0.08996254, 0.17943537]])]
    >>> Cover.plot_cycle_representatives(num_cycles=1, max_death_value=np.inf);
    >>> Cover.plot_cover();
    >>> Cover.plot_cycle_components(num_cycles=1)
    """

    def __init__(self,
                 n_patches=10,
                 center_method='median',
                 filtration_value_fct='distance',
                 distance_function='cardinality_split_distance',
                 svd_solver='auto',
                 cone_solver='glpk'):
        self.n_patches = n_patches
        self.center_method = center_method
        # self.set_filtration_value_fct(filtration_value_fct)
        self.filtration_value_fct = filtration_value_fct
        self.set_distance_function(distance_function)
        self.svd_solver = svd_solver
        self.cone_solver = cone_solver
        solvers.options['show_progress'] = False
        solvers.options['glpk'] = dict(msg_lev='GLP_MSG_OFF')

    def subcover(self,
                 cover_indices):
        if not hasattr(self, 'simplex_tree'):
            raise ValueError('Not ready to construct subcover')
        subcov = Cover(
            n_patches=self.n_patches,
            center_method=self.center_method,
            filtration_value_fct=self.filtration_value_fct,
            distance_function=self.distance_function,
            svd_solver=self.svd_solver)
        subcov.X = self.X
        subcov.parent = self
        subcov.dimension = self.dimension
        subcov.cover = self.cover
        subcov.patch_graph = self.patch_graph
        subcov.simplex_tree = self.simplex_tree.sub_simplex_tree(
            set(cover_indices))
        return subcov

    def remove_nerve(self):
        self.__dict__.pop('nerve', None)
        self.__dict__.pop('simplex_tree', None)

    def populate(self, X):
        self.X = X
        self.remove_nerve()
        self.patch_list[0].populate(np.arange(len(X)))

    def compute_cover(self,
                      X=None,
                      n_patches=None):
        if all((not hasattr(self, 'X'),
                X is None)):
            raise ValueError('X has to be specified')
        if all((hasattr(self, 'cover'),
                X is None,
                n_patches is None)):
            return
        self.__dict__.pop('nerve', None)
        self.__dict__.pop('simplex_tree', None)
        self.__dict__.pop('patch_graph', None)
        if n_patches is not None:
            self.n_patches = n_patches
        if X is None:
            X = self.X

        self.X = X[:, np.min(X, axis=0) < np.max(X, axis=0)]
        self.minX = np.min(self.X, axis=0)
        self.maxX = np.max(self.X, axis=0)
        if self.X.shape[1] == 0:
            raise ValueError('X has to be non-degenerate')
        root_patch = Patch(
            cover=self,
            parent=None,
            principal_direction=get_principal_directions(
                self.X,
                svd_solver=self.svd_solver).flatten(),
            split_value=0,
            point_indices=np.arange(len(self.X)),
            side=0)
        self.patch_list = [root_patch]
        root_patch.split()
        next_patch = 0

        while 1 + len(self.patch_list) < 2 * self.n_patches:
            self.patch_list[next_patch].leaf_cost = -1
            new_patches = self.patch_list[next_patch].children
            for patch in new_patches:
                patch.split()

            self.patch_list.extend(new_patches)
            cost_values = np.array(
                [patch.leaf_cost for patch in self.patch_list])
            next_patch = np.argmax(cost_values)
            if cost_values[next_patch] == 0:
                break
        self.cover = [
            patch for patch in self.patch_list if patch.leaf_cost > -1]
        for idx, patch in enumerate(self.cover):
            patch.index = idx

    def set_distance_function(
            self, distance_function='cardinality_split_distance'):
        if hasattr(self, 'simplex_tree'):
            del self.simplex_tree
        if distance_function == 'cardinality_split_distance':
            distance_function = cardinality_split_distance
        elif distance_function == 'split_distance':
            distance_function = split_distance
        elif distance_function == 'center_distance':
            distance_function = center_distance
        elif distance_function == 'supremum_distance':
            distance_function = supremum_distance
        elif distance_function == 'cardinality_distance':
            distance_function = cardinality_distance
        elif distance_function == 'relative_distance':
            distance_function = relative_distance
        elif distance_function == 'wasserstein_distance':
            distance_function = wasserstein_distance(p=1)
        elif distance_function == 'energy_distance':
            distance_function = wasserstein_distance(p=2)
        elif distance_function == 'hausdorff_distance':
            distance_function = hausdorff_distance
        elif distance_function == 'deviance_distance':
            distance_function = deviance_distance
        self.distance_function = distance_function

    def set_filtration_value_fct(self, fct='distance'):
        if hasattr(self, 'simplex_tree'):
            del self.simplex_tree
        self.patch_fct = max
        if fct == 'distance':
            def fct(self, face):
                if len(face) == 1:
                    return np.inf
                    # vertex = list(face)[0]
                    # return self.cover[vertex].center_to_boundary_distance()
                else:
                    return max((self.patch_graph.edges[edge]['weight'] for edge
                                in combinations(face, 2)))
        if fct == 'self_distance':
            def fct(self, face):
                if len(face) == 1:
                    vertex = list(face)[0]
                    return self.cover[vertex].center_to_boundary_distance()
                else:
                    return max((self.patch_graph.edges[edge]['weight'] for edge
                                in combinations(face, 2)))
        elif fct == 'product':
            def patch_distance(p, q):
                return p * q
            self.patch_fct = patch_distance

            def fct(self, face):
                if len(face) == 1:
                    return np.inf
                    # return 1.0
                else:
                    return (
                        max((self.patch_graph.edges[edge]['weight'] for edge
                             in combinations(face, 2))) /
                        min([self.cover[vertex].center_to_boundary_distance()
                             for vertex in face]))
        elif fct == 'difference':
            def patch_distance(p, q):
                return np.abs(p - q)
            self.patch_fct = patch_distance

            def fct(self, face):
                if len(face) == 1:
                    return 0.0
                else:
                    return (
                        max((self.patch_graph.edges[edge]['weight'] for edge
                             in combinations(face, 2)))
                        -
                        min([self.cover[vertex].center_to_boundary_distance()
                             for vertex in face]))
        elif fct == 'density':
            def fct(self, face):
                wall_distances = [
                    patch.center_to_boundary_distance()
                    for patch in [self.cover[vertex] for vertex in face]]
                return np.max(wall_distances)

        self.filtration_value_fct = fct
        self.filtration_value = fct

    def graph(self, X=None, n_patches=None):
        if (X is not None or
            n_patches is not None or
                not hasattr(self, 'cover')):
            self.compute_cover(X, n_patches)
        if not hasattr(self, 'patch_graph'):
            G = nx.Graph()
            if n_patches == 1:
                patch = self.cover[0]
                G.add_edge(0, 0, weight=self.distance_function(
                    patch, patch, fct=self.patch_fct))
            else:
                for s, s_patch in enumerate(self.cover):
                    for idx in [1, 2]:
                        for t_patch in self.patch_list[idx].neighbors(s_patch):
                            G.add_edge(
                                s, t_patch.index,
                                weight=self.distance_function(
                                    s_patch, t_patch, fct=self.patch_fct))
        else:
            return self.patch_graph
        self.patch_graph = G
        return self.patch_graph

    def set_simplex_tree(self, X=None, n_patches=None, dimension=None):
        if dimension is None:
            dimension = self.dimension
        if hasattr(self, 'simplex_tree'):
            if all((X is None,
                    n_patches is None,
                    dimension == self.dimension)):
                return
        self.set_filtration_value_fct(self.filtration_value_fct)
        if not hasattr(self, 'patch_graph') or not all(
                (X is None, n_patches is None)):
            G = self.graph(X=X, n_patches=n_patches)
        else:
            G = self.patch_graph
        # G.remove_edges_from(G.selfloop_edges())
        self.simplex_tree = SimplexTree(persistence_dim_max=False)
        if dimension is None:
            if not hasattr(self, 'dimension'):
                self.dimension = 0
        else:
            self.dimension = dimension
        if self.dimension == 0:
            for node in G.nodes:
                self.simplex_tree.insert(
                    [node], self.filtration_value(self, [node]))
            for source, target in G.edges:
                if source == target:
                    continue
                self.simplex_tree.insert(
                    [source, target],
                    self.filtration_value(self, [source, target]))
        else:
            for face in enumerate_cliques(G, self.dimension + 2):
                face_indices = [vertex for vertex in face]
                self.simplex_tree.insert(face_indices,
                                         self.filtration_value(self, face))

    def plot_persistence(self,
                         X=None,
                         dimension=None,
                         n_patches=None,
                         plot_only=None,
                         title=None,
                         xy_range=None,
                         labels=None,
                         colormap="default",
                         size=10,
                         alpha=0.5,
                         add_multiplicity=False,
                         ax_color=np.array([0.0, 0.0, 0.0]),
                         colors=None,
                         diagonal=True,
                         lifetime=True,
                         legend=True,
                         show=False,
                         ticks=True,
                         ):
        """
        Show or save persistence diagram

        Parameters
        ----------
        plot_only: list of numeric
            If specified, an array of only the diagrams that should be plotted.
        title: string, default is None
            If title is defined, add it as title of the plot.
        xy_range: list of numeric [xmin, xmax, ymin, ymax]
            User provided range of axes. This is useful for comparing \
            multiple persistence diagrams.
        labels: string or list of strings
            Legend labels for each diagram. \
            If none are specified, we use H_0, H_1, H_2,... by default.
        colormap: str (default : 'default')
            Any of matplotlib color palettes. \
            Some options are 'default', 'seaborn', 'sequential'.
        size: numeric (default : 10)
            Pixel size of each point plotted.
        alpha: numeric (default : 0.5)
            Transparency of each point plotted.
        add_multiplicity: boolean (default : False)
            Show multiplicity of points plotted.
        ax_color: any valid matplotlib color type.
            See https://matplotlib.org/api/colors_api.html for complete API.
        diagonal: bool (default : True)
            Plot the diagonal x=y line.
        lifetime: bool (default : False). If True, diagonal is turned to False.
            Plot life time of each point instead of birth and death.  \
            Essentially, visualize (x, y-x).
        legend: bool (default : True)
            If true, show the legend.
        show: bool (default : False)
            Call plt.show() after plotting. If you are using self.plot() as \
            part of a subplot, set show=False and call plt.show() only once \
            at the end.
        """
        self.set_simplex_tree(X, n_patches, dimension)
        ax = self.simplex_tree.plot_persistence(
            plot_only=plot_only,
            title=title,
            xy_range=xy_range,
            labels=labels,
            colormap=colormap,
            size=size,
            alpha=alpha,
            add_multiplicity=add_multiplicity,
            ax_color=ax_color,
            colors=colors,
            diagonal=diagonal,
            lifetime=lifetime,
            legend=legend,
            show=show,
        )
        if not ticks:
            plt.xticks(())
            plt.yticks(())
        return ax

    def plot_cover(self,
                   X=None,
                   Y=None,
                   n_patches=None,
                   ax=None,
                   s=1,
                   center_size=10,
                   alpha=1,
                   cm=plt.cm.tab20b,
                   sample_size=10000,
                   num_colors=20,
                   ticks=False,
                   plot_centers=False):
        if (X is not None or
            n_patches is not None or
                not hasattr(self, 'cover')):
            self.compute_cover(X, n_patches=n_patches)
        # plt.figure()
        if Y is None:
            Y = self.X[:, [0, 1]]
        if len(self.X) != len(Y):
            raise ValueError('X and Y must be of the same length')
        sample_size = min((sample_size, len(self.X)))
        if ax is None:
            ax = plt.gca()
        colors = cm(range(num_colors))

        # colors = np.random.permutation(colors)
        for idx, patch in enumerate(self.patch_list):
            if patch.leaf_cost == -1:
                continue
            if len(patch.point_indices) > sample_size:
                sample = np.random.choice(
                    len(patch.point_indices),
                    size=sample_size,
                    replace=False)
                point_indices = patch.point_indices[sample]
            else:
                point_indices = patch.point_indices
            ax.scatter(Y[point_indices, 0],
                       Y[point_indices, 1],
                       color=colors[idx % num_colors], s=s, alpha=alpha)
        if plot_centers:
            for patch in self.cover:
                if len(patch.point_indices) == 0:
                    continue
                ax.scatter(np.mean(Y[patch.point_indices, 0]),
                           np.mean(Y[patch.point_indices, 1]),
                           c='black',
                           s=center_size)
        ax.scatter([np.min(Y[:, 0]), np.max(Y[:, 0])],
                   [np.min(Y[:, 1]), np.max(Y[:, 1])],
                   alpha=0)
        if not ticks:
            plt.xticks(())
            plt.yticks(())
        return ax

    def cycle_patches(self,
                      X=None,
                      n_patches=None,
                      cycle_num=None,
                      max_birth_value=np.inf,
                      max_death_value=np.inf,
                      persistence_function=None):
        if (X is not None or
            n_patches is not None or
                not hasattr(self, 'simplex_tree')):
            self.set_simplex_tree(X, n_patches=n_patches, dimension=1)
        if any((not hasattr(self, 'cycle_clust'),
                cycle_num is not None,
                persistence_function is not None)):
            if persistence_function is None:
                if cycle_num is None:
                    cycle_num = 0
                one_pers = np.sort(
                    [pers[1][1] - pers[1][0] for pers in
                     self.simplex_tree.persistence() if
                     (pers[0] == 1 and pers[1][0] <= max_birth_value)])
                num_cycles = min(cycle_num + 1, len(one_pers))
                if num_cycles == 0:
                    return
                persistence_function = one_pers[-num_cycles]
            self.cycle_clust = self.simplex_tree.cycle_components(
                persistence_function=persistence_function,
                max_birth_value=max_birth_value,
                max_death_value=max_death_value)
        return [self.cover[idx] for idx in self.cycle_clust[-1]]

    def plot_cycle_components(self,
                              X=None,
                              Y=None,
                              n_patches=None,
                              cycle_num=None,
                              x_axis=0,
                              y_axis=1,
                              alpha=0.1,
                              s=1,
                              c='black',
                              center_size=10,
                              sample_size=0,
                              max_birth_value=np.inf,
                              max_death_value=np.inf,
                              ax=None,
                              ticks=False,
                              persistence_function=None,
                              plot_centers=False):
        cycle_patches = self.cycle_patches(
            X=X,
            n_patches=n_patches,
            cycle_num=cycle_num,
            max_birth_value=max_birth_value,
            max_death_value=max_death_value,
            persistence_function=persistence_function)

        if Y is None:
            Y = self.X[:, [0, 1]]
        if len(self.X) != len(Y):
            raise ValueError('X and Y must be of the same length')
        if ax is None:
            ax = plt.gca()
        if cycle_patches is None:
            return ax
        for patch in cycle_patches:
            if len(patch.point_indices) > sample_size:
                sample = np.random.choice(
                    len(patch.point_indices),
                    size=sample_size,
                    replace=False)
                point_indices = patch.point_indices[sample]
            else:
                point_indices = patch.point_indices
            ax.scatter(Y[point_indices, 0],
                       Y[point_indices, 1], s=s, alpha=alpha)
        if plot_centers:
            for patch in cycle_patches:
                ax.scatter(np.mean(Y[point_indices, 0]),
                           np.mean(Y[point_indices, 1]),
                           c=c,
                           s=center_size)
        ax.scatter([np.min(Y[:, 0]), np.max(Y[:, 0])],
                   [np.min(Y[:, 1]), np.max(Y[:, 1])],
                   alpha=0)
        if not ticks:
            plt.xticks(())
            plt.yticks(())

        return ax

    def plot_cycle_representatives(self,
                                   X=None,
                                   Y=None,
                                   n_patches=None,
                                   num_cycles=1,
                                   cycles=None,
                                   max_birth_value=np.inf,
                                   max_death_value=np.inf,
                                   x_axis=0,
                                   y_axis=1,
                                   s=1,
                                   alpha=0.1,
                                   ax=None,
                                   KDE=False,
                                   grid_points=1024,
                                   sample_size=1000,
                                   bandwidth=None,
                                   legend=True,
                                   plot_data=False,
                                   ticks=False,
                                   linewidth=1,
                                   size=1,
                                   alpha_data=0.5,
                                   persistence_function=None):
        if ax is None:
            ax = plt.gca()
        if cycles is not None:
            num_cycles = max(cycles) + 1
        elif num_cycles is not None:
            cycles = list(range(num_cycles))
        if (X is not None or
            n_patches is not None or
                not hasattr(self, 'simplex_tree')):
            self.set_simplex_tree(X, n_patches=n_patches, dimension=1)
        if Y is None:
            Y = self.X[:, [0, 1]]
        if len(self.X) != len(Y):
            raise ValueError('X and Y must be of the same length')
        if plot_data:
            ax = plotX(X=Y,
                       bandwidth=bandwidth,
                       grid_points=grid_points,
                       sample_size=sample_size,
                       ticks=ticks,
                       size=size,
                       KDE=KDE,
                       ax=ax,
                       alpha=alpha_data)
        if any((not hasattr(self, 'rep_cycles'),
                num_cycles is not None,
                persistence_function is not None)):
            if persistence_function is None:
                if num_cycles is None:
                    num_cycles = 2
            one_pers = np.sort(
                [pers[1][1] - pers[1][0] for pers in
                 self.simplex_tree.persistence() if (
                     pers[0] == 1 and
                     pers[1][1] <= max_death_value and
                     pers[1][0] <= max_birth_value)])
            num_cycles = min(num_cycles, len(one_pers))
            if num_cycles == 0:
                return ax
            if persistence_function is None:
                persistence_function = one_pers[-num_cycles]
            self.rep_cycles = self.cycle_representatives(
                persistence_function=persistence_function,
                max_birth_value=max_birth_value,
                max_death_value=max_death_value)
            self.rep_cycles = [self.rep_cycles[cycle] for cycle in cycles]
        colors = np.array(list(islice(cycle([  # '#377eb8',
            '#ff7f00', '#4daf4a',
            '#f781bf', '#a65628', '#984ea3',
            '#999999', '#e41a1c', '#dede00']),
            len(self.rep_cycles))))
        center_points = np.array([np.mean(Y[patch.point_indices], axis=0)
                                  for patch in self.cover])
        for idx, cyc in enumerate(self.rep_cycles):
            ax.plot([], color=colors[idx], label=idx)
            for edge in cyc:
                ax.plot([center_points[edge[0], 0],
                         center_points[edge[1], 0]],
                        [center_points[edge[0], 1],
                         center_points[edge[1], 1]],
                        linewidth=linewidth,
                        color=colors[idx])
        ax.scatter([np.min(Y[:, 0]), np.max(Y[:, 0])],
                   [np.min(Y[:, 1]), np.max(Y[:, 1])],
                   alpha=0)
        if legend:
            ax.legend()
        return ax

    def components(self,
                   X=None,
                   n_patches=None,
                   n_components=2,
                   persistence_function=None,
                   max_birth_value=np.inf):
        comps = self.patch_components(
            X=X,
            n_patches=n_patches,
            n_components=n_components,
            persistence_function=persistence_function,
            max_birth_value=max_birth_value)
        return np.array([np.concatenate(
            [self.cover[index].point_indices for
             index in comp]) if len(comp) > 0 else np.array([])
            for comp in comps])

    def patch_components(self,
                         X=None,
                         n_patches=None,
                         n_components=2,
                         persistence_function=None,
                         max_birth_value=np.inf):
        if (X is not None or
            n_patches is not None or
                not hasattr(self, 'simplex_tree')):
            self.set_simplex_tree(X, n_patches=n_patches, dimension=0)
        return self.simplex_tree.components(
            n_components, persistence_function, max_birth_value)

    def plot_outlier_histogram(self,
                               X=None,
                               max_num_clusters=28,
                               figsize=(15, 8)):

        outlier_lengths = {}
        for n_components in range(1, max_num_clusters + 1):
            outlier_lengths[n_components] = len(
                self.components(n_components=n_components)[-1])
        plt.figure(figsize=figsize)
        ax = plt.gca()
        ax.bar(np.arange(len(outlier_lengths)),
               outlier_lengths.values())
        ax.set_xticks(np.arange(
            0, len(outlier_lengths)))
        ax.set_xticklabels(
            np.arange(1, len(outlier_lengths) + 1))
        return ax

    def plot_cluster_histogram(self, X=None, n_clusters=11):
        if X is not None:
            self.X = X
        components = self.components(n_components=n_clusters)
        component_list = np.empty(len(self.X), dtype=int)
        for idx, component in enumerate(components):
            if len(component) > 0:
                component_list[component] = idx
        colors = np.vstack((
            plt.cm.tab10(np.arange(10)),
            plt.cm.Set3(np.arange(12))))
        colors = np.array(list(islice(cycle(colors), n_clusters)))
        colors = np.vstack((colors, [0, 0, 0, 1]))
        ax = plt.gca()
        for idx, comp in enumerate(components):
            height = np.zeros(len(components))
            height[idx] = len(comp)
            ax.bar(np.arange(len(components)),
                   height=height,
                   color=colors[idx])
        return ax

    def plot_components(self,
                        X=None,
                        Y=None,
                        n_patches=None,
                        n_components=2,
                        components=None,
                        comps=None,
                        colors=list(mcolors.TABLEAU_COLORS.values()),
                        cmap='Blues',
                        persistence_function=None,
                        max_birth_value=np.inf,
                        ax=None,
                        sample_size=None,
                        ticks=False,
                        alpha=1,
                        bandwidth=None,
                        legend=True,
                        fontsize=10,
                        markerscale=2,
                        KDE=False,
                        s=1):
        if colors is None:
            colors = np.vstack((
                plt.cm.tab10(np.arange(10)),
                plt.cm.Set3(np.arange(12))))
            colors = np.array(list(islice(cycle(colors), n_components)))
            colors = np.vstack((colors, [0, 0, 0, 1]))
        if Y is None:
            Y = self.X[:, [0, 1]]
        if len(self.X) != len(Y):
            raise ValueError('X and Y must be of the same length')
        if ax is None:
            ax = plt.gca()
        if comps is None:
            comps = self.components(
                X=X,
                n_patches=n_patches,
                n_components=n_components,
                persistence_function=persistence_function,
                max_birth_value=max_birth_value)
        if components is None:
            components = list(range(len(comps)))
        for idx in components:
            if idx >= len(comps):
                continue
            component = comps[idx]
            if len(component) == 0:
                continue
            if idx < n_components:
                color = colors[idx % len(colors)],
            else:
                color = 'black'
            plotX(Y[component],
                  sample_size=sample_size,
                  size=s,
                  ax=ax,
                  color=color,
                  cmap=cmap,
                  alpha=alpha,
                  ticks=ticks,
                  bandwidth=bandwidth,
                  label=idx,
                  KDE=KDE)
        if KDE is False:
            ax.scatter([np.min(Y[:, 0]), np.max(Y[:, 0])],
                       [np.min(Y[:, 1]), np.max(Y[:, 1])],
                       alpha=0)
            ax.legend(scatterpoints=1,
                      fontsize=fontsize,
                      markerscale=markerscale)
        return ax

    def cycle_component(self,
                        X=None,
                        n_patches=None,
                        cycle_num=None,
                        max_birth_value=np.inf,
                        max_death_value=np.inf,
                        persistence_function=None):

        cycle_patches = self.cycle_patches(
            X=X,
            n_patches=n_patches,
            cycle_num=cycle_num,
            max_birth_value=max_birth_value,
            max_death_value=max_death_value,
            persistence_function=persistence_function)

        return np.hstack([patch.point_indices for patch in cycle_patches])

    def cycle_representatives(
            self,
            X=None,
            n_patches=None,
            max_birth_value=np.inf,
            max_death_value=np.inf,
            persistence_function=None):
        if any((X is not None,
                n_patches is not None,
                not hasattr(self, 'simplex_tree'))):
            self.set_simplex_tree(X=X, n_patches=n_patches, dimension=1)
        return self.simplex_tree.cycle_representatives(
            persistence_function=persistence_function,
            max_birth_value=max_birth_value,
            max_death_value=max_death_value)

    # def parent_patch_index(self, index, G):
    #     neighbors = np.array(G[index])
    #     return neighbors[np.argmin([self.simplex_tree.filtration([v])
    #                                 for v in neighbors])]

    # def assign_point_to_cluster(
    #         self,
    #         cluster_list,
    #         index,
    #         assigned_points,
    #         G):
    #     path_cluster = set([index])
    #     while 1:
    #         if index in assigned_points:
    #             path_index = cluster_list[index]
    #             break
    #         index = self.parent_patch_index(index, G)
    #         if index in path_cluster:
    #             path_index = -1
    #             break
    #         path_cluster.add(index)
    #     assigned_points = assigned_points.union(path_cluster)
    #     cluster_list[list(path_cluster)] = path_index
    #     #     else:
    #     #         self.assign_point_to_cluster(
    #     #             cluster_list, parent, assigned_points, G)
    #     #         cluster_list[index] = cluster_list[parent]
    #     #     assigned_points.add(index)

    #     # if index not in assigned_points:
    #     #     parent = self.parent_patch_index(index, G)
    #     #     if parent == index:
    #     #         cluster_list[index] = 0
    #     #     else:
    #     #         self.assign_point_to_cluster(
    #     #             cluster_list, parent, assigned_points, G)
    #     #         cluster_list[index] = cluster_list[parent]
    #     #     assigned_points.add(index)

    def patch_cluster_list(
            self,
            X=None,
            comps=None,
            n_clusters=2,
            persistence_function=None,
            max_birth_value=np.inf):
        if comps is None:
            comps = self.patch_components(
                X=X,
                n_patches=None,
                n_components=n_clusters,
                persistence_function=persistence_function,
                max_birth_value=max_birth_value)
        G = self.graph()

        clusters = np.full(len(self.cover), -1)
        for index, comp in enumerate(comps[:-1]):
            clusters[list(comp)] = index

        for index in range(len(self.cover)):
            if clusters[index] != -1:
                continue
            else:
                L = {index}
                while(1):
                    neighbors = list(G.neighbors(index))
                    parent = neighbors[
                        np.argmin([self.simplex_tree.filtration([v])
                                   for v in neighbors])]
                    if clusters[parent] != -1:
                        clusters[list(L)] = clusters[parent]
                        break
                    else:
                        if parent not in L:
                            L.add(parent)
                            index = parent
                        else:
                            break
        return clusters

        # assigned_points=set.union(*(set(comp) for comp in comps[: -1]))
        # cluster_list=np.empty(len(self.cover), dtype = int)
        # for comp_index, comp in enumerate(comps[: -1]):
        #     for index in comp:
        #         cluster_list[index]=comp_index
        # for index, y in enumerate(self.cover):
        #     self.assign_point_to_cluster(
        #         cluster_list, index, assigned_points, G)
        # assigned_points=set(np.flatnonzero(~(cluster_list == -1)))
        # while len(assigned_points) < len(self.cover):
        #     for index in np.flatnonzero(cluster_list == -1):
        #         assigned_neighbors=np.array(
        #             list(assigned_points.intersection(G[index])))
        #         if len(assigned_neighbors) == 0:
        #             continue
        #         cluster_list[index]=cluster_list[assigned_neighbors[
        #             np.argmin(np.array([
        #                 self.simplex_tree.filtration([v]) for
        #                 v in assigned_neighbors]))]]
        #         assigned_points.add(index)
        # return cluster_list

    def cluster_list(
            self,
            X=None,
            comps=None,
            n_clusters=2,
            persistence_function=None,
            max_birth_value=np.inf):
        patch_clusters = self.patch_cluster_list(
            X=X,
            comps=comps,
            n_clusters=n_clusters,
            persistence_function=persistence_function,
            max_birth_value=max_birth_value)
        clusters = np.full(len(self.X), -1)
        for index, patch_cluster in enumerate(patch_clusters):
            clusters[self.cover[index].point_indices] = patch_cluster
        return clusters

    def plot_patch_clusters(
            self,
            X=None,
            Y=None,
            comps=None,
            n_clusters=2,
            persistence_function=None,
            max_birth_value=np.inf):
        comps = self.patch_components(n_components=n_clusters)
        cluster_list = self.patch_cluster_list(
            X=None,
            comps=comps,
            n_clusters=n_clusters,
            persistence_function=persistence_function,
            max_birth_value=max_birth_value)
        Y = np.array([patch.center for patch in self.cover])
        colors = np.array(list(islice(cycle(['#377eb8',
                                             '#ff7f00', '#4daf4a',
                                             '#f781bf', '#a65628', '#984ea3',
                                             '#999999', '#e41a1c', '#dede00']),
                                      len(comps))))
        plt.scatter(Y[:, 0],
                    Y[:, 1],
                    color=colors[cluster_list],
                    alpha=0.6
                    )
        # plt.scatter(self.X[:, 0], self.X[:, 1], s=0.01, color='grey')


def enumerate_cliques(G, max_cardinality=None):
    """Returns cliques of cardinality up to max_cardinality
    in an undirected graph.

    This method returns cliques of size (cardinality)
    k = 1, 2, 3, ..., max_cardinality.

    For now I require the nodes of G to be consequetive integers.

    Parameters
    ----------
    G: undirected graph
    max_cardinality: int

    Returns
    -------
    generator of lists: generator of list for each clique.

    Notes
    -----
    To obtain a list of all cliques, use
    :samp:`list(enumerate_all_cliques(G))`.

    Based on the algorithm published by Zhang et al. (2005) [1]_
    and adapted to output all cliques discovered.

    This algorithm is not applicable on directed graphs.

    This algorithm ignores self-loops and parallel edges as
    clique is not conventionally defined with such edges.

    There are often many cliques in graphs.
    This algorithm however, hopefully, does not run out of memory
    since it only keeps candidate sublists in memory and
    continuously removes exhausted sublists.

    References
    ----------
    .. [1] Yun Zhang, Abu-Khzam, F.N., Baldwin, N.E., Chesler, E.J.,
           Langston, M.A., Samatova, N.F.,
           Genome-Scale Computational Approaches to Memory-Intensive
           Applications in Systems Biology.
           Supercomputing, 2005. Proceedings of the ACM/IEEE SC 2005
           Conference, pp. 12, 12-18 Nov. 2005.
           doi: 10.1109/SC.2005.29.
           http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=1559964&isnumber=33129
    """
    if max_cardinality is None:
        max_cardinality = G.number_of_nodes()
    index = {}
    nbrs = {}
    for u in G:
        index[u] = len(index)
        # Neighbors of u that appear after u in the iteration order of G.
        nbrs[u] = {v for v in G[u] if v not in index}

    queue = deque(([u], sorted(nbrs[u], key=index.__getitem__)) for u in G)
    # Loop invariants:
    # 1. len(base) is nondecreasing.
    # 2. (base + cnbrs) is sorted with respect to the iteration order of G.
    # 3. cnbrs is a set of common neighbors of nodes in base.
    while queue:
        base, cnbrs = map(list, queue.popleft())
        yield base
        if len(base) < max_cardinality:
            for i, u in enumerate(cnbrs):
                # Use generators to reduce memory consumption.
                queue.append((chain(base, [u]),
                              filter(nbrs[u].__contains__,
                                     islice(cnbrs, i + 1, None))))


def plotX(X,
          x_axis=0,
          y_axis=1,
          bandwidth=None,
          grid_points=1024,
          sample_size=1000,
          ticks=False,
          size=1,
          color=None,
          cmap='Blues',
          KDE=False,
          ax=None,
          label=None,
          alpha=1):
    if ax is None:
        ax = plt.gca()
    if KDE:
        return KDEplotX(
            X=X,
            x_axis=x_axis,
            y_axis=y_axis,
            bandwidth=bandwidth,
            grid_points=grid_points,
            ticks=ticks,
            cmap=cmap,
            ax=ax,
            label=label,
            alpha=alpha)
    else:
        return plotX_sample(
            X=X,
            x_axis=x_axis,
            y_axis=y_axis,
            sample_size=sample_size,
            size=size,
            ticks=ticks,
            ax=ax,
            label=label,
            color=color,
            alpha=alpha)


def plotX_sample(X,
                 x_axis=0,
                 y_axis=1,
                 sample_size=None,
                 size=1,
                 ticks=False,
                 color=None,
                 label=None,
                 ax=None,
                 alpha=1):
    if ax is None:
        ax = plt.gca()
    ax.scatter([np.min(X[:, x_axis]), np.max(X[:, x_axis])],
               [np.min(X[:, y_axis]), np.max(X[:, y_axis])],
               alpha=0, color='black')
    if sample_size is None:
        sample_size = len(X)
    if len(X) > sample_size:
        sample = np.random.choice(
            len(X),
            size=sample_size,
            replace=False)
        X = X[sample]
    ax.scatter(X[:, x_axis],
               X[:, y_axis],
               s=size,
               alpha=alpha,
               color=color,
               label=label)
    if not ticks:
        plt.xticks(())
        plt.yticks(())
    return ax


def KDEplotX(X,
             x_axis=0,
             y_axis=1,
             bandwidth=None,
             grid_points=1024,
             ticks=False,
             ax=None,
             cmap='Blues',
             label=None,
             alpha=1):
    if ax is None:
        ax = plt.gca()
    coords = X[:, [x_axis, y_axis]]
    scaler = StandardScaler()
    coords = scaler.fit_transform(coords)
    n, d = coords.shape
    if bandwidth is None:
        bandwidth = n**(-1./(d+4))
    # kde_fft = FFTKDE(kernel='tri', bw=bandwidth).fit(coords)
    kde_fft = FFTKDE(kernel='gaussian', bw=bandwidth).fit(coords)
    grid, y = kde_fft.evaluate(grid_points=1024)
    grid = scaler.inverse_transform(grid)
    # Plot density heat map
    xmin, xmax = np.min(grid[:, 0]), np.max(grid[:, 0])
    ymin, ymax = np.min(grid[:, 1]), np.max(grid[:, 1])

    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    ax.imshow(y.reshape((
        int(np.sqrt(len(y))), int(np.sqrt(len(y))))).transpose(),
        cmap=cmap,
        origin='lower',
        extent=[xmin, xmax, ymin, ymax],
        alpha=alpha)
    if not ticks:
        plt.xticks(())
        plt.yticks(())
    ax.set_aspect((xmax - xmin) / (ymax - ymin))
    return ax


def division(x, y):
    '''
    Safe division.
    Division by zero yields np.inf
    '''
    if y != 0:
        return x/y
    else:
        return np.inf


def second_moment_sum(X):
    '''
    This cost is total variation times local density
    '''
    return np.sum(np.var(X, axis=0)) * len(X)


def second_moment_max(X):
    '''
    This cost is total variation times local density
    '''
    return np.max(np.var(X, axis=0)) * len(X)


def second_moment_variance(X):
    '''
    This cost is total variation times local density
    '''
    return np.max(np.var(X, axis=0))


def second_moment_variation(X):
    '''
    This cost is total variation times local density
    '''
    return np.sum(np.var(X, axis=0))


def split_costs(x):
    '''
    This is a one-dimensional version of the patch_cost.
    Using two-pass version of Welford's online algorithm
    https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
    '''
    return np.cumsum((
        (np.arange(1, len(x) + 1) * x - np.cumsum(x)) /
        np.arange(1, len(x) + 1))**2)


def get_principal_directions(X, num_directions=1, svd_solver='auto'):
    pca = PCA(n_components=num_directions, svd_solver=svd_solver,
              whiten=False).fit(X)
    return pca.components_


def patch_distance(self, patch, fct=max):
    return fct(
        self.distance_to_point(patch.center),
        patch.distance_to_point(self.center))


def relative_distance(self, patch, fct=max):
    return fct(
        self.distance_to_point(patch.center)**2 /
        patch.center_to_boundary_distance(),
        patch.distance_to_point(self.center)**2 /
        self.center_to_boundary_distance())


def cardinality_distance(self, patch, fct=max):
    return fct(
        self.distance_to_point(patch.center) *
        len(self.point_indices)**(1 / len(self.principal_direction)),
        patch.distance_to_point(self.center) *
        len(patch.point_indices)**(1 / len(self.principal_direction)))


def center_distance(self, patch, fct=max):
    return euclidean(patch.center, self.center)


def supremum_distance(self, patch, fct=max):
    return np.max(np.abs(patch.center - self.center))


def percentile_distance(self, patch, fct=None):
    if fct is None:
        fct = 0.5
    return np.percentile(np.max(np.abs(
        patch.center - self.cover.X[self.point_indices])), q=fct)


def split_distance(self, patch, fct=max):
    principal_directions = np.vstack((
        self.get_unreduced_halfspaces()[:, :-2],
        patch.get_unreduced_halfspaces()[:, :-2]))
    return np.max(np.abs(np.dot(
        principal_directions,
        (self.center - patch.center).transpose())))


def cardinality_split_distance(self, patch, fct=max):
    if self.center is None or patch.center is None:
        return np.inf
    principal_directions = np.vstack((
        self.get_unreduced_halfspaces()[:, :-2],
        patch.get_unreduced_halfspaces()[:, :-2]))
    relative_cardinality = (
        len(self.cover.X) /
        (len(self.point_indices) +
         len(patch.point_indices)))**(1.0 / principal_directions.shape[1])

    return (np.max(np.abs(np.dot(
        principal_directions,
        (self.center - patch.center).transpose()))) *
        relative_cardinality)


# def wasserstein_distance(self, patch, fct=None):
#     if self == patch:
#         return 0.
#     a = self.center - patch.center
#     a = a / np.linalg.norm(a)
#     a = a[:, np.newaxis]
#     u = np.dot(self.cover.X[self.point_indices], a).flatten()
#     v = np.dot(self.cover.X[patch.point_indices], a).flatten()
#     return stats.wasserstein_distance(u, v)


def hausdorff_distance(self, patch, fct=None):
    return max(
        directed_hausdorff(self.cover.X[self.point_indices],
                           patch.cover.X[patch.point_indices])[0],
        directed_hausdorff(patch.cover.X[patch.point_indices],
                           self.cover.X[self.point_indices])[0]
    )


# def energy_distance(self, patch, fct=None):
#     if self == patch:
#         return 0.
#     a = self.center - patch.center
#     a = a / np.linalg.norm(a)
#     a = a[:, np.newaxis]
#     u = np.dot(self.cover.X[self.point_indices], a).flatten()
#     v = np.dot(self.cover.X[patch.point_indices], a).flatten()
#     return stats.energy_distance(u, v)

# def wasserstein_distance(p=1):
#     def fct(self, patch, fct=None):
#         if self == patch:
#             return 0.
#         a = self.center - patch.center
#         a = a / np.linalg.norm(a)
#         a = a[:, np.newaxis]
#         u = np.dot(self.cover.X[self.point_indices], a).flatten()
#         v = np.dot(self.cover.X[patch.point_indices], a).flatten()
#         return _cdf_distance(p, u, v) ** (1/p)
#     return fct


def deviance_distance(self, patch, fct=None):
    point_indices = np.hstack((self.point_indices,
                               patch.point_indices))
    Y = np.unique(self.cover.X[point_indices], axis=0)
    if len(Y) == 1:
        return 0
    pca = PCA(n_components=1, whiten=False).fit(self.cover.X[point_indices])
    return np.sqrt(pca.explained_variance_[0])


def ward_deviance_distance(self, patch, fct=None):
    nself = len(self.point_indices)
    npatch = len(patch.point_indices)
    ward_factor = np.sqrt(1 / (nself + npatch))
    point_indices = np.hstack((self.point_indices,
                               patch.point_indices))
    Y = np.unique(self.cover.X[point_indices], axis=0)
    if len(Y) == 1:
        return 0
    pca = PCA(n_components=1, whiten=False).fit(self.cover.X[point_indices])
    return np.sqrt(pca.explained_variance_[0]) / ward_factor
