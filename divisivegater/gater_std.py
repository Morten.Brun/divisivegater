'''
Copyright: Applied Topology Group at University of Bergen.
No commercial use of the software is permitted without proper license.
'''
import pandas as pd
import numpy as np
import math
from divisivegater.patch_persistence import PatchPersistence
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import re
import fcsparser
import matplotlib.colors as mcolors
import matplotlib
from KDEpy import FFTKDE


class Gater(PatchPersistence):

    def __init__(self,
                 coeff_field=11,
                 min_persistence=0,
                 dimension=None,
                 max_homology_dimension=None,
                 truncation_value=np.inf,
                 min_samples=None,
                 num_inner_points=0,
                 persistence_order='additive',
                 distance_fct=None,
                 labels=None):
        super().__init__(coeff_field=coeff_field,
                         min_persistence=min_persistence,
                         dimension=dimension,
                         max_homology_dimension=max_homology_dimension,
                         truncation_value=truncation_value,
                         min_samples=min_samples,
                         num_inner_points=num_inner_points,
                         labels=labels,
                         distance_fct=distance_fct)

    def read_fcs_file(
            self,
            filepaths,
            asinh_factor=5,
            asinh_transform=True,
            verbose=False):
        '''
        Read in fcs file and store it as data attribute
        Default parser is fcsparser. If the fcs file is
        corrupted it may fail, and the FlowCore parser may
        succeed.
        '''
        if type(filepaths) == str:
            filepaths = [filepaths]
        datas = [read_fcs_file(
            filepath=filepath,
            asinh_factor=asinh_factor,
            asinh_transform=asinh_transform,
            verbose=verbose) for filepath
            in filepaths]
        data = pd.concat(datas, keys=np.arange(len(filepaths)), sort=False)
        data.reset_index(inplace=True)
        data.drop(labels='level_1', axis=1, inplace=True)
        data.rename(columns={'level_0': 'file_number'}, inplace=True)
        self.data = data 

    def set_markers(
            self,
            markers=None,
            point_indices=None,
            scale=True):
        self.__dict__.pop('nerve', None)
        self.__dict__.pop('simplex_tree', None)
        self.__dict__.pop('patch_graph', None)
        self.__dict__.pop('cover', None)
        self.__dict__.pop('patch_list', None)
        self.__dict__.pop('distmat', None)
        if markers is None:
            markers = self.data.columns
        if point_indices is None:
            self.point_indices = np.arange(len(self.data))
        elif point_indices.dtype == 'int64':
            self.point_indices = point_indices
        else:
            self.point_indices = np.hstack(
                (point_indices[component] for component in point_indices))
        self.X = self.data[markers].iloc[self.point_indices].values
        if scale:
            scaler = StandardScaler()
            self.X = scaler.fit_transform(self.X)

    def get_all_markers(
            self):
        return self.data.columns

    def plot_components(
            self,
            X=None,
            Y=None,
            markers=None,
            n_patches=None,
            n_components=2,
            components=None,
            comps=None,
            colors=list(mcolors.TABLEAU_COLORS.values()),
            cmap='Blues',
            persistence_function=None,
            persistence_order=None,
            max_birth_value=np.inf,
            ax=None,
            sample_size=None,
            ticks=False,
            alpha=1,
            bandwidth=None,
            legend=True,
            fontsize=10,
            markerscale=2,
            reducer=None,
            labels=False,
            KDE=False,
            s=1):

        if persistence_order is None:
            persistence_order = self.persistence_order

        ax = super().plot_components(
            Y=Y,
            n_patches=n_patches,
            n_components=n_components,
            components=components,
            comps=comps,
            colors=colors,
            cmap=cmap,
            persistence_function=persistence_function,
            persistence_order=persistence_order,
            max_birth_value=max_birth_value,
            ax=ax,
            sample_size=sample_size,
            ticks=ticks,
            alpha=alpha,
            legend=legend,
            fontsize=fontsize,
            markerscale=markerscale,
            reducer=reducer,
            KDE=KDE,
            s=s)
        if labels:
            ax.set_xlabel(markers[0])
            ax.xaxis.set_label_position('top')
            ax.set_ylabel(markers[1])
            ax.yaxis.set_label_position('right')
        return ax

    def plot_biaxial(
            self,
            markers=None,
            figsize=None,
            plot_columns=None,
            n_patches=None,
            n_components=2,
            components=None,
            comps=None,
            persistence_function=None,
            persistence_order='multiplicative',
            max_birth_value=np.inf,
            ax=None,
            sample_size=10000,
            ticks=False,
            colors=None,
            cmap='jet',
            alpha=1,
            labels=True,
            bandwidth=None,
            legends=False,
            KDE=False,
            s=10):
        if comps is None:
            comps = self.components(
                n_patches=n_patches,
                n_components=n_components,
                persistence_order=persistence_order,
                persistence_function=persistence_function,
                max_birth_value=max_birth_value)
        if markers is None:
            markers = [list(self.data.columns[[0, 1]])]
        if plot_columns is None:
            plot_columns = len(markers)
        if components is None:
            components = list(range(len(comps)))
        if colors is None:
            colors = len(components) * [None]
        if figsize is None:
            figsize = (
                15, (15 / plot_columns) *
                np.ceil((1 + len(markers) +
                         np.ceil(len(markers) / plot_columns) *
                         plot_columns * len(components)) / plot_columns))
        plt.figure(figsize=figsize)
        for component_idx, component in enumerate(components):
            if len(comps[component]) == 0:
                continue
            for marker_idx, marker_pair in enumerate(markers):
                plt.subplot(
                    np.ceil((1 + len(markers) +
                             np.ceil(len(markers) / plot_columns) *
                             plot_columns * len(components)) / plot_columns),
                    plot_columns,
                    marker_idx + 1 +
                    np.ceil(len(markers) / plot_columns) *
                    plot_columns * component_idx)
                ax = None
                ax = self.plot_components(
                    Y=self.data[marker_pair].iloc[self.point_indices].values,
                    markers=marker_pair,
                    n_patches=n_patches,
                    colors=colors,
                    cmap=cmap,
                    n_components=n_components,
                    components=[component],
                    comps=comps,
                    persistence_function=persistence_function,
                    max_birth_value=max_birth_value,
                    ax=ax,
                    sample_size=sample_size,
                    ticks=ticks,
                    alpha=alpha,
                    labels=labels,
                    bandwidth=bandwidth,
                    KDE=KDE,
                    s=s)
                if marker_idx == 0:
                    ax.set_title(component,
                                 x=-0.6, y=0.5)
                if legends:
                    ax.legend([component])
        if ticks:
            plt.subplots_adjust(
                top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.65,
                wspace=0.45)
        else:
            plt.subplots_adjust(
                top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.25,
                wspace=0.35)
        return ax

    def plot_intensities(
            self,
            markers=None,
            figsize=None,
            plot_columns=None,
            n_patches=None,
            n_components=2,
            components=None,
            comps=None,
            persistence_function=None,
            persistence_order='additive',
            max_birth_value=np.inf,
            ax=None,
            sample_size=10000,
            transform=None,
            ticks=False,
            colors=None,
            cmap='jet',
            alpha=1,
            labels=True,
            bandwidth='silverman',
            legends=False,
            s=10):
        if transform is None:
            transform = lambda x: x
        if comps is None:
            comps = self.components(
                n_patches=n_patches,
                n_components=n_components,
                persistence_function=persistence_function,
                persistence_order=persistence_order,
                max_birth_value=max_birth_value)
        comps = np.array([comp for comp in comps if len(comp) > 0], dtype=object)
        if markers is None:
            markers = list(self.data.columns[[0, 1]])
        if plot_columns is None:
            plot_columns = len(markers)
        if components is None:
            components = list(range(len(comps)))
        if colors is None:
            colors = len(components) * [None]
        if figsize is None:
            figsize = (
                15, (15 / plot_columns) *
                np.ceil((1 + len(markers) +
                         np.ceil(len(markers) / plot_columns) *
                         plot_columns * len(components)) / plot_columns))
        plt.figure(figsize=figsize)
        for component_idx, component in enumerate(components):
            for marker_idx, marker in enumerate(markers):
                ax = plt.subplot(
                    math.ceil((1 + len(markers) +
                             np.ceil(len(markers) / plot_columns) *
                             plot_columns * len(components)) / plot_columns),
                    plot_columns,
                    marker_idx + 1 +
                    math.ceil(len(markers) / plot_columns) *
                    plot_columns * component_idx)
                scaler = StandardScaler()
                Z = transform(self.data[[marker]].values)
                Z = scaler.fit_transform(Z)
                estimator = FFTKDE(kernel='gaussian', bw=bandwidth)
                # x = np.linspace(np.min(Y), 1.1 * np.max(Y), 128)
                # - 0.05 * np.max(Y)
                x, y = estimator.fit(Z).evaluate(512)
                estimator = FFTKDE(kernel='gaussian', bw=bandwidth)
                Y = transform(self.data[[marker]].iloc[
                        self.point_indices[comps[component]]].values)
                Y = scaler.transform(Y)
                y = estimator.fit(Y).evaluate(x)
                x = scaler.inverse_transform(x)
                ax.plot(x, y)
                if labels and component_idx == 0:
                    ax.set_xlabel(marker)
                    ax.xaxis.set_label_position('top')
                if marker_idx == 0:
                    ax.set_title(component,
                                 x=-0.6, y=0.5)
                if legends:
                    ax.legend([component])
                ax.set_yticks(())
                ax.set_yticklabels(())
                if any((not ticks,
                        component_idx != len(components) - 1)):
                    ax.set_xticklabels(())

        if ticks:
            plt.subplots_adjust(
                top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.65,
                wspace=0.45)
        else:
            plt.subplots_adjust(
                top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.25,
                wspace=0.35)
        return ax

    def plot_intensities_overlay(
            self,
            markers=None,
            figsize=None,
            plot_columns=None,
            n_patches=None,
            n_components=2,
            components=None,
            comps=None,
            persistence_function=None,
            max_birth_value=np.inf,
            ax=None,
            sample_size=10000,
            ticks=False,
            colors=None,
            cmap='jet',
            alpha=1,
            labels=True,
            bandwidth=None,
            legends=False,
            KDE=False,
            s=10):
        if comps is None:
            comps = self.components(
                n_patches=n_patches,
                n_components=n_components,
                persistence_function=persistence_function,
                max_birth_value=max_birth_value)
        if markers is None:
            markers = list(self.data.columns[[0, 1]])
        if plot_columns is None:
            plot_columns = len(markers)
        if components is None:
            components = list(range(len(comps)))
        if colors is None:
            colors = len(components) * [None]
        if figsize is None:
            figsize = (
                15, (15 / plot_columns) *
                np.ceil((2 + len(markers) +
                         np.ceil(1 + len(markers) / plot_columns) *
                         plot_columns) / plot_columns))
        plt.figure(figsize=figsize)
        plt.subplot(
            np.ceil((1 + len(markers) +
                     np.ceil(len(markers) / plot_columns) *
                     plot_columns) / plot_columns),
            plot_columns,
            1 +
            np.ceil(len(markers) / plot_columns) *
            plot_columns)
        ax = plt.gca()
        for component_idx, component in enumerate(components):
            ax.plot([0, 1], [0, 1], label=component_idx,
                    color=colors[component_idx])
        ax.legend()
        for marker_idx, marker in enumerate(markers):
            plt.subplot(
                np.ceil((1 + len(markers) +
                         np.ceil(len(markers) / plot_columns) *
                         plot_columns) / plot_columns),
                plot_columns,
                marker_idx + 2 +
                np.ceil(len(markers) / plot_columns) *
                plot_columns)
            ax = plt.gca()
            for component_idx, component in enumerate(components):
                Y = self.data[marker].iloc[
                    self.point_indices[comps[component]]].values
                estimator = FFTKDE(kernel='gaussian', bw='silverman')
                # x = np.linspace(np.min(Y), 1.1 * np.max(Y), 128)
                # - 0.05 * np.max(Y)
                x, y = estimator.fit(Y).evaluate(256)
                ax.plot(x, y, color=colors[component_idx])
                if labels:
                    ax.set_xlabel(marker)
                    ax.xaxis.set_label_position('top')
                if marker_idx == 0:
                    ax.set_title(component,
                                 x=-0.6, y=0.5)
                if legends:
                    ax.legend([component])
                plt.yticks(())
                if not ticks:
                    plt.xticks(())
        if ticks:
            plt.subplots_adjust(
                top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.65,
                wspace=0.45)
        else:
            plt.subplots_adjust(
                top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.25,
                wspace=0.35)
        return ax

    def plot_component_array(
            self,
            markers=None,
            figsize=None,
            n_patches=None,
            n_components=2,
            components=None,
            persistence_function=None,
            max_birth_value=np.inf,
            ax=None,
            sample_size=10000,
            ticks=False,
            alpha=1,
            labels=True,
            legends=True,
            s=1):
        if components is None:
            components = list(range(n_components + 1))
        if figsize is None:
            figsize = (15, 5 * (1 + (len(components)) // 3))
        plt.figure(figsize=figsize)
        for idx, component in enumerate(components):
            plt.subplot(1 + (len(components)) // 3, 3, idx + 1)
            ax = None
            ax = self.plot_components(
                markers=markers,
                n_patches=n_patches,
                n_components=n_components,
                components=[component],
                persistence_function=persistence_function,
                max_birth_value=max_birth_value,
                ax=ax,
                sample_size=sample_size,
                ticks=ticks,
                alpha=alpha,
                labels=labels,
                s=s)
            if legends:
                ax.legend([component])

    def plot_heatmap(
            self, 
            n_components=15,
            n_patches=None,
            persistence_function=None,
            persistence_order=None,
            components=None,
            max_birth_value=np.inf,
            markers=None,
            cmap='Spectral_r',
            cbarlabel='relative expression',
            ax=None,
            comps=None,
            scaler=StandardScaler(),
            tick_fontsize=None):
        if markers is None:
            markers = list(self.get_all_markers())

        if ax is None:
            ax = plt.gca()
        if comps is None:
            comps = self.components(
                X=X,
                n_patches=n_patches,
                n_components=n_components,
                persistence_function=persistence_function,
                persistence_order=persistence_order,
                max_birth_value=max_birth_value)
        component_marker_means = np.empty((len(comps), len(markers)))
        for marker_id, marker in enumerate(markers):
            marker_data = np.array(self.data[[marker]])
            # scaler = StandardScaler()
            marker_data = scaler.fit_transform(marker_data)
            for comp_id, comp in enumerate(comps):
                component_marker_means[comp_id, marker_id] = np.mean(marker_data[comp])
        component_marker_means = component_marker_means / np.max(np.abs(component_marker_means), axis=0)
        if components is None:
            components = np.arange(len(comps))
        im, cbar = heatmap(component_marker_means[components], components, markers, ax=ax,
                cmap=cmap, cbarlabel=cbarlabel, tick_fontsize=tick_fontsize, vmin=-1, vmax=1)
        return ax


def read_fcs_file(filepath,
                  asinh_factor=5,
                  asinh_transform=True,
                  verbose=False):
    if verbose:
        print(filepath)
    meta, data = fcsparser.parse(filepath, reformat_meta=True)
    # pick out markers that have been used and rename columns
    Pchannels = [str for str in meta.keys() if
                 (str.endswith('S') and str.startswith('$P'))]
    Pchannel_numbers = [
        [int(s) for s in re.findall(r'\d+', channel)][0]
        for channel in Pchannels]
    names = [name for name in meta['_channel_names_']]
    for k, n in enumerate(Pchannel_numbers):
        names[n-1] = meta[Pchannels[k]]
    data.columns = names
    asinh_markers = list(meta[name] for name in Pchannels)
    if asinh_transform:
        data[asinh_markers] = np.arcsinh(data[asinh_markers] / 5)
    data = data[np.sort([x for x in data.columns])]
    return data


'''
The following is taken from
the matplotlib documentation

https://matplotlib.org/3.1.1/gallery/images_contours_and_fields/image_annotated_heatmap.html
'''
def heatmap(data, row_labels, col_labels, ax=None,
            cbar_kw={}, cbarlabel="", tick_fontsize=7, **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (N, M).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-90, ha="right",
             rotation_mode="anchor", fontsize=tick_fontsize)
    plt.setp(ax.get_yticklabels(), fontsize=tick_fontsize)

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar


def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=["black", "white"],
                     threshold=None, **textkw):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A list or array of two color specifications.  The first is used for
        values below a threshold, the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **kwargs
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.
  
    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts
