"""Principal Direction Voronoi Cover

Input: A point cloud X in euclidean space

Method: The set X is divided iteratively.

Division:
    The principal direction of X is found
    and X is split so that the sums of variations
    along this direction is smallest possible.
"""

# Authors: Morten Brun <morten.brun@uib.no>
# License:
import itertools
import collections
# import operator
# import functools
import math
import numpy as np
from scipy.spatial.distance import cdist, squareform
from scipy.spatial import Delaunay
# from sklearn.cluster import MiniBatchKMeans
from miniball import Miniball
import networkx as nx
# from scipy.spatial import Delaunay
from divisivegater.simplex_tree import SimplexTree
from divisivegater.basic_cover import BasicCover
from divisivegater.utils import circumcenter
###############################################################################
# Patch class


def dtm_patch_distance(
        percentile=None,
        upper_percentile=1.,
        lower_percentile=0.,
        k=100,
        p=2):
    '''
    Sum of squared distances to barycenter.
    '''
    if percentile is not None:
        lower_percentile = 0
        upper_percentile = percentile

    def fct(X, face, patch_list):
        if len(face) == 1:
            return np.inf
        patches = [X[patch_list[patch_index].point_indices]
                   for patch_index in face]
        return patch_dtm(
            patches, lower_percentile, upper_percentile, k, p)
    return fct


def patch_dtm(patches, lower_percentile, upper_percentile, k, p):

    barycenters = [np.mean(patch, axis=0)[np.newaxis, :] for patch in patches]
    mb = Miniball(np.vstack(barycenters))
    # barycenter = np.mean(np.vstack(barycenters), axis=0)[np.newaxis, :]
    return max(dtm(np.array([mb.center()]), patch,
                   lower_percentile, upper_percentile, k, p)
               for patch in patches)


def dtm(center, patch, lower_percentile, upper_percentile, k, p):
    k = max(k, int(lower_percentile * len(patch)))
    k = min(k, math.ceil(upper_percentile * len(patch)))
    dists = cdist(center, patch).flatten()
    dists = np.partition(dists, k-1)[:k]
    return np.mean(dists ** p) ** (1/p)


class Patch(object):
    """Patch object
    """

    def __init__(
            self,
            point_indices,
            X
    ):
        self.point_indices = point_indices
        self.barycenter = np.mean(X[point_indices], axis=0)


class PatchPersistence(BasicCover):
    """Patch Persistence Cover

    """

    def __init__(self,
                 coeff_field=11,
                 min_persistence=0,
                 dimension=None,
                 max_homology_dimension=None,
                 truncation_value=np.inf,
                 min_samples=None,
                 nerve_method=None,
                 sphere_method=None,
                 num_inner_points=0,
                 inner_factor=1,
                 distance_fct=None,
                 labels=None):
        self.coeff_field = coeff_field
        self.min_persistence = min_persistence
        self.dimension = dimension
        self.max_homology_dimension = max_homology_dimension
        self.truncation_value = truncation_value
        self.min_samples = min_samples
        self.nerve_method = nerve_method
        self.sphere_method = sphere_method
        self.num_inner_points = num_inner_points
        self.inner_factor = inner_factor
        if distance_fct is None:
            distance_fct = dtm_patch_distance(
                percentile=None,
                upper_percentile=0.1,
                lower_percentile=0.,
                k=10000,
                p=2)
        self.distance_function = distance_fct
        # self.filtration_value_fct = self.distance_function

    def set_cover(self,
                  X=None,
                  labels=None):
        '''
        Merging of clusters is not good.
        Need some kind of
        persistence to merge.
        '''
        if all((not hasattr(self, 'X'),
                X is None)):
            raise ValueError('X has to be specified')
        if all((hasattr(self, 'cover'),
                X is None,
                labels is None)):
            return
        self.__dict__.pop('nerve', None)
        self.__dict__.pop('simplex_tree', None)
        self.__dict__.pop('patch_graph', None)
        if X is None:
            X = self.X
        self.X = X
        if labels is not None:
            self.labels = labels
        clusters = [
            np.flatnonzero(self.labels == label) for
            label in np.unique(self.labels)]
        self.minX = np.min(self.X, axis=0)
        self.maxX = np.max(self.X, axis=0)
        if self.X.shape[1] == 0:
            raise ValueError('X has to be non-degenerate')

        self.cover = [Patch(
            point_indices=point_indices,
            X=self.X) for
            point_indices in clusters]
        self.barycenters = np.array([
            patch.barycenter for patch in self.cover])

    def set_simplex_tree(
            self,
            X=None,
            n_patches=None,
            min_samples=None,
            labels=None,
            truncation_value=None,
            max_homology_dimension=None,
            dimension=None):

        if X is not None:
            self.__dict__.pop('simplex_tree', None)

        if not hasattr(self, 'cover'):
            self.set_cover(X=X, labels=labels)
            self.__dict__.pop('nerve', None)
            self.__dict__.pop('simplex_tree', None)
            self.__dict__.pop('patch_graph', None)

        if hasattr(self, 'simplex_tree'):
            if all((X is None,
                    min_samples is None,
                    dimension is None,
                    max_homology_dimension is None,
                    truncation_value is None)):
                return
        if dimension is not None:
            self.dimension = dimension
        if max_homology_dimension is not None:
            self.max_homology_dimension = max_homology_dimension
        if min_samples is not None:
            self.min_samples = min_samples

        simplex_tree = simplex_tree_from_patch_list(
            patch_list=self.cover,
            X=self.X,
            barycenters=self.barycenters,
            filtration_function=self.distance_function,
            cover=self.cover,
            truncation_value=self.truncation_value,
            coeff_field=self.coeff_field,
            nerve_method=self.nerve_method,
            sphere_method=self.sphere_method,
            num_inner_points=self.num_inner_points,
            inner_factor=self.inner_factor,
            min_persistence=self.min_persistence,
            max_homology_dimension=self.max_homology_dimension,
            dimension=self.dimension)

        if self.min_samples is not None:
            simplex_tree = mutual_information_simplex_tree(
                simplex_tree, self.min_samples)

        self.simplex_tree = simplex_tree


def patch_distance_matrix(X,
                          cover,
                          distance_function):
    distances = []
    for index1 in range(len(cover)):
        for index2 in range(index1 + 1, len(cover)):
            face = [index1, index2]
            distances.append(distance_function(
                X,
                face=face,
                patch_list=cover))

    distances = squareform(distances)
    np.fill_diagonal(distances, np.inf)

    return distances


def approximate_maximal_faces(X, patch_list, simplex_batch_size=10000):
    '''(Approximate) Delaunay maximal faces of barycenters
       More precisely strong Witness complex as in Carlsson
       Topology and Data.
    '''
    if X.shape[1] < 8:
        max_faces = delaunay_maximal_faces(X=X, patch_list=patch_list)
    barycenters = np.vstack([
        np.mean(X[patch.point_indices], axis=0) for patch in patch_list])
    max_faces = set()
    for index, x in enumerate(X):
        if index % simplex_batch_size:
            max_faces = set(eliminate_subsets(max_faces))
        dists = cdist([x], barycenters).flatten()
        face = frozenset(
            np.flatnonzero(dists <= np.min(dists)))
        if len(face) > X.shape[1] + 1:
            face = frozenset(
                np.argpartition(dists, X.shape[1])[:X.shape[1]])
        # elif len(face) <= X.shape[1]:

        max_faces.add(face)
    return eliminate_subsets(max_faces)


def delaunay_maximal_faces(X, patch_list):
    '''Delaunay maximal faces of barycenters
    '''
    barycenters = np.vstack([
        np.mean(X[patch.point_indices], axis=0) for patch in patch_list])
    tri = Delaunay(barycenters)
    return set(frozenset(face) for face in tri.simplices)


def del_maximal_faces(X, patch_list, dimension):
    '''Delaunay maximal faces of barycenters
    '''
    barycenters = np.vstack([
        np.mean(X[patch.point_indices], axis=0) for patch in patch_list])
    tri = Delaunay(barycenters)
    return set(frozenset(face) for face in tri.simplices)


def get_nerve_from_maximal_faces(maximal_faces,
                                 dimension):
    '''Returns nerve given a list of maximal faces
    Parameters
    -----------
    maximal_face :
    dimension : int
        Maximal homology dimension
    Returns
    -----------
    nerve : ndarray of lists of integers
        List of all simplices up to desired dimension
    '''
    # This needs to be optimized.
    # First we find the maximal faces of the dimension + 1 skeleton.
    # Then we fill in the rest of the faces.
    faces = ((frozenset(face)
              for face in
              powerset(maximal_face,
                       max_card=dimension + 2,
                       min_card=min(len(maximal_face),
                                    dimension + 2)))
             for maximal_face in maximal_faces)
    faces = frozenset(
        itertools.chain.from_iterable(faces))
    faces = ((frozenset(face)
              for face in
              powerset(maximal_face,
                       max_card=dimension + 2))
             for maximal_face in faces)
    return np.array(list(frozenset(
        itertools.chain.from_iterable(faces))))


def powerset(some_set, max_card, min_card=1):
    return itertools.chain(*[
        list(itertools.combinations(some_set, dim + 1))
        for dim in range(min_card - 1, max_card)])


def simplex_tree_from_patch_list(
        patch_list,
        X,
        barycenters,
        filtration_function,
        cover,
        truncation_value,
        coeff_field=11,
        nerve_method=None,
        sphere_method='circumsphere',
        num_inner_points=2,
        inner_factor=1,
        min_persistence=0,
        max_homology_dimension=None,
        dimension=None):

    if max_homology_dimension is None:
        max_homology_dimension = X.shape[1] - 1

    if dimension is None:
        dimension = max_homology_dimension + 1

    geometric_dimension = X.shape[1]
    persistence_dim_max = geometric_dimension <= dimension
    if persistence_dim_max:
        print('strange thing')
    simplex_tree = SimplexTree(
        coeff_field=coeff_field,
        persistence_dim_max=persistence_dim_max,
        truncation_value=truncation_value,
        min_persistence=min_persistence)

    mst = nx.minimum_spanning_tree(nx.Graph(patch_distance_matrix(
        X, cover, filtration_function)))
    for edge in mst.edges:
        simplex_tree.insert(edge, mst.edges[edge]['weight'])

    return simplex_tree


def minimum_spanning_tree(filtmat):
    filtmat = filtmat.copy()
    np.fill_diagonal(filtmat, 0)
    G = nx.from_numpy_array(filtmat)
    T = nx.minimum_spanning_tree(G)
    return [(node, ) for node in T.nodes] + [edge for edge in T.edges]


def eliminate_subsets(set_of_tuples):
    max_cardinality = max(len(subset) for subset in set_of_tuples)
    maximal_subsets = set(subset for subset in set_of_tuples
                          if len(subset) == max_cardinality)
    set_of_tuples.difference_update(maximal_subsets)
    for subset in sorted(set_of_tuples, key=len, reverse=True):
        if not any(set(subset).issubset(max_subset) for
                   max_subset in maximal_subsets):
            maximal_subsets.add(subset)
    return maximal_subsets

# def is_power_of_two(n):
#     """Returns True iff n is a power of two.  Assumes n > 0."""
#     return (n & (n - 1)) == 0


# def eliminate_subsets(sequence_of_sets):
#     """I did not write this. Far too clever for me.
#     Return a list of the elements of `sequence_of_sets`, removing all
#     elements that are subsets of other elements.  Assumes that each
#     element is a set or frozenset."""
#     # The code below does not handle the case of a sequence containing
#     # only the empty set, so let's just handle all easy cases now.
#     sequence_of_sets = list(frozenset(sequence_of_sets))
#     if len(sequence_of_sets) <= 1:
#         return list(sequence_of_sets)
#     # We need an indexable sequence so that we can use a bitmap to
#     # represent each set.
#     if not isinstance(sequence_of_sets, collections.Sequence):
#         sequence_of_sets = list(sequence_of_sets)
#     # For each element, construct the list of all sets containing that
#     # element.
#     sets_containing_element = {}
#     for i, s in enumerate(sequence_of_sets):
#         for element in s:
#             try:
#                 sets_containing_element[element] |= 1 << i
#             except KeyError:
#                 sets_containing_element[element] = 1 << i
#     # For each set, if the intersection of all of the lists in which it is
#     # contained has length != 1, this set can be eliminated.
#     out = [s for s in sequence_of_sets
#            if s and is_power_of_two(functools.reduce(
#                operator.and_, (sets_containing_element[x] for x in s)))]
#     return out


def mutual_information_simplex_tree(simplex_tree, k):
    mi_simplex_tree = SimplexTree()
    for face, value in simplex_tree.get_filtration():
        star_filtration = simplex_tree.get_star(face)
        if star_filtration:
            star, star_filtration_values = zip(*star_filtration)
            kk = min(k, len(star_filtration_values) - 1)
            core_value = np.partition(star_filtration_values, kk)[kk]
            star_filtration_values = np.maximum(
                star_filtration_values, core_value)
            for idx, facet in enumerate(star):
                if mi_simplex_tree.find(facet):
                    filt_value = max(mi_simplex_tree.filtration(facet),
                                     star_filtration_values[idx])
                else:
                    filt_value = star_filtration_values[idx]
                mi_simplex_tree.insert(facet, filt_value)
    mi_simplex_tree.make_filtration_non_decreasing()
    return mi_simplex_tree


def enumerate_cliques(G, max_cardinality=None):
    """Returns cliques of cardinality up to max_cardinality
    in an undirected graph.

    This method returns cliques of size (cardinality)
    k = 1, 2, 3, ..., max_cardinality.

    For now I require the nodes of G to be consequetive integers.

    Parameters
    ----------
    G: undirected graph
    max_cardinality: int

    Returns
    -------
    generator of lists: generator of list for each clique.

    Notes
    -----
    To obtain a list of all cliques, use
    :samp:`list(enumerate_all_cliques(G))`.

    Based on the algorithm published by Zhang et al. (2005) [1]_
    and adapted to output all cliques discovered.

    This algorithm is not applicable on directed graphs.

    This algorithm ignores self-loops and parallel edges as
    clique is not conventionally defined with such edges.

    There are often many cliques in graphs.
    This algorithm however, hopefully, does not run out of memory
    since it only keeps candidate sublists in memory and
    continuously removes exhausted sublists.

    References
    ----------
    .. [1] Yun Zhang, Abu-Khzam, F.N., Baldwin, N.E., Chesler, E.J.,
           Langston, M.A., Samatova, N.F.,
           Genome-Scale Computational Approaches to Memory-Intensive
           Applications in Systems Biology.
           Supercomputing, 2005. Proceedings of the ACM/IEEE SC 2005
           Conference, pp. 12, 12-18 Nov. 2005.
           doi: 10.1109/SC.2005.29.
           http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=1559964&isnumber=33129
    """
    if max_cardinality is None:
        max_cardinality = G.number_of_nodes()
    index = {}
    nbrs = {}
    for u in G:
        index[u] = len(index)
        # Neighbors of u that appear after u in the iteration order of G.
        nbrs[u] = {v for v in G[u] if v not in index}

    queue = collections.deque(
        ([u], sorted(nbrs[u], key=index.__getitem__)) for u in G)
    # Loop invariants:
    # 1. len(base) is nondecreasing.
    # 2. (base + cnbrs) is sorted with respect to the iteration order of G.
    # 3. cnbrs is a set of common neighbors of nodes in base.
    while queue:
        base, cnbrs = map(list, queue.popleft())
        yield base
        if len(base) < max_cardinality:
            for i, u in enumerate(cnbrs):
                # Use generators to reduce memory consumption.
                queue.append((itertools.chain(base, [u]),
                              filter(nbrs[u].__contains__,
                                     itertools.islice(cnbrs, i + 1, None))))
