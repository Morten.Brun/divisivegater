# divisivegater

Tool for clustering cytometry data.

## Installation

```
git clone https://git.app.uib.no/Morten.Brun/divisivegater.git
cd divisivegater
```
The following lines are optional. They create a separate
python environment for divisivegater.
```
conda create -n divisivegater python=3
source activate divisivegater
python -m pip install ipykernel
python -m ipykernel install --user --name=divisivegater
```
Install the weddinge miniball package
```
cd ..
git clone git@github.com:weddige/miniball.git
cd miniball
python setup.py install
cd divisivegater
```
The next lines do the rest of the installation.
```
conda install -c conda-forge gudhi 
pip install -e .
```

If you have trouble with the virtual environment, read the blog [Using Virtual Environments in Jupyter Notebook and Python](https://janakiev.com/til/jupyter-virtual-envs/)
